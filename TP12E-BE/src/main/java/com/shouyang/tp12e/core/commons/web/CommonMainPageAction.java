package com.shouyang.tp12e.core.commons.web;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.shouyang.tp12e.core.web.GenericAction;
import com.shouyang.tp12e.module.apply.account.Account;

/**
 * CommonHomePageAction
 * 
 * @author Roderick
 * @version 2014/10/12
 */
@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CommonMainPageAction extends GenericAction<Account> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8307378612623091026L;
	
}
