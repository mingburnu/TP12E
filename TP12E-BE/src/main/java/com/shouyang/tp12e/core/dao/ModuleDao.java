package com.shouyang.tp12e.core.dao;

import com.shouyang.tp12e.core.entity.GenericEntity;

/**
 * ModuleDao
 * 
 * @author Rodertick
 * @version 2014/11/21
 */
public class ModuleDao<T extends GenericEntity> extends
		GenericHibernateDao<T> {

}
