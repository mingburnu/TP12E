package com.shouyang.tp12e.core.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

/**
 * @author Roderick
 */
@MappedSuperclass
public abstract class GenericEntity extends FileIoProperties {

    /**
     *
     */
    private static final long serialVersionUID = -8739689797669392643L;

    /**
     * The log.
     */
    @Transient
    protected final transient Logger log = LoggerFactory.getLogger(getClass());

    /**
     * The id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, insertable = true, updatable = false, precision = 20)
    private Integer id;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * check entity has id or not.
     *
     * @return true, if has id
     */
    public boolean hasId() {
        return id != null;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this,
                ToStringStyle.DEFAULT_STYLE);
    }

    /**
     * transient properties
     */
    @Transient
    private String option;

    @Transient
    private Integer listNo;

    @Transient
    private Long[] checkItem;

    @Transient
    private String[] tempNotes;

    /**
     * @return the option
     */
    public String getOption() {
        return option;
    }

    /**
     * @param option the option to set
     */
    public void setOption(String option) {
        this.option = option;
    }

    /**
     * @return the listNo
     */
    public Integer getListNo() {
        return listNo;
    }

    /**
     * @param listNo the listNo to set
     */
    public void setListNo(Integer listNo) {
        this.listNo = listNo;
    }

    /**
     * @return the checkItem
     */
    public Long[] getCheckItem() {
        return checkItem;
    }

    /**
     * @param checkItem the checkItem to set
     */
    public void setCheckItem(Long[] checkItem) {
        this.checkItem = checkItem;
    }

    /**
     * @return the tempNotes
     */
    public String[] getTempNotes() {
        return tempNotes;
    }

    /**
     * @param tempNotes the tempNotes to set
     */
    public void setTempNotes(String[] tempNotes) {
        this.tempNotes = tempNotes;
    }

    @Transient
    private Integer hour;

    @Transient
    private Integer minute;

    @Transient
    private Integer second;

    /**
     * @return the hour
     */
    public Integer getHour() {
        return hour;
    }

    /**
     * @param hour the hour to set
     */
    public void setHour(Integer hour) {
        this.hour = hour;
    }

    /**
     * @return the minute
     */
    public Integer getMinute() {
        return minute;
    }

    /**
     * @param minute the minute to set
     */
    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    /**
     * @return the second
     */
    public Integer getSecond() {
        return second;
    }

    /**
     * @param second the second to set
     */
    public void setSecond(Integer second) {
        this.second = second;
    }

    @Transient
    private LocalDateTime start;

    @Transient
    private LocalDateTime end;

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }
}
