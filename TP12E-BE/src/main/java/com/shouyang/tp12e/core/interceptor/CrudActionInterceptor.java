package com.shouyang.tp12e.core.interceptor;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionInvocation;
import com.shouyang.tp12e.module.apply.account.Account;

/**
 * CRUD Action Interceptor
 * 
 * @author Roderick
 * @version 2015/1/20
 */
public class CrudActionInterceptor extends RootInterceptor {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4326400245013459644L;

	@Autowired
	private Account accountNumber;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		removeErrorParameters(invocation);

		HttpServletResponse response = ServletActionContext.getResponse();

		if (invocation.getProxy().getNamespace().equals("/crud")) {
			if (!isUsableMethod(invocation)) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return "list";
			}
		}

		return invocation.invoke();
	}
}
