package com.shouyang.tp12e.core.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.shouyang.tp12e.module.apply.visitor.VisitorDao;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * 儲存檢索log
 *
 * @author Roderick
 * @version 2015/1/20
 */
public class PageActionInterceptor extends RootInterceptor {

    /**
     *
     */
    private static final long serialVersionUID = -5010620168664114783L;

    @Autowired
    private VisitorDao visitorDao;

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        removeErrorParameters(invocation);

        if (invocation.getProxy().getActionName().equals("visitor")) {
            HttpServletRequest request = ServletActionContext.getRequest();
            request.setAttribute("todayVisitor", visitorDao.todayVisitor());
        }

        String result = invocation.invoke();

        return result;
    }
}
