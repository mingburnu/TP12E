package com.shouyang.tp12e.core.service;

import com.shouyang.tp12e.core.dao.GenericDao;
import com.shouyang.tp12e.core.entity.GenericEntity;
import com.shouyang.tp12e.module.apply.account.Account;
import com.shouyang.tp12e.module.apply.account.AccountDao;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

/**
 * GenericService
 *
 * @author Roderick
 * @version 2014/9/29
 */
public abstract class GenericService<T extends GenericEntity> extends
        ServiceFactory<T> {

    protected final transient Logger log = Logger.getLogger(getClass());

    protected abstract GenericDao<T> getDao();

    @Autowired
    private AccountDao userDao;

    @Override
    public T save(T entity, Account user) throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");

        return getDao().save(entity);
    }

    @Override
    public T getById(Integer id) throws Exception {
        Assert.notNull(id, "[Assertion failed] - this argument is required; it must not be null");

        return getDao().findById(id);
    }

    @Override
    public T update(T entity, Account user, String... ignoreProperties)
            throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");

        T dbEntity = getDao().findById(entity.getId());

        if (ignoreProperties.length == 0) {
            BeanUtils.copyProperties(entity, dbEntity);
        } else {
            BeanUtils.copyProperties(entity, dbEntity, ignoreProperties);
        }

        getDao().update(dbEntity);

        return dbEntity;
    }

    @Override
    public void deleteById(Integer id) throws Exception {
        Assert.notNull(id, "[Assertion failed] - this argument is required; it must not be null");

        getDao().deleteById(id);
    }
}
