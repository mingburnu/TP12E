package com.shouyang.tp12e.core.web;

import com.shouyang.tp12e.core.entity.GenericEntity;

/**
 * GenericCRUDAction
 * 
 * @author Roderick
 * @version 2014/11/21
 */
public abstract class GenericWebAction<T extends GenericEntity> extends
		GenericCRUDAction<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -923075655713880057L;

}
