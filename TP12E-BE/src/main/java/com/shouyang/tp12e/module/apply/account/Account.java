package com.shouyang.tp12e.module.apply.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.struts2.json.annotations.JSON;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.shouyang.tp12e.core.entity.GenericEntity;

/**
 * 使用者
 * 
 * @author Roderick
 * @version 2016/04/25
 */
@Entity
@Table(name = "account")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Account extends GenericEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5804311089729989927L;

	/**
	 * 使用者代號
	 */
	@Column(name = "userid", unique = true)
	private String userId;

	/**
	 * 使用者密碼
	 */
	@Column(name = "passwd")
	private String passwd;

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the passwd
	 */
	@JSON(serialize = false)
	public String getPasswd() {
		return passwd;
	}

	/**
	 * @param passwd
	 *            the passwd to set
	 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Account(String userId, String passwd) {
		super();
		this.userId = userId;
		this.passwd = passwd;
	}
}
