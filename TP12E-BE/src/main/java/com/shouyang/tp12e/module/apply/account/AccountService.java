package com.shouyang.tp12e.module.apply.account;

import com.shouyang.tp12e.core.dao.DsRestrictions;
import com.shouyang.tp12e.core.dao.GenericDao;
import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.core.service.GenericService;
import com.shouyang.tp12e.core.util.EncryptorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

/**
 * 使用者 Service
 *
 * @author Roderick
 * @version 2014/9/30
 */
@Service
public class AccountService extends GenericService<Account> {

    @Autowired
    private AccountDao dao;

    @Override
    protected GenericDao<Account> getDao() {
        return dao;
    }

    /**
     * 登入取得帳號資料
     *
     * @param ds
     * @return
     * @throws Exception
     */
    @Override
    public DataSet<Account> getByRestrictions(DataSet<Account> ds)
            throws Exception {
        Assert.notNull(ds, "[Assertion failed] - this argument is required; it must not be null");
        Assert.notNull(ds.getEntity(), "[Assertion failed] - this argument is required; it must not be null");

        Account entity = ds.getEntity();
        DsRestrictions restrictions = getDsRestrictions();

        if (StringUtils.isNotEmpty(entity.getUserId())) {
            restrictions.eq("userId", entity.getUserId());
        }

        return dao.findByRestrictions(restrictions, ds);
    }

    @Override
    public Account save(Account entity, Account user) throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");

        if (StringUtils.isNotEmpty(entity.getPasswd())) { // 密碼非空則進行加密
            final String encryptedPassword = EncryptorUtils.encrypt(entity
                    .getPasswd());
            entity.setPasswd(encryptedPassword);
        }

        return dao.save(entity);
    }

    @Override
    public Account update(Account entity, Account user,
                          String... ignoreProperties) throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");

        if (StringUtils.isNotEmpty(entity.getPasswd())) {
            final String encryptedPassword = EncryptorUtils.encrypt(entity
                    .getPasswd());
            entity.setPasswd(encryptedPassword);
        }

        Account dbEntity = dao.findById(entity.getId());

        BeanUtils.copyProperties(entity, dbEntity, ignoreProperties);

        dao.update(dbEntity);

        return dbEntity;
    }

    /**
     * 檢查登入帳密
     *
     * @param entity
     * @return
     * @throws Exception
     */
    public Boolean checkUserId(Account entity) throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");

        DsRestrictions restrictions = getDsRestrictions();
        restrictions.eq("userId", entity.getUserId());
        List<Account> secUsers = dao.findByRestrictions(restrictions);
        return !CollectionUtils.isEmpty(secUsers);

    }

    public Boolean checkUserPw(Account entity) throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");

        DsRestrictions restrictions = getDsRestrictions();
        restrictions.eq("userId", entity.getUserId());

        List<Account> secUsers = dao.findByRestrictions(restrictions);
        if (CollectionUtils.isEmpty(secUsers)) {
            return false;
        }
        Account secUser = secUsers.get(0);

        return EncryptorUtils.checkPassword(entity.getPasswd(),
                secUser.getPasswd());
    }

    public long getSerNoByUserId(String userId) throws Exception {
        DsRestrictions restrictions = getDsRestrictions();
        restrictions.customCriterion(Restrictions.eq("userId", userId));

        if (dao.findByRestrictions(restrictions).size() > 0) {
            return (dao.findByRestrictions(restrictions).get(0)).getId();
        } else {
            return 0;
        }
    }
}
