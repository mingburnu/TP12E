package com.shouyang.tp12e.module.apply.attachment;

import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.core.web.GenericWebAction;
import com.shouyang.tp12e.module.apply.enums.Category;
import com.shouyang.tp12e.module.apply.news.News;
import com.shouyang.tp12e.module.apply.news.NewsService;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.UUID;

/**
 * 使用者
 *
 * @author Roderick
 * @version 2014/9/29
 */
@Controller
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AttachmentAction extends GenericWebAction<Attachment> {

    /**
     *
     */
    private static final long serialVersionUID = 9198667697775718131L;

    @Autowired
    private Attachment attachment;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private News news;

    @Override
    protected void validateSave() throws Exception {
        if (hasNews()) {
            if (StringUtils.isBlank(getEntity().getName())) {
                errorMessages.add("name is blank");
            } else {
                if (StringUtils.isBlank(getEntity().getOption())) {
                    errorMessages.add("has no option");
                } else if (getEntity().getOption().equals("url")) {
                    log.info(getEntity().getOption());
                    if (!isLink(getEntity().getUrl())) {
                        errorMessages.add("is not link");
                    }
                } else if (getEntity().getOption().equals("file")) {
                    if (gtMaxSize(getRequest(), 25 * 1024 * 1024)) {
                        errorMessages.add("gt size limit");
                    } else if (ArrayUtils.isEmpty(getEntity().getFile())) {
                        errorMessages.add("has no file");
                    }
                } else {
                    errorMessages.add("has no option");
                }
            }
        } else {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @Override
    protected void validateUpdate() throws Exception {
        if (!hasEntity()) {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        } else {
            if (StringUtils.isBlank(getEntity().getName())) {
                errorMessages.add("name is blank");
            }
        }
    }

    @Override
    protected void validateDelete() throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public String add() throws Exception {
        if (!hasNews()) {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        return ADD;
    }

    @Override
    public String edit() throws Exception {
        if (hasNews()) {
            DataSet<Attachment> ds = initDataSet();
            ds.getResults().addAll(news.getAttachments());
        } else {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        return EDIT;
    }

    @Override
    public String list() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String save() throws Exception {
        validateSave();
        setActionErrors(errorMessages);

        if (!hasActionErrors()) {

            if (getEntity().getOption().equals("url")) {
                attachment = new Attachment(getEntity().getName(), getEntity()
                        .getUrl(), null, getEntity().getTop(), getEntity().getCategory(),
                        getEntity().getNews());
            } else {
                String parentDir = "/tp12efile/" + UUID.randomUUID().toString()
                        + "-" + UUID.randomUUID().toString();

                while (new File(parentDir).exists()) {
                    parentDir = "/tp12efile/" + UUID.randomUUID().toString()
                            + "-" + UUID.randomUUID().toString();
                }

                File destFile = new File(parentDir, getEntity()
                        .getFileFileName()[0]);
                FileUtils.copyFile(getEntity().getFile()[0], destFile);

                attachment = new Attachment(getEntity().getName(), null,
                        destFile.getAbsolutePath(), getEntity().getTop(), getEntity()
                        .getCategory(), getEntity().getNews());
            }

            if (getEntity().getTop() == null) {
                getEntity().setTop(false);
            }
            attachmentService.save(attachment, getLoginUser());

            return LIST;
        } else {

            return ADD;
        }
    }

    @Override
    public String update() throws Exception {
        validateUpdate();
        setActionErrors(errorMessages);

        if (!hasActionErrors()) {
            if (getEntity().getTop() == null) {
                getEntity().setTop(false);
            }

            attachmentService.update(getEntity(), getLoginUser(), "url",
                    "path", "news");

            return LIST;
        } else {

            return EDIT;
        }
    }

    @Override
    public String delete() throws Exception {
        if (hasEntity()) {
            if (StringUtils.isNotBlank(attachment.getPath())) {
                FileUtils.deleteDirectory(new File(attachment.getPath())
                        .getParentFile());
            }

            attachmentService.deleteById(getEntity().getId());
        } else {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        return EDIT;
    }

    public String show() throws Exception {
        if (hasEntity()) {
            if (StringUtils.isNotBlank(attachment.getPath())) {
                File file = new File(attachment.getPath());
                FileInputStream fis = new FileInputStream(file);

                UserAgent userAgent = UserAgent
                        .parseUserAgentString(getRequest().getHeader(
                                "User-Agent"));

                if (userAgent.getBrowser().getGroup().equals(Browser.IE)
                        || userAgent.getBrowser().getGroup()
                        .equals(Browser.EDGE)) {
                    getEntity().setReportFile(
                            URLEncoder.encode(file.getName(), "UTF8")
                                    .replaceAll("\\+", "%20"));
                } else {
                    getEntity().setReportFile(
                            new String(file.getName().getBytes("UTF8"),
                                    "ISO8859-1"));
                }

                getEntity().setInputStream(fis);
            } else {
                getResponse().sendRedirect(attachment.getUrl());
            }
        } else {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        return DOC;
    }

    public String modify() throws Exception {
        if (hasEntity()) {
            setEntity(attachment);
        } else {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        return MODIFY;
    }

    public String brochure() throws Exception {
        getEntity().setCategory(Category.簡章下載);
        DataSet<Attachment> ds = attachmentService
                .getByRestrictions(initDataSet());

        if (ds.getResults().size() == 0 && ds.getPager().getCurrentPage() > 1) {
            Double lastPage = Math.ceil(ds.getPager().getTotalRecord()
                    .doubleValue()
                    / ds.getPager().getRecordPerPage().doubleValue());
            ds.getPager().setCurrentPage(lastPage.intValue());
            attachmentService.getByRestrictions(ds);
        }

        return LIST;
    }

    public String briefing() throws Exception {
        getEntity().setCategory(Category.簡報講綱);
        DataSet<Attachment> ds = attachmentService
                .getByRestrictions(initDataSet());

        if (ds.getResults().size() == 0 && ds.getPager().getCurrentPage() > 1) {
            Double lastPage = Math.ceil(ds.getPager().getTotalRecord()
                    .doubleValue()
                    / ds.getPager().getRecordPerPage().doubleValue());
            ds.getPager().setCurrentPage(lastPage.intValue());
            attachmentService.getByRestrictions(ds);
        }

        return LIST;
    }

    public String cut() throws Exception {
        if (hasEntity() && attachment.getCategory() != null) {

            getEntity().setCategory(attachment.getCategory());

            if (StringUtils.isNotBlank(attachment.getPath())) {
                FileUtils.deleteDirectory(new File(attachment.getPath())
                        .getParentFile());
            }

            attachmentService.deleteById(getEntity().getId());

            if (getEntity().getCategory().equals(Category.簡章下載)) {
                brochure();
            }

            if (getEntity().getCategory().equals(Category.簡報講綱)) {
                briefing();
            }

        } else {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        return LIST;
    }

    protected boolean hasEntity() throws Exception {
        if (getEntity().getId() == null) {
            getEntity().setId(-1);
            return false;
        }

        attachment = attachmentService.getById(getEntity().getId());
        if (attachment == null) {
            return false;
        }

        return true;
    }

    protected boolean hasNews() throws Exception {
        if (getEntity().getNews() == null) {
            getEntity().setNews(new News());
            getEntity().getNews().setId(-1);
            return false;
        }

        if (getEntity().getNews().getId() == null) {
            getEntity().setId(-1);
            return false;
        }

        news = newsService.getById(getEntity().getNews().getId());
        if (news == null) {
            return false;
        }

        return true;
    }

    protected boolean isLink(String url) {
        if (StringUtils.isNotEmpty(url)) {
            url = url.trim();
        }

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            return false;
        }

        return ESAPI.validator().isValidInput("Attachment URL", url, "URL",
                Integer.MAX_VALUE, false);
    }
}
