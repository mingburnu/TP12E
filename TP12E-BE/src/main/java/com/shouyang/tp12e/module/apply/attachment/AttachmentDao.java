package com.shouyang.tp12e.module.apply.attachment;

import org.springframework.stereotype.Repository;

import com.shouyang.tp12e.core.dao.ModuleDao;

/**
 * 使用者 Dao
 * 
 * @author Roderick
 * @version 2014/9/29
 */
@Repository
public class AttachmentDao extends ModuleDao<Attachment> {

}
