package com.shouyang.tp12e.module.apply.enums;

/**
 * Action
 * 
 * @author Roderick
 * @version 2015/01/19
 */
public enum NewsClass {
	/**
	 * Logs
	 */
	公告("公告"),

	更新("更新"),

	活動("活動"),

	其他("其他");

	private String newsClass;

	private NewsClass() {
	}

	private NewsClass(String newsClass) {
		this.newsClass = newsClass;
	}

	/**
	 * @return the action
	 */
	public String getNewsClass() {
		return newsClass;
	}

}
