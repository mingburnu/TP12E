package com.shouyang.tp12e.module.apply.news;

import java.io.File;
import java.util.Iterator;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.core.web.GenericWebAction;
import com.shouyang.tp12e.module.apply.attachment.Attachment;
import com.shouyang.tp12e.module.apply.enums.NewsClass;

/**
 * 使用者
 * 
 * @author Roderick
 * @version 2014/9/29
 */
@Controller
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NewsAction extends GenericWebAction<News> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9198667697775718131L;

	@Autowired
	private News news;

	@Autowired
	private NewsService newsService;

	@Override
	protected void validateSave() throws Exception {
		if (StringUtils.isBlank(getEntity().getTitle())) {
			errorMessages.add("請輸入標題");
		}

		if (getEntity().getNewsClass() == null) {
			getEntity().setNewsClass(NewsClass.公告);
		}

		String time = getEntity().getHour() + ":" + getEntity().getMinute()
				+ ":" + getEntity().getSecond();
		LocalTime localTime = convertTime(time);
		if (getEntity().getPublishTime() == null || localTime == null) {
			errorMessages.add("請輸入發佈日期");
		} else {
			getEntity().setPublishTime(
					getEntity().getPublishTime().plusMillis(
							localTime.getMillisOfDay()));
		}

		if (StringUtils.isBlank(getEntity().getContent())) {
			errorMessages.add("請輸入內容");
		}
	}

	@Override
	protected void validateUpdate() throws Exception {
		if (!hasEntity()) {
			getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
		} else {
			if (StringUtils.isBlank(getEntity().getTitle())) {
				errorMessages.add("請輸入標題");
			}

			if (getEntity().getNewsClass() == null) {
				getEntity().setNewsClass(NewsClass.公告);
			}

			String time = getEntity().getHour() + ":" + getEntity().getMinute()
					+ ":" + getEntity().getSecond();
			LocalTime localTime = convertTime(time);
			if (getEntity().getPublishTime() == null || localTime == null) {
				errorMessages.add("請輸入發佈日期");
			} else {
				getEntity().setPublishTime(
						getEntity().getPublishTime().plusMillis(
								localTime.getMillisOfDay()));
			}

			if (StringUtils.isBlank(getEntity().getContent())) {
				errorMessages.add("請輸入內容");
			}
		}

	}

	@Override
	protected void validateDelete() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public String add() throws Exception {
		return ADD;
	}

	@Override
	public String edit() throws Exception {
		if (hasEntity()) {
			news.setHour(news.getPublishTime().getHourOfDay());
			news.setMinute(news.getPublishTime().getMinuteOfHour());
			news.setSecond(news.getPublishTime().getSecondOfMinute());
			setEntity(news);
		} else {
			getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		return EDIT;
	}

	@Override
	public String list() throws Exception {
		DataSet<News> ds = newsService.getByRestrictions(initDataSet());

		if (ds.getResults().size() == 0 && ds.getPager().getCurrentPage() > 1) {
			Double lastPage = Math.ceil(ds.getPager().getTotalRecord()
					.doubleValue()
					/ ds.getPager().getRecordPerPage().doubleValue());
			ds.getPager().setCurrentPage(lastPage.intValue());
			newsService.getByRestrictions(ds);
		}

		return LIST;
	}

	@Override
	public String save() throws Exception {
		validateSave();
		setActionErrors(errorMessages);

		if (!hasActionErrors()) {
			newsService.save(getEntity(), getLoginUser());
			list();
			return LIST;
		} else {

			return ADD;
		}
	}

	@Override
	public String update() throws Exception {
		validateUpdate();
		setActionErrors(errorMessages);

		if (!hasActionErrors()) {
			newsService.update(getEntity(), getLoginUser());
			list();
			return LIST;
		} else {

			return EDIT;
		}
	}

	@Override
	public String delete() throws Exception {
		if (hasEntity()) {
			Iterator<Attachment> iterator = news.getAttachments().iterator();
			while (iterator.hasNext()) {
				Attachment attachment = iterator.next();
				if (StringUtils.isNotEmpty(attachment.getPath())) {
					File file = new File(attachment.getPath());
					FileUtils.deleteDirectory(file.getParentFile());
				}
			}

			newsService.deleteById(getEntity().getId());
		} else {
			getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
		}

		list();
		return LIST;
	}

	protected LocalTime convertTime(String time) throws Exception {
		LocalTime localTime = null;
		try {
			localTime = LocalTime.parse(time,
					DateTimeFormat.forPattern("HH:mm:ss"));
		} catch (IllegalArgumentException e) {
			return null;
		}

		return localTime;
	}

	protected boolean hasEntity() throws Exception {
		if (getEntity().getId() == null) {
			getEntity().setId(-1);
			return false;
		}

		news = newsService.getById(getEntity().getId());
		if (news == null) {
			return false;
		}

		return true;
	}
}
