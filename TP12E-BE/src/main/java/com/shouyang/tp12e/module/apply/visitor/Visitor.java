package com.shouyang.tp12e.module.apply.visitor;

import com.shouyang.tp12e.core.entity.GenericEntity;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 使用者
 *
 * @author Roderick
 * @version 2016/04/25
 */
@Entity
@Table(name = "visitor")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Visitor extends GenericEntity {

    private static final long serialVersionUID = -5425500874466218285L;

    /**
     * SessionId
     */
    @Column(name = "sessionId")
    private String sessionId;
    
    /**
     * IP
     */
    @Column(name = "ip")
    private String ip;

    /**
     * 訪問時間
     */
    @Column(name = "visit_time")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime visitTime;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public LocalDateTime getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(LocalDateTime visitTime) {
        this.visitTime = visitTime;
    }

    public Visitor() {
        super();
    }

    public Visitor(String sessionId, String ip, LocalDateTime visitTime) {
        super();
        this.sessionId = sessionId;
        this.ip = ip;
        this.visitTime = visitTime;
    }
}
