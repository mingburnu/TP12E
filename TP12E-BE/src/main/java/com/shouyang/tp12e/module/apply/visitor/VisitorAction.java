package com.shouyang.tp12e.module.apply.visitor;

import com.shouyang.tp12e.core.web.GenericWebAction;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by roderick on 2017/5/9.
 */
@Controller
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class VisitorAction extends GenericWebAction<Visitor> {

    private static final long serialVersionUID = -4929493029062783514L;

    @Autowired
    private VisitorService visitorService;

    @Override
    protected void validateSave() throws Exception {

    }

    @Override
    protected void validateUpdate() throws Exception {

    }

    @Override
    protected void validateDelete() throws Exception {

    }

    @Override
    public String add() throws Exception {
        return null;
    }

    @Override
    public String edit() throws Exception {
        return null;
    }

    @Override
    public String list() throws Exception {
        getRequest().setAttribute("todayVisitor", visitorService.todayVisitor());

        validateList();

        setActionErrors(errorMessages);

        if (!hasActionErrors()) {
            getRequest().setAttribute("betweenVisitor", visitorService.betweenVisitor(initDataSet()));

            Map<String, Object> datas = new LinkedHashMap<>();
            LocalDateTime yearMonth = getEntity().getStart();
            while (yearMonth.compareTo(getEntity().getEnd()) <= 0) {
                datas.put(yearMonth.getYear() + "年" + yearMonth.getMonthOfYear() + "月", visitorService.monthVisitor(yearMonth));
                yearMonth = yearMonth.plusMonths(1);
            }

            initDataSet().setDatas(datas);
        }

        return LIST;
    }

    @Override
    public String save() throws Exception {
        return null;
    }

    @Override
    public String update() throws Exception {
        return null;
    }

    @Override
    public String delete() throws Exception {
        return null;
    }

    public String report() throws IOException {
        validateList();
        setActionErrors(errorMessages);

        if (!hasActionErrors()) {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet spreadsheet = workbook.createSheet("visitor statics");
            XSSFRow row;

            Map<String, Object[]> empinfo = new LinkedHashMap<String, Object[]>();
            empinfo.put("1", new Object[]{"年月", "訪客瀏覽次數"});

            LocalDateTime yearMonth = getEntity().getStart();

            int i = 0;
            while (yearMonth.compareTo(getEntity().getEnd()) <= 0) {
                empinfo.put(String.valueOf(i + 2), new Object[]{yearMonth.getYear() + "年" + yearMonth.getMonthOfYear() + "月", visitorService.monthVisitor(yearMonth)});
                yearMonth = yearMonth.plusMonths(1);
                i++;
            }

            Set<String> keyid = empinfo.keySet();
            int rowid = 0;
            for (String key : keyid) {
                row = spreadsheet.createRow(rowid++);
                Object[] objectArr = empinfo.get(key);
                int cellid = 0;
                for (Object obj : objectArr) {
                    Cell cell = row.createCell(cellid++);
                    cell.setCellValue(obj.toString());
                }
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            workbook.write(baos);
            getEntity().setReportFile("counts.xlsx");
            getEntity().setInputStream(new ByteArrayInputStream(baos.toByteArray()));
            return XLSX;
        } else {
            getResponse().sendError(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
    }

    protected void validateList() {
        LocalDateTime start = null;
        LocalDateTime end = null;
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime firstMonth = LocalDateTime.parse("2017-06-01");
        LocalDateTime lastMonth = LocalDateTime.parse(now.toString("yyyy") + "-"+now.toString("MM")+"-01");

        try {
            start = LocalDateTime.parse(getRequest().getParameter("startYear") + "-" + getRequest().getParameter("startMonth"), DateTimeFormat.forPattern("yyyy-MM"));
            if (start.compareTo(firstMonth) == -1 || start.compareTo(lastMonth) == 1) {
                errorMessages.add("請輸入正確起訖月份");
            }
        } catch (IllegalArgumentException e) {
            errorMessages.add("請輸入正確起訖月份");
        }

        try {
            end = LocalDateTime.parse(getRequest().getParameter("endYear") + "-" + getRequest().getParameter("endMonth"), DateTimeFormat.forPattern("yyyy-MM"));
            if (end.compareTo(firstMonth) == -1 || end.compareTo(lastMonth) == 1) {
                errorMessages.add("請輸入正確起訖月份");
            }
        } catch (IllegalArgumentException e) {
            errorMessages.add("請輸入正確起訖月份");
        }

        getEntity().setStart(start);
        getEntity().setEnd(end);

        if (start.compareTo(end) > 0) {
            errorMessages.add("請輸入正確起訖月份");
        }
    }

}
