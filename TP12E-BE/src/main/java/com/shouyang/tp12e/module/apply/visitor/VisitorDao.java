package com.shouyang.tp12e.module.apply.visitor;

import com.shouyang.tp12e.core.dao.ModuleDao;
import com.shouyang.tp12e.core.model.DataSet;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

/**
 * Created by roderick on 2017/5/9.
 */
@Repository
public class VisitorDao extends ModuleDao<Visitor> {
    private long visitorBefore201606 = 241196L;

    public Long todayVisitor() {
        Criteria criteria = getSession().createCriteria(Visitor.class);
        criteria.add(Restrictions.ge("visitTime", LocalDateTime.parse(LocalDateTime.now().toString().split("T")[0])));
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    public Long betweenVisitor(DataSet<Visitor> ds) {

        if (ds.getEntity().getStart().compareTo(LocalDateTime.parse("2017-06-01")) == 1) {
            Criteria criteria = getSession().createCriteria(Visitor.class);
            criteria.add(Restrictions.ge("visitTime", ds.getEntity().getStart()));
            criteria.add(Restrictions.lt("visitTime", ds.getEntity().getEnd().plusMonths(1)));
            criteria.setProjection(Projections.rowCount());
            return (Long) criteria.list().get(0);
        } else if (ds.getEntity().getStart().compareTo(LocalDateTime.parse("2017-06-01")) == 0 && ds.getEntity().getStart().compareTo(ds.getEntity().getEnd()) == 0) {
            return visitorBefore201606;
        } else {
            Criteria criteria = getSession().createCriteria(Visitor.class);
            criteria.add(Restrictions.ge("visitTime", ds.getEntity().getStart()));
            criteria.add(Restrictions.lt("visitTime", ds.getEntity().getEnd().plusMonths(1)));
            criteria.setProjection(Projections.rowCount());
            return (Long) criteria.list().get(0) + visitorBefore201606;
        }
    }

    public Long monthVisitor(LocalDateTime firstDayOfYearMonth) {
        if (firstDayOfYearMonth.compareTo(LocalDateTime.parse("2017-06-01")) == 1) {
            Criteria criteria = getSession().createCriteria(Visitor.class);
            criteria.add(Restrictions.ge("visitTime", firstDayOfYearMonth));
            criteria.add(Restrictions.lt("visitTime", firstDayOfYearMonth.plusMonths(1)));
            criteria.setProjection(Projections.rowCount());
            return (Long) criteria.list().get(0);
        } else {
            return visitorBefore201606;
        }
    }
}