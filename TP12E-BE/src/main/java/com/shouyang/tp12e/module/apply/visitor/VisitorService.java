package com.shouyang.tp12e.module.apply.visitor;

import com.shouyang.tp12e.core.dao.GenericDao;
import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.core.service.GenericService;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by roderick on 2017/5/9.
 */
@Service
public class VisitorService extends GenericService<Visitor> {

    @Autowired
    private VisitorDao dao;

    @Override
    protected GenericDao<Visitor> getDao() {
        return dao;
    }

    @Override
    public DataSet<Visitor> getByRestrictions(DataSet<Visitor> ds) throws Exception {
        return null;
    }

    public Long todayVisitor() {
        return dao.todayVisitor();
    }

    public Long betweenVisitor(DataSet<Visitor> ds) {
        return dao.betweenVisitor(ds);
    }

    public Long monthVisitor(LocalDateTime firstDayOfYearMonth) {
        return dao.monthVisitor(firstDayOfYearMonth);
    }
}
