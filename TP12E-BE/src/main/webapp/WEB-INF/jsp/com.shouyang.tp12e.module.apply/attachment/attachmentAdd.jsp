<%@ page import="com.shouyang.tp12e.module.apply.attachment.Attachment" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="esapi"
           uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<!DOCTYPE html>
<!-- 功能 區塊 Begin -->
<div class="func">
    <a class="btn_02" href="javascript:goBack();">返回</a>
    <a class="btn_02" href="javascript:void(0);" onClick="addRow();">新 增 附 件</a>
    <a class="btn_02" href="javascript:void(0);" onClick="form_submit();">確 認 更 新</a>
</div>
<!-- 功能 區塊 End -->

<!-- appfile 區塊 Begin -->
<div class="appfile">
    <div class="row_template" style="display: none;">
        <s:form namespace="/crud" action="apply.attachment.save">
            <s:hidden name="entity.news.id"/>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <th>檔案名稱(&#8226;)</th>
                    <td><input type="text" name="entity.name" size="50"
                               class="v_file_title"></td>
                </tr>
                <tr>
                    <th>檔案來源</th>
                    <td>
                        <div>
                            使用方式： <label><input type="radio" class="radio_url"
                                                name="entity.option" value="url"
                                                onClick="chgShowField(this,'url');" checked>檔案網址</label> <label><input
                                type="radio" class="radio_file" name="entity.option"
                                value="file" onClick="chgShowField(this,'file');">上傳檔案</label>
                        </div>
                        <div class="appfile_box url">
                            檔案網址：<input type="text" class="v_file_url" name="entity.url"
                                        size="30"> <span>需要輸入http:// 或 https://</span>
                        </div>
                        <div class="appfile_box file" style="display: none;">
                            從電腦上傳檔案：<input type="file" class="v_file_file" name="entity.file">
                            <span>檔案大小的限制於25MB以下</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>發佈範圍</th>
                    <td>
                        <div>
                            <s:checkboxlist name="entity.category"
                                            list="@com.shouyang.tp12e.module.apply.enums.Category@values()"
                                            listKey="name()" listValue="name()" onchange="check($(this))"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>是否顯示於首頁</th>
                    <td>
                        <s:radio name="entity.top" list="#{'false':'否','true':'是'}" value="false"/>
                    </td>
                </tr>
            </table>
        </s:form>
        <div align="right">
            <a class="btn_03" href="javascript:void(0);" onClick="delRow(this);">刪除此筆</a>
        </div>
    </div>
    <div class="appfile list"></div>

</div>
<!-- appfile 區塊 End -->


<!-- Note 區塊 Begin -->
<div class="detail_note">
    <div class="detail_note_title">Note</div>
    <div class="detail_note_content">
        <span class="required">(&#8226;)</span>為必填欄位
    </div>
</div>
<!-- Note 區塊 End -->

<script type="text/javascript">
    $(document).ready(
            function () {
                /**/
                $("#__multiselect_apply_attachment_save_entity_category").attr(
                        "type", "checkbox").attr("name", "entity.category")
                        .attr("checked", true).css({
                    "display": "none"
                });

                addRow();
            });

    function check(i) {
        if (i.is(":checked")) {
            i.parent().children().prop("checked", false);
            i.prop("checked", true);
        } else {
            i.parent().children().prop("checked", false);
            i.parent().children().last().prop("checked", true);
        }
    }

    function rad() {
        alert($("div.row > form#apply_attachment_save").serialize());
    }

    function chgShowField(arg1, arg2) {
        if (arg2 == 'url') {
            $(arg1).parent().parent().find('.radio_url').prop('checked', true);
            $(arg1).parent().parent().find('.radio_file')
                    .prop('checked', false);

            $(arg1).parent().parent().parent().find('.appfile_box.url').show();
            $(arg1).parent().parent().parent().find('.appfile_box.file').hide();
        } else if (arg2 == 'file') {
            $(arg1).parent().parent().find('.radio_url').prop('checked', false);
            $(arg1).parent().parent().find('.radio_file').prop('checked', true);

            $(arg1).parent().parent().parent().find('.appfile_box.url').hide();
            $(arg1).parent().parent().parent().find('.appfile_box.file').show();
        }
    }

    function goBack() {
        var url = "<c:url value='/' />crud/apply.news.list.action";
        var data = "pager.currentPage=${pager.currentPage}";
        $.ajax({
            url: url,
            data: data,
            success: function (result) {
                $("div.browse").html(result);
            }
        });
        $("body").scrollTop(0);
    }

    function addRow() {
        var new_html = $(".row_template").html();
        $(".appfile.list").append('<div class="row">' + new_html + '</div>');
    }

    function delRow(arg) {
        if (confirm('您確認要刪除該筆？')) {
            $(arg).parent().parent().detach();
        } else {
            //
        }
    }

    function form_submit() {
        var isEmpty_v_title = false;
        var isEmpty_v_option = false;
        var isError_v_URL = false;
        var isEmpty_v_file = false;
        $(".row")
                .each(
                        function (index) {
                            //驗證標題
                            if ($(this).find(".v_file_title").val() == "") {
                                isEmpty_v_title = true;
                            }

                            //驗證選項
                            if ($(this)
                                            .find(
                                                    "input[name='entity.option'][type='radio']:checked")
                                            .val() == null) {
                                isEmpty_v_option = true;
                            } else if ($(this)
                                            .find(
                                                    "input[name='entity.option'][type='radio']:checked")
                                            .val() == "url") {
                                //驗證檔案網址的字頭http:// 和 https://
                                if ($(this).find(".v_file_url").length == 0
                                        || !$(this).find(".v_file_url").val()
                                                .match(/^http([s]?):\/\/.*/)) {
                                    isError_v_URL = true;
                                }
                            } else if ($(this)
                                            .find(
                                                    "input[name='entity.option'][type='radio']:checked")
                                            .val() == "file") {
                                //驗證附件
                                if ($(this)
                                                .find(
                                                        "input[class='v_file_file'][type='file']").length == 0
                                        || $(this)
                                                .find(
                                                        "input[class='v_file_file'][type='file']")
                                                .val() == "") {
                                    isEmpty_v_file = true;
                                }

                            } else {
                                isEmpty_v_option = true;
                            }

                            if (isEmpty_v_title == true
                                    && isEmpty_v_option == true
                                    && isError_v_URL == true
                                    && isEmpty_v_file == true) {
                                return false;
                            }
                        });

        var msg = "";
        if (isEmpty_v_title) {
            msg += "．請輸入標題。\r\n";
        }

        if (isEmpty_v_option) {
            msg += "．請選擇使用方式。\r\n";
        }

        if (isEmpty_v_file) {
            msg += "．請上傳檔案。\r\n";
        }
        if (isError_v_URL) {
            msg += "．檔案網址請輸入http:// 或 https://。\r\n";
        }
        if (msg != "") {
            alert(msg);
            return false;
        } else {
            upload();
        }
    }

    function upload() {
        var msg = "";

        $(".row")
                .each(
                        function (index) {
                            if ($(this)
                                            .find(
                                                    "input[name='entity.option'][type='radio']:checked")
                                            .val() == "file") {

                                if (window.FormData !== undefined) {
                                    var file = $(this)
                                            .find(
                                                    "input[class='v_file_file'][type='file']")[0].files[0];
                                    if (file.size > 26214400) {
                                        msg += "．"
                                                + $(this).find(".v_file_title")
                                                        .val() + "檔案超過25MB。\r\n";
                                    }
                                } else {
                                    msg += "．本站不支援舊版瀏覽器上傳附件。\r\n";
                                    return false;
                                }
                            }
                        });

        if (msg != "") {
            alert(msg);
            return false;
        }

        function getDoc(frame) {
            var doc = null;

            // IE8 cascading access check
            try {
                if (frame.contentWindow) {
                    doc = frame.contentWindow.document;
                }
            } catch (err) {
            }

            if (doc) { // successful getting content
                return doc;
            }

            try { // simply checking may throw in ie8 under ssl or mismatched protocol
                doc = frame.contentDocument ? frame.contentDocument
                        : frame.document;
            } catch (err) {
                // last attempt
                doc = frame.document;
            }
            return doc;
        }

        var length = $(".row").length;

        $(".row")
                .each(
                        function (index, element) {
                            var formURL = $(this).children().first().attr(
                                    "action");
                            var formData = null;
                            if ($(this)
                                            .find(
                                                    "input[name='entity.option'][type='radio']:checked")
                                            .val() == "url") {
                                $(this).find("input[name='entity.file']")
                                        .remove();
                                formData = $(this).children().first()
                                        .serialize();

                                $
                                        .ajax({
                                            url: formURL,
                                            type: 'POST',
                                            data: formData,
                                            success: function (data,
                                                               textStatus, jqXHR) {
                                                if (index == length - 1) {
                                                    goBack();
                                                }
                                            },
                                            error: function (jqXHR, textStatus,
                                                             errorThrown) {
                                                if (index == length - 1) {
                                                    goBack();
                                                }
                                            }
                                        });

                            }

                            if ($(this)
                                            .find(
                                                    "input[name='entity.option'][type='radio']:checked")
                                            .val() == "file") {
                                $(this).find("input[name='entity.url']")
                                        .remove();
                                formData = new FormData($(this).children()
                                        .first()[0]);

                                $
                                        .ajax({
                                            url: formURL,
                                            type: 'POST',
                                            data: formData,
                                            mimeType: "multipart/form-data",
                                            contentType: false,
                                            cache: false,
                                            processData: false,
                                            success: function (data,
                                                               textStatus, jqXHR) {
                                                if (index == length - 1) {
                                                    goBack();
                                                }
                                            },
                                            error: function (jqXHR, textStatus,
                                                             errorThrown) {
                                                if (index == length - 1) {
                                                    goBack();
                                                }
                                            }
                                        });
                            }
                        });
    }


</script>