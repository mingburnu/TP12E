<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="esapi"
           uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<!-- 分頁 區塊 Begin -->
<c:choose>
    <c:when test="${entity.category=='簡章下載' }">
        <jsp:include page="/WEB-INF/jsp/layout/pagination.jsp">
            <jsp:param name="action" value="apply.attachment.brochure"/>
            <jsp:param name="pager" value="${ds.pager}"/>
            <jsp:param name="pager.offset" value="${ds.pager.offset}"/>
        </jsp:include>
    </c:when>
    <c:when test="${entity.category=='簡報講綱' }">
        <jsp:include page="/WEB-INF/jsp/layout/pagination.jsp">
            <jsp:param name="action" value="apply.attachment.briefing"/>
            <jsp:param name="pager" value="${ds.pager}"/>
            <jsp:param name="pager.offset" value="${ds.pager.offset}"/>
        </jsp:include>
    </c:when>
</c:choose>

<!-- 分頁 區塊 End -->

<!-- 列表 區塊 Begin -->
<div class="list">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th width="80">發佈日期</th>
            <th>標題 / 附件</th>
            <th width="160">是否顯示於首頁</th>
            <th width="100">功能</th>
        </tr>
        <c:forEach var="item" items="${ds.results}" varStatus="status">
            <tr valign="top">
                <td>${fn:replace(fn:split(item.news.publishTime, 'T')[0],'-','/')}</td>
                <td><a target="blank"
                       href="${pageContext.request.contextPath}/crud/apply.attachment.show.action?entity.id=${item.id}"><esapi:encodeForHTML>${item.name }</esapi:encodeForHTML></a>
                </td>
                <td>
                    <c:choose>
                        <c:when test="${item.top }">
                            是
                        </c:when>
                        <c:otherwise>
                            否
                        </c:otherwise>
                    </c:choose>
                </td>
                <td><a class="btn_03" onclick="goModify('${item.id}')">修 改</a>
                    <a class="btn_03" href="javascript:void(0);"
                       onClick="goDel('${item.id}');">刪 除</a> <%--c:if test="${item.top }">
						<a class="btn_03" href="javascript:void(0);"
							onClick="goCancel('${item.id}');">取 消</a>
					</c:if> <c:if test="${!item.top }">
						<a class="btn_03" href="javascript:void(0);"
							onClick="goTop('${item.id}');">置 頂</a>
					</c:if--%></td>
            </tr>
        </c:forEach>
    </table>
</div>
<!-- 列表 區塊 End -->

<!-- 分頁 區塊 Begin -->
<c:choose>
    <c:when test="${entity.category=='簡章下載' }">
        <jsp:include page="/WEB-INF/jsp/layout/pagination.jsp">
            <jsp:param name="action" value="apply.attachment.brochure"/>
            <jsp:param name="pager" value="${ds.pager}"/>
            <jsp:param name="pager.offset" value="${ds.pager.offset}"/>
        </jsp:include>
    </c:when>
    <c:when test="${entity.category=='簡報講綱' }">
        <jsp:include page="/WEB-INF/jsp/layout/pagination.jsp">
            <jsp:param name="action" value="apply.attachment.briefing"/>
            <jsp:param name="pager" value="${ds.pager}"/>
            <jsp:param name="pager.offset" value="${ds.pager.offset}"/>
        </jsp:include>
    </c:when>
</c:choose>
<!-- 分頁 區塊 End -->

<script type="text/javascript">
    function goModify(id) {
        var url = "<c:url value='/' />crud/apply.attachment.modify.action";
        var data = "entity.id=" + id + "&"
                + "pager.currentPage=${pager.currentPage}";
        $.ajax({
            url: url,
            data: data,
            success: function (result) {
                $("div.browse").html(result);
            }
        });
        $("body").scrollTop(0);
    }

    function goDel(id) {
        if (confirm('您確認要刪除該筆？')) {

            var url = "<c:url value='/' />crud/apply.attachment.cut.action?entity.id="
                    + id + "&" + "pager.currentPage=${pager.currentPage}";
            $.post(url, function (data) {
                //$(this).ajaxComplete(gotoPage("${pager.currentPage}"));
                $("div.browse").html(data);
            });

        } else {
            //
        }
    }
    <%--function goTop(id) {
            if (confirm('您確認要置頂該筆？')) {
                var url = "<c:url value='/' />crud/apply.attachment.list.action?entity.id="
                        + id
                        + "&"
                        + "pager.currentPage=${pager.currentPage}";
                $.post(url, function(data) {
                    $("div.browse").html(data);
                });

            } else {
                //
            }
        }--%>

    <%--function goCancel(id) {
            if (confirm('您確認要取消置頂該筆？')) {
                var url = "<c:url value='/' />crud/apply.attachment.list.action?entity.id="
                        + id + "&" + "pager.currentPage=${pager.currentPage}";
                $.post(url, function(data) {
                    $("div.browse").html(data);
                });

            } else {
                //
            }
        }--%>


</script>