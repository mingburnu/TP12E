<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="esapi"
           uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<!DOCTYPE html>

<!-- appfile 區塊 Begin -->
<div class="appfile">
    <div class="row">
        <s:form namespace="/crud" action="apply.attachment.update">
            <s:hidden name="entity.id"/>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <th>檔案名稱(&#8226;)</th>
                    <td><s:textfield name="entity.name" cssClass="v_file_title"
                                     size="50"/></td>
                </tr>
                <tr>
                    <th>檢視檔案</th>
                    <td>
                        <div class="appfile_view">
                            <a target="_blank"
                               href="${pageContext.request.contextPath}/crud/apply.attachment.show.action?entity.id=${entity.id}"><esapi:encodeForHTML>${entity.name }</esapi:encodeForHTML></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>發佈範圍</th>
                    <td>
                        <div>
                            <s:checkboxlist name="entity.category"
                                            list="@com.shouyang.tp12e.module.apply.enums.Category@values()"
                                            listKey="name()" listValue="name()" onchange="check($(this))"
                                            value='%{#attr.entity.category }'/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>是否顯示於首頁</th>
                    <td>
                        <s:radio name="entity.top" list="#{'false':'否','true':'是'}" value="%{#attr.entity.top}"/>
                    </td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td><a class="btn_02" href="javascript:goBack();">返回</a> <a
                            class="btn_02" href="javascript:void(0);" onClick="form_submit();">送出</a></td>
                </tr>
            </table>
        </s:form>
    </div>
</div>
<!-- appfile 區塊 End -->


<!-- Note 區塊 Begin -->
<div class="detail_note">
    <div class="detail_note_title">Note</div>
    <div class="detail_note_content">
        <span class="required">(&#8226;)</span>為必填欄位
    </div>
</div>
<!-- Note 區塊 End -->
<c:set var="goBack">
    <c:choose>
        <c:when test="${entity.category=='簡章下載' }">
            ${pageContext.request.contextPath}/crud/apply.attachment.brochure.action
        </c:when>
        <c:when test="${entity.category=='簡報講綱' }">
            ${pageContext.request.contextPath}/crud/apply.attachment.briefing.action
        </c:when>
    </c:choose>
</c:set>
<script type="text/javascript">
    $(document)
            .ready(
                    function () {
                        $(
                                "div > #__multiselect_apply_attachment_update_entity_category")
                                .each(
                                        function () {
                                            $(this).attr("type", "checkbox")
                                                    .attr("name",
                                                            "entity.category")
                                                    .css({
                                                        "display": "none"
                                                    });

                                            var length = $(this).parent()
                                                    .children().length;
                                            $(this)
                                                    .parent()
                                                    .children()
                                                    .each(
                                                            function (index,
                                                                      element) {
                                                                if ($(this)
                                                                                .is(
                                                                                        ":checked")) {
                                                                    return false;
                                                                }

                                                                if (index == length - 1) {
                                                                    $(this)
                                                                            .attr(
                                                                                    "checked",
                                                                                    true);
                                                                }
                                                            });
                                        });
                    });

    function check(i) {
        if (i.is(":checked")) {
            i.parent().children().prop("checked", false);
            i.prop("checked", true);
        } else {
            i.parent().children().prop("checked", false);
            i.parent().children().last().prop("checked", true);
        }
    }

    function goBack() {
        var url = "${goBack}";
        var data = "pager.currentPage=${pager.currentPage}";
        $.ajax({
            url: url,
            data: data,
            success: function (result) {
                $("div.browse").html(result);
            }
        });
        $("body").scrollTop(0);
    }

    function form_submit() {
        //驗證標題
        var isEmpty_v_file_title = false;
        $(".row").each(function (index) {
            if ($(this).find(".v_file_title").val() == "") {
                isEmpty_v_file_title = true;
                return false;
            }
        });

        var msg = "";
        if (isEmpty_v_file_title) {
            msg += "．請輸入標題。\r\n";
        }

        if (msg != "") {
            alert(msg);
            return false;
        } else {
            update();
        }
    }

    function update() {
        var length = $(".row").length;

        $(".row").each(function (index, element) {
            var formURL = $(this).children().first().attr("action");
            var formData = $(this).children().first().serialize();

            $.ajax({
                url: formURL,
                type: 'POST',
                data: formData,
                success: function (data, textStatus, jqXHR) {
                    if (index == length - 1) {
                        $(this).ajaxComplete(goBack());
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (index == length - 1) {
                        $(this).ajaxComplete(goBack());
                    }
                }
            });
        });
    }


</script>