<%@ page import="org.owasp.esapi.ESAPI"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="esapi"
	uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API"%>
<%
	String publishTime = "";
	if (request.getParameter("entity.publishTime") != null) {
		publishTime = request.getParameter("entity.publishTime");
	} else {
		if (request.getAttribute("entity.publishTime") != null) {
			publishTime = request.getAttribute("entity.publishTime")
					.toString().split("T")[0].replace("-", "/");
		}
	}
%>
<div class="detail">
	<s:form namespace="/crud" action="apply.news.update">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<th>標題(•)</th>
					<td><s:textfield name="entity.title" cssClass="v_01" size="50" /></td>
				</tr>
				<tr>
					<th>類別(•)</th>
					<td><s:select name="entity.newsClass" cssClass="v_02"
							list="@com.shouyang.tp12e.module.apply.enums.NewsClass@values()" /></td>
				</tr>
				<tr>
					<th>發佈日期(•)</th>
					<td><input type="text" name="entity.publishTime"
						value="<%=ESAPI.encoder().encodeForHTMLAttribute(publishTime)%>"
						id="v_03" class="v_03"> <select name="entity.hour">
							<c:forEach var="i" begin="0" end="23">
								<c:choose>
									<c:when test="${i < 10 }">
										<c:choose>
											<c:when test="${i == entity.hour }">
												<option value="0${i }" selected="selected">0${i }</option>
											</c:when>
											<c:otherwise>
												<option value="0${i }">0${i }</option>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${i == entity.hour }">
												<option value="${i }" selected="selected">${i }</option>
											</c:when>
											<c:otherwise>
												<option value="${i }">${i }</option>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</select> 時 <select name="entity.minute">
							<c:forEach var="i" begin="0" end="59">
								<c:choose>
									<c:when test="${i < 10 }">
										<c:choose>
											<c:when test="${i == entity.minute }">
												<option value="0${i }" selected="selected">0${i }</option>
											</c:when>
											<c:otherwise>
												<option value="0${i }">0${i }</option>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${i == entity.minute }">
												<option value="${i }" selected="selected">${i }</option>
											</c:when>
											<c:otherwise>
												<option value="${i }">${i }</option>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</select> 分 <select name="entity.second">
							<c:forEach var="i" begin="0" end="59">
								<c:choose>
									<c:when test="${i < 10 }">
										<c:choose>
											<c:when test="${i == entity.second }">
												<option value="0${i }" selected="selected">0${i }</option>
											</c:when>
											<c:otherwise>
												<option value="0${i }">0${i }</option>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${i == entity.second }">
												<option value="${i }" selected="selected">${i }</option>
											</c:when>
											<c:otherwise>
												<option value="${i }">${i }</option>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</select> 秒</td>
				</tr>
				<tr>
					<th>內容(•)</th>
					<td><s:textarea name="entity.content" cssClass="v_04"
							cols="80" rows="10" /></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td><a class="btn_02" href="javascript:goBack();">返回</a> <a
						class="btn_02" href="javascript:form_submit();">送出</a></td>
				</tr>
			</tbody>
		</table>

	</s:form>
</div>
<div class="detail_note">
	<div class="detail_note_title">Note</div>
	<div class="detail_note_content">
		<span class="required">(&#8226;)</span>為必填欄位
	</div>

</div>
<script type="text/javascript">
	$("input#v_03").datepicker({
		dateFormat : "yy/mm/dd"
	});

	function form_submit() {
		var msg = "";
		if ($(".v_01").val() == "") {
			msg += "．請輸入標題。\r\n";
		}
		if ($(".v_03").val() == "") {
			msg += "．請輸入發佈日期。\r\n";
		}
		if ($(".v_04").val() == "") {
			msg += "．請輸入內容。\r\n";
		}
		if (msg != "") {
			alert(msg);
			return false;
		} else {
			var url = "<c:url value='/' />crud/apply.news.update.action";
			var data = "entity.id=${entity.id}" + "&"
					+ $("form#apply_news_update").serialize();
			$.ajax({
				url : url,
				data : data,
				success : function(result) {
					$("div.browse").html(result);
				}
			});
			$("body").scrollTop(0);
		}
	}

	function goBack() {
		var url = "<c:url value='/' />crud/apply.news.list.action";
		var data = "pager.currentPage=${pager.currentPage}";
		$.ajax({
			url : url,
			data : data,
			success : function(result) {
				$("div.browse").html(result);
			}
		});
		$("body").scrollTop(0);
	}
</script>
<jsp:include page="/WEB-INF/jsp/layout/msg.jsp" />