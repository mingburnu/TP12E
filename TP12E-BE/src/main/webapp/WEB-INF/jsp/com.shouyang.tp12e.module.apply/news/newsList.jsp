<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="esapi"
           uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>

<!-- 功能 區塊 Begin -->
<div class="func">
    <a class="btn_02" onclick="goAdd()">新 增 消 息</a>
</div>
<!-- 功能 區塊 End -->

<!-- 分頁 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/pagination.jsp">
    <jsp:param name="action" value="apply.news.list"/>
    <jsp:param name="pager" value="${ds.pager}"/>
    <jsp:param name="pager.offset" value="${ds.pager.offset}"/>
</jsp:include>

<!-- 分頁 區塊 End -->


<!-- 列表 區塊 Begin -->
<div class="list">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th width="80">發佈日期</th>
            <th width="50">類別</th>
            <th>標題 / 附件</th>
            <th width="100">功能</th>
        </tr>
        <c:forEach var="item" items="${ds.results}" varStatus="status">
            <tr valign="top">
                <td>${fn:replace(fn:split(item.publishTime, 'T')[0],'-','/')}</td>
                <td>${item.newsClass }</td>
                <td><esapi:encodeForHTML>${item.title }</esapi:encodeForHTML>
                    <ul>
                        <c:forEach var="attachment" items="${item.attachments }">
                            <li><a target="_blank"
                                   href="${pageContext.request.contextPath}/crud/apply.attachment.show.action?entity.id=${attachment.id }"><esapi:encodeForHTML>${attachment.name }</esapi:encodeForHTML></a>
                            </li>
                        </c:forEach>
                    </ul>
                </td>
                <td><a class="btn_03" onclick="goEdit('${item.id}')">修 改</a> <a
                        class="btn_03" onclick="goUpload('${item.id}')">新 增 附 件 </a> <c:if
                        test="${not empty item.attachments }">
                    <a class="btn_03" onclick="goModify('${item.id}')">修 改 附 件</a>
                </c:if> <a class="btn_03" href="javascript:void(0);"
                           onClick="goDel('${item.id}');">刪 除</a></td>
            </tr>
        </c:forEach>
    </table>
</div>
<!-- 列表 區塊 End -->

<!-- 分頁 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/pagination.jsp">
    <jsp:param name="action" value="apply.news.list"/>
    <jsp:param name="pager" value="${ds.pager}"/>
    <jsp:param name="pager.offset" value="${ds.pager.offset}"/>
</jsp:include>

<!-- 分頁 區塊 End -->

<script type="text/javascript">
    function goAdd() {
        var url = "<c:url value='/' />crud/apply.news.add.action";
        var data = "";
        $.ajax({
            url: url,
            data: data,
            success: function (result) {
                $("div.browse").html(result);
            }
        });
        $("body").scrollTop(0);
    }

    function goEdit(id) {
        var url = "<c:url value='/' />crud/apply.news.edit.action";
        var data = "entity.id=" + id;
        $.ajax({
            url: url,
            data: data,
            success: function (result) {
                $("div.browse").html(result);
            }
        });
        $("body").scrollTop(0);
    }

    function goUpload(id) {
        var url = "<c:url value='/' />crud/apply.attachment.add.action";
        var data = "entity.news.id=" + id + "&"
                + "pager.currentPage=${pager.currentPage}";
        $.ajax({
            url: url,
            data: data,
            success: function (result) {
                $("div.browse").html(result);
            }
        });
        $("body").scrollTop(0);

    }

    function goModify(id) {
        var url = "<c:url value='/' />crud/apply.attachment.edit.action";
        var data = "entity.news.id=" + id + "&"
                + "pager.currentPage=${pager.currentPage}";
        $.ajax({
            url: url,
            data: data,
            success: function (result) {
                $("div.browse").html(result);
            }
        });
        $("body").scrollTop(0);

    }

    function goDel(id) {
        var url = "<c:url value='/' />crud/apply.news.delete.action";
        var data = "entity.id=" + id + "&"
                + "pager.currentPage=${pager.currentPage}";
        $.ajax({
            url: url,
            data: data,
            success: function (result) {
                $("div.browse").html(result);
            }
        });
        $("body").scrollTop(0);
    }

</script>