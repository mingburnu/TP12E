<%@ page import="org.joda.time.LocalDateTime" %>
<%--
  Created by IntelliJ IDEA.
  User: roderick
  Date: 2017/5/19
  Time: 下午 04:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="esapi" uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>

<!-- 功能 區塊 Begin -->
<div class="func">
    <div class="func_in_2">今日訪客人數累計 <span>${todayVisitor}</span> 人次</div>
    <form id="visitor">
        <select class="v_01" name="startYear">
            <c:forEach var="y" begin="2017" end="<%=LocalDateTime.now().getYear()%>">
                <c:choose>
                    <c:when test='${y eq pageContext.request.getParameter("startYear")}'>
                        <option value="${y}" selected>${y}年</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${y}">${y}年</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>

        <select class="v_02" name="startMonth">
            <c:forEach var="m" begin="01" end="12">
                <c:choose>
                    <c:when test='${m eq pageContext.request.getParameter("startMonth")}'>
                        <option value="${m}" selected>${m}月</option>
                    </c:when>
                    <c:when test='${empty pageContext.request.getParameter("startMonth") && m eq 6}'>
                        <option value="${m}" selected>${m}月</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${m}">${m}月</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        ~
        <select class="v_03" name="endYear">
            <c:forEach var="y" begin="2017" end="<%=LocalDateTime.now().getYear()%>">
                <c:choose>
                    <c:when test='${y eq pageContext.request.getParameter("endYear")}'>
                        <option value="${y}" selected>${y}年</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${y}">${y}年</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <select class="v_04" name="endMonth">
            <c:forEach var="m" begin="01" end="12">
                <c:choose>
                    <c:when test='${m eq pageContext.request.getParameter("endMonth")}'>
                        <option value="${m}" selected>${m}月</option>
                    </c:when>
                    <c:when test='${empty pageContext.request.getParameter("endMonth") && m eq 6}'>
                        <option value="${m}" selected>${m}月</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${m}">${m}月</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <a class="btn_02" href="javascript:void(0);" onClick="form_submit();">查詢</a>

        <a class="btn_02" href="javascript:void(0);" onClick="form_submit_xls();">匯出</a>
    </form>
</div>
<div class="func">
    <div class="func_in_2">自2017年7月起，可按月統計瀏覽人次</div>
</div>
<!-- 功能 區塊 End -->


<!-- 列表 區塊 Begin -->
<div class="list">
    <c:if test="${not empty betweenVisitor }">
        <div class="summary_text">
            開始時間：<span><s:property value="entity.start.year"/>年<s:property value="entity.start.monthOfYear"/>月</span>
        </div>
        <div class="summary_text">
            結束時間：<span><s:property value="entity.end.year"/>年<s:property value="entity.end.monthOfYear"/>月</span>
        </div>
        <div class="summary_text">累計人次：<span>${betweenVisitor}</span></div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <th>年月</th>
                <th>訪客瀏覽次數</th>
            </tr>
            <s:iterator value="ds.datas" var="data">
                <tr>
                    <td>${data.key}</td>
                    <td>${data.value}</td>
                </tr>
            </s:iterator>


        </table>
    </c:if>
</div>
<!-- 列表 區塊 End -->
<script>
    function form_submit() {
        var msg = "";
        var s_01 = $(".v_01").val() - $(".v_03").val();
        var s_02 = $(".v_02").val() - $(".v_04").val();

        if (s_01 == 0) {
            if (s_02 > 0) {
                msg += "．請輸入正確起訖月份。\r\n";
            }
        }
        if (s_01 >= 1) {
            msg += "．請輸入正確起訖年份。\r\n";
        }
        if (msg != "") {
            alert(msg);
            return false;
        }

        var url = "${pageContext.request.contextPath}/crud/apply.visitor.list.action";
        $.ajax({
            url: url,
            data: $("form#visitor").serialize(),
            success: function (result) {
                $("div.browse").html(result);
            }
        });

        $("body").scrollTop(0);
    }

    function form_submit_xls() {
        var msg = "";
        var s_01 = $(".v_01").val() - $(".v_03").val();
        var s_02 = $(".v_02").val() - $(".v_04").val();

        if (s_01 == 0) {
            if (s_02 > 0) {
                msg += "．請輸入正確起訖月份。\r\n";
            }
        }
        if (s_01 >= 1) {
            msg += "．請輸入正確起訖年份。\r\n";
        }
        if (msg != "") {
            alert(msg);
            return false;
        }

        var url = "${pageContext.request.contextPath}/crud/apply.visitor.report.action?" + $("form#visitor").serialize();
        $.fileDownload(url, {
            failCallback: function (html, url) {
                alert("．請輸入正確起訖年份。\r\n");
            }
        });
    }
</script>
<jsp:include page="/WEB-INF/jsp/layout/msg.jsp"/>