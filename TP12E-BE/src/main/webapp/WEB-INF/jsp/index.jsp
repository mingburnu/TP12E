<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, maximum-scale=1, initial-scale=1">
    <meta http-equiv="expires" content="0">
    <title>臺北市十二年國民基本教育資訊網</title>
    <link rel="icon"
          href="<%=request.getContextPath()%>/templates/images/favicon.ico">
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/templates/art.css">
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/templates/jquery-ui.css">
    <script type="text/javascript"
            src="<%=request.getContextPath()%>/templates/jquery-1.11.3.min.js"></script>
    <script type="text/javascript"
            src="<%=request.getContextPath()%>/templates/jquery-ui.js"></script>
    <script type="text/javascript"
            src="<%=request.getContextPath()%>/templates/art.js"></script>
    <script type="text/javascript"
            src="<%=request.getContextPath()%>/resources/js/jquery.history.js"></script>
    <script type="text/javascript"
            src="<%=request.getContextPath()%>/resources/js/jquery.fileDownload.js"></script>
</head>

<body>
<div class="wrapper">
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp"/>
    <div class="container">
        <div class="innerwrapper">
            <jsp:include page="/WEB-INF/jsp/layout/menu.jsp"/>

            <div class="browse"></div>
        </div>
    </div>
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
</div>

<!-- 執行javascript 區塊 Begin -->
<script type="text/javascript">
    $(document).ready(function () {
        var url = "<c:url value = '/'/>";
        if (typeof (history.pushState) == "undefined") {
            History.pushState({
                "page": url,
                "data": ""
            }, url, url);
        } else {
            history.pushState({
                page: url,
                data: ""
            }, url, url);
        }
    });

    if (typeof (history.pushState) == "undefined") {
        window.onhashchange = function () {
            history.forward();
        };
    } else {
        window.addEventListener("popstate", function (e) {
            history.forward();
        });
    }

    $(document).ready(function () {
        goNews();
    });
</script>
</body>
</html>