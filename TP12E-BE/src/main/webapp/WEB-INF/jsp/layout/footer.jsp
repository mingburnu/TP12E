<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="footer">
	<div class="innerwrapper">
		<div class="footer_01">十二年國教諮詢專線：02-27668812</div>
		<div class="footer_02">
			臺北市政府教育局 地址：11008 臺北市市府路一號8F <a target="_blank"
				href="http://www.edunet.taipei.gov.tw/content.asp?Cuitem=1288454&amp;mp=104001">本局電話分機一覽表</a>
			<a target="_blank"
				href="http://tcgwww.taipei.gov.tw/ct.asp?xItem=1106851&amp;ctNode=37338&amp;mp=104001">隱私權政策</a>
			<a target="_blank"
				href="http://www.doe.taipei.gov.tw/ct.asp?xItem=1187337&amp;ctNode=37341&amp;mp=104001">著作權聲明</a>
		</div>
		<div class="footer_03">臺北市政府教育局 版權所有 Copyright &copy; 2016. All
			Rights Reserved.</div>
	</div>
</div>