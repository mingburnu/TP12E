<%@ page import="org.joda.time.LocalDateTime" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="menu">
    <a class="menu_link_a" onclick="goNews()">消 息 管 理</a>
    <a class="menu_link_a" onclick="goBrochures()">簡 章 下 載 管 理</a>
    <a class="menu_link_a" onclick="goBriefings()">簡 報 講 綱 管 理</a>
    <a class="menu_link_a" onclick="goVisitor()">訪 客 人 數 歷 史 查 詢</a>
    <a class="menu_link_b" href="${pageContext.request.contextPath}/authorization/logout.action">登 出</a>
</div>
<script type="text/javascript">
    function goNews() {
        var url = "${pageContext.request.contextPath}/crud/apply.news.list.action";
        $.ajax({
            url: url,
            success: function (result) {
                $("div.browse").html(result);
            }
        });

        $("body").scrollTop(0);
    }

    function goBrochures() {
        var url = "${pageContext.request.contextPath}/crud/apply.attachment.brochure.action";
        $.ajax({
            url: url,
            success: function (result) {
                $("div.browse").html(result);
            }
        });

        $("body").scrollTop(0);
    }

    function goBriefings() {
        var url = "${pageContext.request.contextPath}/crud/apply.attachment.briefing.action";
        $.ajax({
            url: url,
            success: function (result) {
                $("div.browse").html(result);
            }
        });

        $("body").scrollTop(0);
    }

    function goVisitor() {
        var url = "${pageContext.request.contextPath}/page/visitor.action";
        $.ajax({
            url: url,
            success: function (result) {
                $("div.browse").html(result);
            }
        });

        $("body").scrollTop(0);
    }
</script>