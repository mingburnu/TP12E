<%@ page import="org.owasp.esapi.ESAPI" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg" uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib prefix="esapi"
           uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>

<c:set var="recordPerPage" value="${pager.recordPerPage}"/>
<c:set var="totalRecord" value="${pager.totalRecord}"/>
<c:set var="currentPage" value="${pager.currentPage}"/>
<c:set var="recordPoint" value="${pager.recordPoint}"/>

<c:set var="goToPage">
    <c:url
            value='<%="/crud/"
						+ ESAPI.encoder().encodeForXMLAttribute(
								request.getParameter("action")) + ".action"%>'/>
</c:set>

<c:set var="lastPage">
    <pg:pager url="${goToPage}" items="${totalRecord}"
              maxPageItems="${recordPerPage}" maxIndexPages="0">
        <pg:index>
            <pg:last>${pageNumber }</pg:last>
        </pg:index>
    </pg:pager>
</c:set>

<script type="text/javascript">
    function gotoPage(page) {
        var isNum = /^\d+$/.test(page);
        var lastPage = "${lastPage}";
        if (!isNum) {
            page = "${currentPage}";
        } else {
            if (parseInt(page) < 1) {
                page = 1;
            } else if (parseInt(page) > parseInt(lastPage)) {
                page = parseInt(lastPage);
            }
        }

        var url = "${goToPage }" + "?pager.currentPage=" + page;
        $.ajax({
            url: url,
            success: function (result) {
                $("div.browse").html(result);
            }
        });
        $("body").scrollTop(0);
    }
</script>

<div class="page">
    <pg:pager url="${goToPage}" items="${totalRecord}"
              maxPageItems="${recordPerPage }" maxIndexPages="5">
        <pg:index>
            <pg:first>
                <c:choose>
                    <c:when test="${pageNumber eq currentPage}">
                        <a class="page_pp" href="#"><img
                                src="<%=request.getContextPath()%>/templates/images/page_pp.png"
                                width="30" height="22" align="absmiddle" alt="第一頁"></a>
                    </c:when>
                    <c:otherwise>
                        <a class="page_pp" href="#" onclick="gotoPage('${pageNumber}')"><img
                                src="<%=request.getContextPath()%>/templates/images/page_pp.png"
                                width="30" height="22" align="absmiddle" alt="第一頁"></a>
                    </c:otherwise>
                </c:choose>
            </pg:first>
            <pg:prev ifnull="true">
                <c:choose>
                    <c:when test="${empty pageNumber}">
                        <a class="page_p" href="#" onclick="return false;"><img
                                src="<%=request.getContextPath()%>/templates/images/page_p.png"
                                width="30" height="22" align="absmiddle" alt="上一頁"></a>
                    </c:when>
                    <c:otherwise>
                        <a class="page_p" href="#" onclick="gotoPage('${pageNumber}')"><img
                                src="<%=request.getContextPath()%>/templates/images/page_p.png"
                                width="30" height="22" align="absmiddle" alt="上一頁"></a>
                    </c:otherwise>
                </c:choose>
            </pg:prev>
            <pg:pages>
                <c:choose>
                    <c:when test="${pageNumber eq currentPage}">
                        <a class="page_num" href="#" onclick="return false;">
                                ${pageNumber} </a>
                    </c:when>
                    <c:otherwise>
                        <a class="page_num" href="#" onclick="gotoPage('${pageNumber}')">
                                ${pageNumber} </a>
                    </c:otherwise>
                </c:choose>
            </pg:pages>
            <pg:next ifnull="true">
                <c:choose>
                    <c:when test="${empty pageNumber}">
                        <a class="page_n" href="#" onclick="return false;"><img
                                src="<%=request.getContextPath()%>/templates/images/page_n.png"
                                width="30" height="22" align="absmiddle" alt="下一頁"></a>
                    </c:when>
                    <c:otherwise>
                        <a class="page_n" href="#" onclick="gotoPage('${pageNumber}')"><img
                                src="<%=request.getContextPath()%>/templates/images/page_n.png"
                                width="30" height="22" align="absmiddle" alt="下一頁"></a>
                    </c:otherwise>
                </c:choose>
            </pg:next>
            <pg:last>
                <c:choose>
                    <c:when test="${pageNumber eq currentPage}">
                        <a class="page_nn" href="#" onclick="return false;"><img
                                src="<%=request.getContextPath()%>/templates/images/page_nn.png"
                                width="30" height="22" align="absmiddle" alt="最後一頁"></a>
                    </c:when>
                    <c:otherwise>
                        <a class="page_nn" href="#" onclick="gotoPage('${pageNumber}')"><img
                                src="<%=request.getContextPath()%>/templates/images/page_nn.png"
                                width="30" height="22" align="absmiddle" alt="最後一頁"></a>
                    </c:otherwise>
                </c:choose>
            </pg:last>
        </pg:index>
    </pg:pager>
</div>