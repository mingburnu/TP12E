<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, maximum-scale=1, initial-scale=1">
    <meta http-equiv="expires" content="0">
    <title>臺北市十二年國民基本教育資訊網</title>
    <link rel="icon"
          href="<%=request.getContextPath()%>/templates/images/favicon.ico">
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/templates/jquery-ui.css">
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/templates/art.css">
</head>
<%
    if (request.getSession().getAttribute("login") != null) {
        response.sendRedirect(request.getContextPath()
                + "/page/main.action");
    }
%>
<body>
<div class="wrapper">

    <!-- 登入 區塊 Begin -->
    <div class="login">
        <div class="innerwrapper">
            <s:form action="login" namespace="/authorization" method="post">
                <table border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr valign="middle">
                        <th colspan="2"><img
                                src="<%=request.getContextPath()%>/templates/images/login_header.png"></th>
                    </tr>
                    <tr valign="middle">
                        <td align="right">您的帳號</td>
                        <td align="left"><input class="input_text v_username"
                                                name="user.userId" type="text"></td>
                    </tr>
                    <tr valign="middle">
                        <td align="right">您的密碼</td>
                        <td align="left"><input class="input_text v_password"
                                                name="user.passwd" type="password"></td>
                    </tr>
                    <tr valign="middle">
                        <td colspan="2">
                            <div class="login_bottom">
                                <a class="btn_01" onClick="form_submit();"> <span>登入</span></a>

                            </div>
                        </td>
                    </tr>
                </table>
            </s:form>

            <div class="login_footer">
                臺北市政府教育局 版權所有<BR/>Copyright &copy; 2016. All Rights Reserved.
            </div>
        </div>
    </div>
    <!-- 登入 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<script type="text/javascript"
        src="<%=request.getContextPath()%>/templates/jquery-1.11.3.min.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/templates/art.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //
    });

    function form_submit() {
        var msg = "";
        if ($(".v_username").val() == "") {
            msg += "．請輸入您的帳號。\r\n";
        }
        if ($(".v_password").val() == "") {
            msg += "．請輸入您的密碼。\r\n";
        }

        if (msg != "") {
            alert(msg);
        } else {
            document.getElementById('login').submit();
        }

    }
</script>
<jsp:include page="/WEB-INF/jsp/layout/msg.jsp"/>
<!-- 執行javascript 區塊 End -->
</body>
</html>