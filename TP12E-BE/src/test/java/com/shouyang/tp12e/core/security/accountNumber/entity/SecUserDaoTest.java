package com.shouyang.tp12e.core.security.accountNumber.entity;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.shouyang.tp12e.core.GenericTest;
import com.shouyang.tp12e.core.dao.DsRestrictions;
import com.shouyang.tp12e.core.service.ServiceFactory;
import com.shouyang.tp12e.module.apply.account.Account;
import com.shouyang.tp12e.module.apply.account.AccountDao;

/**
 * SecUserDaoTest
 * 
 * @author Roderick
 * @version 2014/9/29
 */
public class SecUserDaoTest extends GenericTest {

	@Autowired
	private AccountDao dao;

	@Test
	public void testCRUD() throws Exception {

		final String user1Code = "0123";
		final String user2Code = "0456";

		// Save user 1
		Account user1 = new Account();
		user1.setUserId(user1Code);

		Account dbUser1 = dao.save(user1);
		final Integer user1Id = dbUser1.getId();
		Assert.assertEquals(user1Code, dbUser1.getUserId());

		// Save user 2
		Account user2 = new Account();
		user2.setUserId(user2Code);

		Account dbUser2 = dao.save(user2);
		final Integer user2Id = dbUser2.getId();
		Assert.assertEquals(user2Code, dbUser2.getUserId());

		// Query by id
		dbUser1 = dao.findById(user1Id);
		Assert.assertEquals(user1Code, dbUser1.getUserId());

		// update
		final String user1UpdName = "Admin_test";
		boolean updated = true;
		try {
			dao.update(dbUser1);
		} catch (Exception e) {
			updated = false;
		}
		Assert.assertTrue(updated);

		// query by condition
		DsRestrictions restrictions = ServiceFactory.getDsRestrictions();
		restrictions.eq("userName", user1UpdName);
		List<Account> users = dao.findByRestrictions(restrictions);
		Assert.assertEquals(1, users.size());

		// delete by id
		boolean deleted = true;
		try {
			dao.deleteById(user1Id);
			dao.deleteById(user2Id);
		} catch (Exception e) {
			deleted = false;
		}
		Assert.assertTrue(deleted);
	}

}
