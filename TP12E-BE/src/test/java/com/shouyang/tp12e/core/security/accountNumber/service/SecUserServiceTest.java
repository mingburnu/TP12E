package com.shouyang.tp12e.core.security.accountNumber.service;

import java.util.List;

//


import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.shouyang.tp12e.core.GenericTest;
import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.module.apply.account.Account;
import com.shouyang.tp12e.module.apply.account.AccountService;

/**
 * SecUserServiceTest
 * 
 * @author David Hsu
 * @version 2014/3/11
 */
public class SecUserServiceTest extends GenericTest {

	@Autowired
	private AccountService service;

	@Autowired
	private DataSet<Account> ds;

	@Test
	public void testCRUD() throws Exception {

		final String user1Code = "0123";
		final String user2Code = "0456";

		// Save user 1
		Account user1 = new Account();
		user1.setUserId(user1Code);
		user1.setPasswd("test");

		Account dbUser1 = service.save(user1, user1);
		final Integer user1Id = dbUser1.getId();
		Assert.assertEquals(user1Code, dbUser1.getUserId());

		// Save user 2
		Account user2 = new Account();
		user2.setUserId(user2Code);
		user2.setPasswd("test");

		Account dbUser2 = service.save(user2, user1);
		final Integer user2Id = dbUser2.getId();
		Assert.assertEquals(user2Code, dbUser2.getUserId());

		// Query by id
		dbUser1 = service.getById(user1Id);
		Assert.assertEquals(user1Code, dbUser1.getUserId());

		// update
		dbUser1 = service.update(dbUser1, user1);

		// query by condition
		Account queryUser = new Account();
		ds.setEntity(queryUser);
		ds = service.getByRestrictions(ds);
		List<Account> users = ds.getResults();
		Assert.assertEquals(5, users.size());

		// delete by id
		boolean deleted = true;
		try {
			service.deleteById(user1Id);
			service.deleteById(user2Id);
		} catch (Exception e) {
			deleted = false;
		}
		Assert.assertTrue(deleted);

	}

}
