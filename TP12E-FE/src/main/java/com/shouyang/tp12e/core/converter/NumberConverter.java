package com.shouyang.tp12e.core.converter;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class NumberConverter extends RootConverter {

	@SuppressWarnings("rawtypes")
	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		try {
			if (NumberUtils.isCreatable(values[0])) {
				if (toClass.equals(Long.class)) {
					return Long.parseLong(values[0]);
				}

				if (toClass.equals(Integer.class)) {
					return Integer.parseInt(values[0]);
				}
			}
		} catch (NumberFormatException e) {
			return null;
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String convertToString(Map context, Object o) {

		if (o != null) {
			return o.toString();
		} else {
			return "";
		}
	}

}
