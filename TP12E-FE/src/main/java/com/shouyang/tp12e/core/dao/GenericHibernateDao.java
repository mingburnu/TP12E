package com.shouyang.tp12e.core.dao;

import com.shouyang.tp12e.core.entity.Entity;
import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.core.model.Pager;
import com.shouyang.tp12e.core.util.EntityUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.*;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * GenericHibernateDao
 *
 * @author Roderick
 * @version 2014/11/7
 */
public abstract class GenericHibernateDao<T extends Entity> extends
        GenericDao<T> {

    @Autowired
    private SessionFactory sessionFactory;

    private Class<T> entityClass;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    public GenericHibernateDao() {
        this.entityClass = null;
        Class<?> c = getClass();
        Type t = c.getGenericSuperclass();
        if (t instanceof ParameterizedType) {
            Type[] p = ((ParameterizedType) t).getActualTypeArguments();
            this.entityClass = (Class<T>) p[0];
        }
    }

    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    @Override
    public T findById(Integer id) throws Exception {
        Assert.notNull(id, "[Assertion failed] - this argument is required; it must not be null");
        return (T) getSession().byId(entityClass).load(id);
    }

    @Override
    public List<T> findByRestrictions(DsRestrictions restrictions)
            throws Exception {
        DataSet<T> results = findByRestrictions(restrictions, null);
        return results.getResults();
    }

    @SuppressWarnings("unchecked")
    @Override
    public DataSet<T> findByRestrictions(DsRestrictions restrictions,
                                         DataSet<T> ds) throws Exception {
        Assert.notNull(restrictions, "[Assertion failed] - this argument is required; it must not be null");
        Criteria dataCri = getSession().createCriteria(entityClass);
        Criteria countCri = getSession().createCriteria(entityClass);

        // createAlias
        Map<String, String> aliases = restrictions.getAliases();
        for (Entry<String, String> entry : aliases.entrySet()) {
            dataCri.createAlias(entry.getKey(), entry.getValue());
            countCri.createAlias(entry.getKey(), entry.getValue());
        }

        // add restrictions
        List<Criterion> crions = (List<Criterion>) restrictions.getCriterions();
        for (Criterion crion : crions) {
            countCri.add(crion);
            dataCri.add(crion);
        }

        // add orders
        List<Order> orders = (List<Order>) restrictions.getOrders();
        for (Order order : orders) {
            dataCri.addOrder(order);
        }

        if (ds != null && ds.getPager() != null) { // 分頁
            Pager pager = ds.getPager();

            // count total records
            countCri.setProjection(Projections.rowCount());
            Long totalRecord = (Long) countCri.list().get(0);
            log.debug("totalRecord:" + totalRecord);
            pager.setTotalRecord(totalRecord);

            dataCri.setFirstResult(pager.getOffset());
            dataCri.setMaxResults(pager.getRecordPerPage());
        } else {
            ds = new DataSet<>();
        }

        ds.setResults(dataCri.list());

        return ds;
    }

    @Override
    public T save(T entity) throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");
        EntityUtils.trimAllStrings(entity);
        Serializable id = getSession().save(entity);
        BeanUtils.setProperty(entity, "id", id);
        return entity;
    }

    @Override
    public void update(T entity) throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");
        EntityUtils.trimAllStrings(entity);
        getSession().update(entity);
    }

    @Override
    public void merge(T entity) throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");
        EntityUtils.trimAllStrings(entity);
        getSession().merge(entity);
    }

    @Override
    public void deleteById(Integer id) throws Exception {
        Assert.notNull(id, "[Assertion failed] - this argument is required; it must not be null");
        T t = findById(id);
        getSession().delete(t);
    }

    @Override
    public void delete(T entity) throws Exception {
        Assert.notNull(entity, "[Assertion failed] - this argument is required; it must not be null");
        getSession().delete(entity);
    }

    @Override
    public List<?> findByHQL(DsQueryLanguage dsQL) {
        Assert.notNull(dsQL, "[Assertion failed] - this argument is required; it must not be null");
        Assert.notNull(dsQL.getHql(), "[Assertion failed] - this argument is required; it must not be null");

        Query data = getSession().createQuery(dsQL.getHql());
        for (Entry<String, Object> keyValue : dsQL.getParameters().entrySet()) {
            data.setParameter(keyValue.getKey(), keyValue.getValue());
        }

        for (Entry<String, Collection<?>> keyValue : dsQL.getParameterLists()
                .entrySet()) {
            data.setParameterList(keyValue.getKey(), keyValue.getValue());
        }

        return data.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public DataSet<T> findByHQL(DsQueryLanguage dsQL, DataSet<T> ds) {
        Assert.notNull(dsQL, "[Assertion failed] - this argument is required; it must not be null");
        Assert.notNull(dsQL.getHql(), "[Assertion failed] - this argument is required; it must not be null");

        Query data = getSession().createQuery(dsQL.getHql());
        Query total = getSession().createQuery(dsQL.getHql());

        for (Entry<String, Object> keyValue : dsQL.getParameters().entrySet()) {
            data.setParameter(keyValue.getKey(), keyValue.getValue());
            total.setParameter(keyValue.getKey(), keyValue.getValue());
        }

        for (Entry<String, Collection<?>> keyValue : dsQL.getParameterLists()
                .entrySet()) {
            data.setParameterList(keyValue.getKey(), keyValue.getValue());
            total.setParameterList(keyValue.getKey(), keyValue.getValue());
        }

        if (ds != null && ds.getPager() != null) { // 分頁
            Pager pager = ds.getPager();

            // count total records
            Long totalRecord = (long) total.list().size();
            log.debug("totalRecord:" + totalRecord);
            pager.setTotalRecord(totalRecord);

            data.setFirstResult(pager.getOffset());
            data.setMaxResults(pager.getRecordPerPage());
        } else {
            ds = new DataSet<>();
        }

        ds.setResults(data.list());

        return ds;
    }

    @Override
    public void checkFK(Boolean constraint) {
        if (constraint) {
            // SET REFERENTIAL_INTEGRITY TRUE
            SQLQuery fkAble = getSession().createSQLQuery(
                    "SET FOREIGN_KEY_CHECKS=1");
            fkAble.executeUpdate();
        } else {
            // SET REFERENTIAL_INTEGRITY FALSE
            SQLQuery fkDisable = getSession().createSQLQuery(
                    "SET FOREIGN_KEY_CHECKS=0");
            fkDisable.executeUpdate();
        }

    }

}
