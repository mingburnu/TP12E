package com.shouyang.tp12e.core.entity;

import java.io.Serializable;

/**
 * Entity
 * @author Roderick
 * @version 2014/09/29
 */
public interface Entity extends Serializable {
	
}
