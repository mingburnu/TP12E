package com.shouyang.tp12e.core.filter;

//import com.shouyang.tp12e.core.interceptor.AsyncRequestInterceptor;

import com.shouyang.tp12e.core.interceptor.AsyncRequestInterceptor;
import com.shouyang.tp12e.module.apply.visitor.Visitor;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.*;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate5.SessionFactoryUtils;
import org.springframework.orm.hibernate5.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.async.WebAsyncManager;
import org.springframework.web.context.request.async.WebAsyncUtils;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Servlet Filter implementation class ClassNotFoundFilter
 */
public class CounterFilter extends OncePerRequestFilter {

    private static final String DEFAULT_SESSION_FACTORY_BEAN_NAME = "sessionFactory";

    private String sessionFactoryBeanName = DEFAULT_SESSION_FACTORY_BEAN_NAME;


    /**
     * Set the bean name of the SessionFactory to fetch from Spring's
     * root application context. Default is "sessionFactory".
     *
     * @see #DEFAULT_SESSION_FACTORY_BEAN_NAME
     */
    public void setSessionFactoryBeanName(String sessionFactoryBeanName) {
        this.sessionFactoryBeanName = sessionFactoryBeanName;
    }

    /**
     * Return the bean name of the SessionFactory to fetch from Spring's
     * root application context.
     */
    private String getSessionFactoryBeanName() {
        return this.sessionFactoryBeanName;
    }

    /**
     * Returns "false" so that the filter may re-bind the opened Hibernate
     * {@code Session} to each asynchronously dispatched thread and postpone
     * closing it until the very last asynchronous dispatch.
     */
    @Override
    protected boolean shouldNotFilterAsyncDispatch() {
        return false;
    }

    /**
     * Returns "false" so that the filter may provide a Hibernate
     * {@code Session} to each error dispatches.
     */
    @Override
    protected boolean shouldNotFilterErrorDispatch() {
        return false;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        SessionFactory sessionFactory = lookupSessionFactory(request);
        boolean participate = false;


        WebAsyncManager asyncManager = WebAsyncUtils.getAsyncManager(request);
        String key = getAlreadyFilteredAttributeName();

        if (TransactionSynchronizationManager.hasResource(sessionFactory)) {
            // Do not modify the Session: just set the participate flag.
            participate = true;
        } else {
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
            Browser[] nonHumans = {Browser.SILK, Browser.BOT, Browser.BOT_MOBILE, Browser.CFNETWORK, Browser.EVOLUTION, Browser.DOWNLOAD, Browser.UNKNOWN};
            Browser[] browsers = ArrayUtils.removeElements(Browser.values(), nonHumans);
            Browser browser = userAgent.getBrowser();

            logger.info("browser type : " + browser);
            boolean isBrowser = Arrays.asList(browsers).contains(browser);

            if (isBrowser) {
                logger.debug("Opening Hibernate Session in CounterFilter");
                Session session = openSession(sessionFactory);

                SessionHolder sessionHolder = new SessionHolder(session);
                TransactionSynchronizationManager.bindResource(sessionFactory, sessionHolder);

                AsyncRequestInterceptor interceptor = new AsyncRequestInterceptor(sessionFactory, sessionHolder);
                asyncManager.registerCallableInterceptor(key, interceptor);
                asyncManager.registerDeferredResultInterceptor(key, interceptor);

                if (request.getSession().isNew()) {
                    String remoteAddr = request.getHeader("X-FORWARDED-FOR");
                    if (StringUtils.isBlank(remoteAddr)) {
                        remoteAddr = request.getRemoteAddr();
                    }

                    session.save(new Visitor(request.getSession().getId(), remoteAddr, LocalDateTime.now()));
                }

                Criteria criteria = session.createCriteria(Visitor.class);
                criteria.setProjection(Projections.rowCount());
                request.setAttribute("totalVisitor", (Long) criteria.list().get(0) + 241196L);

                criteria.add(Restrictions.ge("visitTime", LocalDateTime.parse(LocalDateTime.now().minusDays(LocalDateTime.now().getDayOfMonth()).plusDays(1).toString().split("T")[0])));
                request.setAttribute("thisMonthVisitor", criteria.list().get(0));
            }
        }

        try {
            filterChain.doFilter(request, response);

        } finally {
            if (!participate) {
                SessionHolder sessionHolder =
                        (SessionHolder) TransactionSynchronizationManager.unbindResource(sessionFactory);
                if (!isAsyncStarted(request)) {
                    logger.debug("Closing Hibernate Session in CounterFilter");
                    SessionFactoryUtils.closeSession(sessionHolder.getSession());
                }
            }
        }
    }

    /**
     * Look up the SessionFactory that this filter should use,
     * taking the current HTTP request as argument.
     * <p>The default implementation delegates to the {@link #lookupSessionFactory()}
     * variant without arguments.
     *
     * @param request the current request
     * @return the SessionFactory to use
     */
    private SessionFactory lookupSessionFactory(HttpServletRequest request) {
        return lookupSessionFactory();
    }

    /**
     * Look up the SessionFactory that this filter should use.
     * <p>The default implementation looks for a bean with the specified name
     * in Spring's root application context.
     *
     * @return the SessionFactory to use
     * @see #getSessionFactoryBeanName
     */
    private SessionFactory lookupSessionFactory() {
        if (logger.isDebugEnabled()) {
            logger.debug("Using SessionFactory '" + getSessionFactoryBeanName() + "' for CounterFilter");
        }
        WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        return wac.getBean(getSessionFactoryBeanName(), SessionFactory.class);
    }

    /**
     * Open a Session for the SessionFactory that this filter uses.
     * <p>The default implementation delegates to the {@link SessionFactory#openSession}
     * method and sets the {@link Session}'s flush mode to "MANUAL".
     *
     * @param sessionFactory the SessionFactory that this filter uses
     * @return the Session to use
     * @throws DataAccessResourceFailureException if the Session could not be created
     * @see org.hibernate.FlushMode#MANUAL
     */
    private Session openSession(SessionFactory sessionFactory) throws DataAccessResourceFailureException {
        try {
            Session session = sessionFactory.openSession();
            session.setFlushMode(FlushMode.MANUAL);
            return session;
        } catch (HibernateException ex) {
            throw new DataAccessResourceFailureException("Could not open Hibernate Session", ex);
        }
    }

    private boolean applySessionBindingInterceptor(WebAsyncManager asyncManager, String key) {
        if (asyncManager.getCallableInterceptor(key) == null) {
            return false;
        }
        ((AsyncRequestInterceptor) asyncManager.getCallableInterceptor(key)).bindSession();
        return true;
    }

    private void testGooglebot(HttpServletRequest request) {
        String requestIp = request.getHeader("X-FORWARDED-FOR");
        if (StringUtils.isBlank(requestIp)) {
            requestIp = request.getRemoteAddr();
        }

        String userAgent = request.getHeader("User-Agent");
        logger.info("User Agent : " + userAgent);

        if (!StringUtils.isEmpty(userAgent)) {

            if (userAgent.toLowerCase().contains("googlebot")) {

                //check fake user agent
                String command = "cmd /c " + "nslookup " + requestIp;
                logger.info("Command : " + command);
                String output = executeCommand("cmd /c " + "nslookup " + requestIp);
                logger.info("Output : " + output);

                if (output.toLowerCase().contains("googlebot.com")) {
                    logger.info("This is Google bot");
                } else {
                    logger.info("This is fake user agent");
                }

            } else {
                logger.info("Not from Google");
            }
        }
    }

    private String executeCommand(String command) {

        StringBuilder output = new StringBuilder();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();
    }
}
