package com.shouyang.tp12e.core.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.shouyang.tp12e.module.apply.visitor.Visitor;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 儲存檢索log
 * 
 * @author Roderick
 * @version 2015/5/16
 */
public class SearchActionInterceptor extends RootInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5107110044080126585L;

	@Autowired
	private Visitor visitor;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		removeErrorParameters(invocation);

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpServletResponse response = ServletActionContext.getResponse();

		if (invocation.getProxy().getNamespace().equals("/crud")) {
			if (!isUsableMethod(invocation)) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return "list";
			}
		}

		return invocation.invoke();
	}
}
