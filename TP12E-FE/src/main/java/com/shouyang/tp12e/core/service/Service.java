package com.shouyang.tp12e.core.service;

import com.shouyang.tp12e.core.entity.Entity;
import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.module.apply.account.Account;

/**
 * Service
 * 
 * @author Roderick
 * @version 2014/3/11
 */
public interface Service<T extends Entity> {

	public T save(T entity, Account user) throws Exception;

	public T getById(Integer id) throws Exception;

	public DataSet<T> getByRestrictions(DataSet<T> ds) throws Exception;

	public T update(T entity, Account user, String... ignoreProperties)
			throws Exception;

	public void deleteById(Integer id) throws Exception;

}
