package com.shouyang.tp12e.core.web;

import com.shouyang.tp12e.core.entity.Entity;

/**
 * Action
 * 
 * @author Roderick
 * @version 2014/12/11
 */
public interface Action<T extends Entity> {

	public final static String INDEX = "index";

	public final static String MORE = "more";

	public final static String LIST = "list";

	public final static String VIEW = "view";

	public final static String DOC = "doc";

	public final static String PIC = "pic";

}
