package com.shouyang.tp12e.module.apply.attachment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.shouyang.tp12e.core.entity.GenericEntity;
import com.shouyang.tp12e.module.apply.enums.Category;
import com.shouyang.tp12e.module.apply.news.News;

/**
 * 使用者
 * 
 * @author Roderick
 * @version 2016/04/25
 */
@Entity
@Table(name = "attachment")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Attachment extends GenericEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5804311089729989927L;

	/**
	 * 檔案名稱
	 */
	@Column(name = "name")
	private String name;

	/**
	 * 檔案連結
	 */
	@Column(name = "url")
	private String url;

	/**
	 * 檔案路徑
	 */
	@Column(name = "path")
	private String path;

	/**
	 * 置頂
	 */
	@Column(name = "top")
	private Boolean top;

	/**
	 * 發佈範圍
	 */
	@Column(name = "category")
	@Enumerated(EnumType.STRING)
	private Category category;

	@ManyToOne
	@JoinColumn(name = "news_id", nullable = false)
	private News news;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the top
	 */
	public Boolean getTop() {
		return top;
	}

	/**
	 * @param top
	 *            the top to set
	 */
	public void setTop(Boolean top) {
		this.top = top;
	}

	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * @param news
	 *            the news to set
	 */
	public void setNews(News news) {
		this.news = news;
	}

	public Attachment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Attachment(String name, String url, String path, Boolean top,
			Category category, News news) {
		super();
		this.name = name;
		this.url = url;
		this.path = path;
		this.top = top;
		this.category = category;
		this.news = news;
	}
}
