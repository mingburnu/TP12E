package com.shouyang.tp12e.module.apply.attachment;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.core.web.GenericWebAction;
import com.shouyang.tp12e.module.apply.news.News;
import com.shouyang.tp12e.module.apply.news.NewsService;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;

/**
 * 使用者
 * 
 * @author Roderick
 * @version 2015/5/9
 */
@Controller
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AttachmentAction extends GenericWebAction<Attachment> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9198667697775718131L;

	@Autowired
	private Attachment attachment;

	@Autowired
	private AttachmentService attachmentService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private News news;

	@Override
	protected void validateSave() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	protected void validateUpdate() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	protected void validateDelete() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public String add() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String edit() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String update() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String more() throws Exception {
		if (getEntity().getCategory() == null) {
			getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
		} else {
			DataSet<Attachment> ds = attachmentService
					.getByRestrictions(initDataSet());

			if (ds.getResults().size() == 0
					&& ds.getPager().getCurrentPage() > 1) {
				Double lastPage = Math.ceil(ds.getPager().getTotalRecord()
						.doubleValue()
						/ ds.getPager().getRecordPerPage().doubleValue());
				ds.getPager().setCurrentPage(lastPage.intValue());
				attachmentService.getByRestrictions(ds);
			}
		}

		return MORE;
	}

	public String download() throws Exception {
		if (hasEntity()) {
			if (StringUtils.isNotBlank(attachment.getPath())) {
				File file = new File(attachment.getPath());
				FileInputStream fis = new FileInputStream(file);

				UserAgent userAgent = UserAgent
						.parseUserAgentString(getRequest().getHeader(
								"User-Agent"));

				if (userAgent.getBrowser().getGroup().equals(Browser.IE)
						|| userAgent.getBrowser().getGroup()
								.equals(Browser.EDGE)) {
					getEntity().setReportFile(
							URLEncoder.encode(file.getName(), "UTF8")
									.replaceAll("\\+", "%20"));
				} else {
					getEntity().setReportFile(
							new String(file.getName().getBytes("UTF8"),
									"ISO8859-1"));
				}

				getEntity().setInputStream(fis);
			} else {
				getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
			}
		} else {
			getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
		}

		return DOC;
	}

	protected boolean hasEntity() throws Exception {
		if (getEntity().getId() == null) {
			getEntity().setId(-1);
			return false;
		}

		attachment = attachmentService.getById(getEntity().getId());
		if (attachment == null) {
			return false;
		}

		return true;
	}

	protected boolean hasNews() throws Exception {
		if (getEntity().getNews() == null) {
			getEntity().setNews(new News());
			getEntity().getNews().setId(-1);
			return false;
		}

		if (getEntity().getNews().getId() == null) {
			getEntity().setId(-1);
			return false;
		}

		news = newsService.getById(getEntity().getNews().getId());
		if (news == null) {
			return false;
		}

		return true;
	}
}
