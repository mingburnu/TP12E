package com.shouyang.tp12e.module.apply.attachment;

import com.shouyang.tp12e.core.dao.DsRestrictions;
import com.shouyang.tp12e.core.dao.GenericDao;
import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.core.model.Pager;
import com.shouyang.tp12e.core.service.GenericService;
import com.shouyang.tp12e.module.apply.enums.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

/**
 * 使用者 Service
 *
 * @author Roderick
 * @version 2014/9/30
 */
@Service
public class AttachmentService extends GenericService<Attachment> {

    @Autowired
    private AttachmentDao dao;

    @Override
    protected GenericDao<Attachment> getDao() {
        return dao;
    }

    /**
     * 登入取得帳號資料
     *
     * @param ds
     * @return
     * @throws Exception
     */
    @Override
    public DataSet<Attachment> getByRestrictions(DataSet<Attachment> ds)
            throws Exception {
        Assert.notNull(ds, "[Assertion failed] - this argument is required; it must not be null");
        Assert.notNull(ds.getEntity(), "[Assertion failed] - this argument is required; it must not be null");

        Attachment entity = ds.getEntity();
        DsRestrictions restrictions = getDsRestrictions();
        ds.getPager().setRecordPerPage(Integer.MAX_VALUE);

        if (entity.getCategory() != null) {
            restrictions.eq("category", entity.getCategory());
            restrictions.createAlias("news", "news");

            restrictions.addOrderDesc("top");
            restrictions.addOrderDesc("news.publishTime");
        } else {
            return ds;
        }

        return dao.findByRestrictions(restrictions, ds);
    }

    public List<Attachment> getHomeAttachments(Category category)
            throws Exception {
        DataSet<Attachment> ds = new DataSet<>();
        ds.setPager(new Pager());
        ds.getPager().setRecordPerPage(Integer.MAX_VALUE);

        DsRestrictions restrictions = getDsRestrictions();
        restrictions.eq("category", category);
        restrictions.eq("top", true);
        restrictions.createAlias("news", "news");
        restrictions.addOrderAsc("news.publishTime");

        return dao.findByRestrictions(restrictions, ds).getResults();
    }
}
