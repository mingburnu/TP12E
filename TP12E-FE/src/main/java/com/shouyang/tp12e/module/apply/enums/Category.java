package com.shouyang.tp12e.module.apply.enums;

/**
 * Action
 * 
 * @author Roderick
 * @version 2015/01/19
 */
public enum Category {
	/**
	 * Logs
	 */
	簡章下載("簡章下載"),

	簡報講綱("簡報講綱");

	private String category;

	private Category() {
	}

	private Category(String category) {
		this.category = category;
	}

	/**
	 * @return the action
	 */
	public String getCategory() {
		return category;
	}

}
