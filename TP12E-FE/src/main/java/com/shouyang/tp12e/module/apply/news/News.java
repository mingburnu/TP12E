package com.shouyang.tp12e.module.apply.news;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.shouyang.tp12e.core.entity.GenericEntity;
import com.shouyang.tp12e.module.apply.attachment.Attachment;
import com.shouyang.tp12e.module.apply.enums.NewsClass;

/**
 * 使用者
 * 
 * @author Roderick
 * @version 2016/04/25
 */
@Entity
@Table(name = "news")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class News extends GenericEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5804311089729989927L;

	/**
	 * 標題
	 */
	@Column(name = "title")
	private String title;

	/**
	 * 公告時間
	 */
	@Column(name = "publish_time")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime publishTime;

	/**
	 * 內容
	 */
	@Column(name = "content")
	@Type(type = "text")
	private String content;

	/**
	 * 類別
	 */
	@Column(name = "news_class")
	@Enumerated(EnumType.STRING)
	private NewsClass newsClass;

	@OneToMany(mappedBy = "news", orphanRemoval = true)
	private Set<Attachment> attachments;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the publishTime
	 */
	public LocalDateTime getPublishTime() {
		return publishTime;
	}

	/**
	 * @param publishTime
	 *            the publishTime to set
	 */
	public void setPublishTime(LocalDateTime publishTime) {
		this.publishTime = publishTime;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the newsClass
	 */
	public NewsClass getNewsClass() {
		return newsClass;
	}

	/**
	 * @param newsClass
	 *            the newsClass to set
	 */
	public void setNewsClass(NewsClass newsClass) {
		this.newsClass = newsClass;
	}

	/**
	 * @return the attachments
	 */
	public Set<Attachment> getAttachments() {
		return attachments;
	}

	public News() {
		super();
		// TODO Auto-generated constructor stub
	}

	public News(String title, LocalDateTime publishTime, String content,
			NewsClass newsClass) {
		super();
		this.title = title;
		this.publishTime = publishTime;
		this.content = content;
		this.newsClass = newsClass;
	}

}
