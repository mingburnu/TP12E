package com.shouyang.tp12e.module.apply.news;

import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.core.web.GenericWebAction;
import com.shouyang.tp12e.module.apply.attachment.Attachment;
import com.shouyang.tp12e.module.apply.attachment.AttachmentService;
import com.shouyang.tp12e.module.apply.enums.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 使用者
 *
 * @author Roderick
 * @version 2014/9/29
 */
@Controller
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NewsAction extends GenericWebAction<News> {

    /**
     *
     */
    private static final long serialVersionUID = 9198667697775718131L;

    @Autowired
    private News news;

    @Autowired
    private NewsService newsService;

    @Autowired
    private Attachment attachment;

    @Autowired
    private AttachmentService attachmentService;

    @Override
    protected void validateSave() throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected void validateUpdate() throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected void validateDelete() throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public String add() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String edit() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String list() throws Exception {
        DataSet<News> ds = newsService.getByRestrictions(initDataSet());

        if (ds.getResults().size() == 0 && ds.getPager().getCurrentPage() > 1) {
            Double lastPage = Math.ceil(ds.getPager().getTotalRecord()
                    .doubleValue()
                    / ds.getPager().getRecordPerPage().doubleValue());
            ds.getPager().setCurrentPage(lastPage.intValue());
            newsService.getByRestrictions(ds);
        }

        List<Attachment> brochures = attachmentService.getHomeAttachments(
                Category.簡章下載);
        List<Attachment> briefings = attachmentService.getHomeAttachments(
                Category.簡報講綱);

        getRequest().setAttribute("brochures", brochures);
        getRequest().setAttribute("briefings", briefings);

        return LIST;
    }

    @Override
    public String save() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String update() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String delete() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    public String more() throws Exception {
        DataSet<News> ds = newsService.getByMore(initDataSet());

        if (ds.getResults().size() == 0 && ds.getPager().getCurrentPage() > 1) {
            Double lastPage = Math.ceil(ds.getPager().getTotalRecord()
                    .doubleValue()
                    / ds.getPager().getRecordPerPage().doubleValue());
            ds.getPager().setCurrentPage(lastPage.intValue());
            newsService.getByMore(ds);
        }

        return MORE;
    }

    public String view() throws IOException, Exception {
        if (hasEntity()) {
            log.info(news.getAttachments());
            setEntity(news);
        } else {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        return VIEW;
    }

    protected boolean hasEntity() throws Exception {
        if (getEntity().getId() == null) {
            getEntity().setId(-1);
            return false;
        }

        news = newsService.getById(getEntity().getId());
        if (news == null) {
            return false;
        }

        return true;
    }
}
