package com.shouyang.tp12e.module.apply.news;

import com.shouyang.tp12e.core.dao.DsRestrictions;
import com.shouyang.tp12e.core.dao.GenericDao;
import com.shouyang.tp12e.core.model.DataSet;
import com.shouyang.tp12e.core.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * 使用者 Service
 *
 * @author Roderick
 * @version 2014/9/30
 */
@Service
public class NewsService extends GenericService<News> {

    @Autowired
    private NewsDao dao;

    @Override
    public DataSet<News> getByRestrictions(DataSet<News> ds) throws Exception {
        Assert.notNull(ds, "[Assertion failed] - this argument is required; it must not be null");
        Assert.notNull(ds.getEntity(), "[Assertion failed] - this argument is required; it must not be null");

        DsRestrictions restrictions = getDsRestrictions();
        restrictions.addOrderDesc("publishTime");

        ds.getPager().setRecordPerPage(7);
        ds.getPager().setCurrentPage(1);

        return dao.findByRestrictions(restrictions, ds);
    }

    public DataSet<News> getByMore(DataSet<News> ds) throws Exception {
        Assert.notNull(ds, "[Assertion failed] - this argument is required; it must not be null");
        Assert.notNull(ds.getEntity(), "[Assertion failed] - this argument is required; it must not be null");

        DsRestrictions restrictions = getDsRestrictions();
        restrictions.addOrderDesc("publishTime");

        ds.getPager().setRecordPerPage(Integer.MAX_VALUE);

        return dao.findByRestrictions(restrictions, ds);
    }

    @Override
    protected GenericDao<News> getDao() {
        return dao;
    }
}
