<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="esapi"
	uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />
<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->


		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<c:choose>
			<c:when test='${entity.category == "簡章下載"}'>
				<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
					<jsp:param name="position"
						value='<%=request.getAttribute(
							"javax.servlet.forward.request_uri").toString()%>' />
					<jsp:param name="aName" value="簡章下載" />
				</jsp:include></c:when>
			<c:when test='${entity.category == "簡報講綱"}'>
				<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
					<jsp:param name="position"
						value='<%=request.getAttribute(
							"javax.servlet.forward.request_uri").toString()%>' />
					<jsp:param name="aName" value="簡報講綱" />
				</jsp:include></c:when>
		</c:choose>

		<!-- crumbs 區塊 End -->


		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><c:choose>
								<c:when test='${entity.category == "簡章下載"}'>
									<jsp:include page="/WEB-INF/jsp/layout/listBox.jsp">
										<jsp:param value="4" name="hover" />
									</jsp:include></c:when>
								<c:when test='${entity.category == "簡報講綱"}'><jsp:include
										page="/WEB-INF/jsp/layout/listBox.jsp">
										<jsp:param value="5" name="hover" />
									</jsp:include></c:when>
							</c:choose></td>
						<td class="layout_04_02"><c:choose>
								<c:when test='${entity.category == "簡章下載"}'>
									<jsp:include page="/WEB-INF/jsp/layout/subListBox.jsp">
										<jsp:param value="9" name="title" />
										<jsp:param value="" name="hover" />
									</jsp:include></c:when>

								<c:when test='${entity.category == "簡報講綱"}'>
									<jsp:include page="/WEB-INF/jsp/layout/subListBox.jsp">
										<jsp:param value="14" name="title" />
										<jsp:param value="" name="hover" />
									</jsp:include></c:when>
							</c:choose>

							<div class="catalog_content_box">
								<div class="catalog_file_list">
									<div class="cf_Lv01_list">
										<c:forEach items="${ds.results }" var="item">
											<c:choose>
												<c:when test="${not empty item.url }">
													<a class="download_icon" target="_blank"
														href="${item.url }" title="${item.name }">●
														${item.name }</a>
												</c:when>
												<c:otherwise>
													<a class="download_icon" target="_blank"
														href="${pageContext.request.contextPath}/attachment/download/${item.id }"
														title="${item.name }">● ${item.name }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</div>
								</div>
							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->

			</div>
		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />
	<!-- 執行javascript 區塊 End -->
</body>
</html>
