<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="esapi"
           uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>
<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header_home 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="0"/>
    </jsp:include>
    <!-- header_home 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp"/>
    <!-- crumbs 區塊 End -->


    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：內容區塊" class="layout_03">
                <tr valign="top">
                    <td class="layout_03_01">
                        <!-- 主題選單 區塊 Begin -->
                        <jsp:include
                                page="/WEB-INF/jsp/layout/topicMenu.jsp"/> <!-- 主題選單 區塊 End -->
                    </td>
                    <td class="layout_03_02">
                        <!-- 最新消息 區塊 Begin -->
                        <div class="news_home">
                            <div class="title">
                                <img src="${pageContext.request.contextPath}/templates/images/titles_02.png"
                                     width="100" height="20" alt="最新消息">
                                <a class="more_btn" href="${pageContext.request.contextPath}/news/more">
                                    <img src="${pageContext.request.contextPath}/templates/images/btn_20.png"
                                         width="85" height="20" alt="更多消息"></a>
                            </div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                   summary="最新消息列表" class="news_home_table">
                                <c:forEach var="item" items="${ds.results}" varStatus="status">
                                    <tr valign="top">
                                        <td class="news_home_td_01">${fn:replace(fn:split(item.publishTime, 'T')[0],'-','/')}</td>
                                        <td class="news_home_td_02"><span>${item.newsClass }</span></td>
                                        <td class="news_home_td_03"><a
                                                href="${pageContext.request.contextPath}/news/view/${item.id }"><esapi:encodeForHTML>${item.title }</esapi:encodeForHTML></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div> <!-- 最新消息 區塊 End -->
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- 主內容 區塊 End -->

    <c:set var="brochures">
        <c:forEach items="${brochures }" var="item">
            <div>
                <c:choose>
                    <c:when test="${not empty item.url }">
                        <a target="_blank" href="${item.url }">●
                            <esapi:encodeForHTML>${item.name }</esapi:encodeForHTML></a>
                    </c:when>
                    <c:when test="${not empty item.path }">
                        <a target="_blank"
                           href="${pageContext.request.contextPath}/attachment/download/${item.id}">●
                            <esapi:encodeForHTML>${item.name }</esapi:encodeForHTML>
                        </a>
                    </c:when>
                </c:choose>

            </div>
        </c:forEach>
    </c:set>
    <c:set var="briefings">
        <c:forEach items="${briefings }" var="item">
            <div>
                <c:choose>
                    <c:when test="${not empty item.url }">
                        <a target="_blank" href="${item.url }">●
                            <esapi:encodeForHTML>${item.name }</esapi:encodeForHTML></a>
                    </c:when>
                    <c:when test="${not empty item.path }">
                        <a target="_blank"
                           href="${pageContext.request.contextPath}/attachment/download/${item.id}">●
                            <esapi:encodeForHTML>${item.name }</esapi:encodeForHTML>
                        </a>
                    </c:when>
                </c:choose>

            </div>
        </c:forEach>
    </c:set>

    <!-- 選單列表 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/menuList.jsp">
        <jsp:param name="home" value="1"/>
        <jsp:param name="brochures" value="${brochures}"/>
        <jsp:param name="briefings" value="${briefings}"/>
    </jsp:include>
    <!-- 選單列表 區塊 End -->


    <!-- 相關連結 區塊 Begin -->
    <div class="link">
        <div class="innerwrapper">
            <div class="title">
                <img src="${pageContext.request.contextPath}/templates/images/titles_05.png"
                     width="100" height="20" alt="相關連結">
            </div>
            <div class="link_box">
                <a target="_blank" href="http://12basic.tp.edu.tw">臺北市十二年國教資訊網</a>
                <a target="_blank" href="http://12basic.edu.tw">教育部十二年國教資訊網</a>
                <a target="_blank" href="http://expo.tp.edu.tw/">臺北市107年度高中職網路博覽會</a>
                <a target="_blank" href="http://adapt.k12ea.gov.tw/">107年國中畢業生適性入學宣導網站</a>
                <a target="_blank" href="http://cweb.saihs.edu.tw/web/skillcenter/">臺北市技藝教育資訊網</a>
                <a target="_blank" href="https://school.tp.edu.tw/Login.action?l=tp">臺北市國中學生生涯領航儀表板（二代校務行政系統）</a>
                <a target="_blank" href="http://cap.ntnu.edu.tw">國中教育會考網站</a>
                <a target="_blank" href="http://ttk.entry.edu.tw">107年基北區免試入學網站</a>
                <a target="_blank"
                   href="https://107priorefa.tp.edu.tw/NoExamImitate_TP/NoExamImitateAB/Apps/Page/Public/News.aspx">臺北市107學年度優先免試入學網站</a>
            </div>
        </div>
    </div>
    <!-- 相關連結 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>
<script type="text/javascript">
    $(document).ready(function () {
        //
    });
</script>
<!-- 執行javascript 區塊 End -->
</body>
</html>