<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="esapi"
           uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>
<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position"
                   value='<%=request.getAttribute("javax.servlet.forward.request_uri")
					.toString()%>'/>
        <jsp:param name="aName" value="消息列表"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->


    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 消息列表 區塊 Begin -->
            <div class="news">
                <div class="title">
                    <img src="${pageContext.request.contextPath}/templates/images/titles_03.png"
                         width="100" height="20" alt="消息列表">
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                       summary="消息列表" class="news_table">
                    <c:forEach var="item" items="${ds.results}" varStatus="status">
                        <tr valign="top">
                            <td class="news_td_01">${fn:replace(fn:split(item.publishTime, 'T')[0],'-','/')}</td>
                            <td class="news_td_02"><span>${item.newsClass }</span></td>
                            <td class="news_td_03"><a
                                    href="${pageContext.request.contextPath}/news/view/${item.id }"><esapi:encodeForHTML>${item.title }</esapi:encodeForHTML></a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <!-- 消息列表 區塊 End -->

        </div>
    </div>
    <!-- 主內容 區塊 End -->

    <!-- 選單列表 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/menuList.jsp">
        <jsp:param name="home" value="0"/>
    </jsp:include>
    <!-- 選單列表 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>
<!-- 執行javascript 區塊 End -->
</body>
</html>