<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="esapi"
	uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API"%>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />
<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->


		<!-- header_home 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header_home 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="news/more" />
			<jsp:param name="aName" value="消息列表" />
			<jsp:param name="subPosition"
				value='<%=request.getAttribute("javax.servlet.forward.request_uri")
					.toString()%>' />
			<jsp:param name="subAName" value="${entity.title }" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 消息詳細內容 區塊 Begin -->
				<div class="news_detail">
					<div class="news_detail_date">${fn:replace(fn:split(entity.publishTime, 'T')[0],'-','/')}</div>
					<div class="news_detail_title">${entity.title }</div>
					<div class="news_detail_content">
						<esapi:encodeForHTML>${entity.content }</esapi:encodeForHTML>
					</div>
					<div class="file_list">
						<c:forEach items="${entity.attachments }" var="item">
							<ul>
								<li><c:choose>
										<c:when test="${not empty item.url }">
											<a target="_blank" href="${item.url }" title="${item.name }">●
												${item.name }</a>
										</c:when>
										<c:otherwise>
											<a target="_blank"
												href="${pageContext.request.contextPath}/attachment/download/${item.id }"
												title="${item.name }">● ${item.name }</a>
										</c:otherwise>
									</c:choose></li>

							</ul>
						</c:forEach>
					</div>
				</div>
				<!-- 消息詳細內容 區塊 End -->

			</div>
		</div>
		<!-- 主內容 區塊 End -->


		<!-- 選單列表 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/menuList.jsp">
			<jsp:param name="home" value="0" />
		</jsp:include>
		<!-- 選單列表 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />
	<script type="text/javascript">
		
	</script>

	<!-- 執行javascript 區塊 End -->
</body>
</html>