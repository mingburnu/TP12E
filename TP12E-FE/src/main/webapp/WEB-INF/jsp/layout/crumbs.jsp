<%@page import="org.owasp.esapi.ESAPI"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="esapi"
	uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API"%>

<c:choose>
	<c:when test="${(empty param.position) && (empty param.subPosition)  }">
		<div class="crumbs">
			<div class="innerwrapper">
				<div class="crumbs_box">
					<a accesskey="C"
						href="${pageContext.request.contextPath}/sitemap.jsp"
						title="主要內容區"> :::</a> <span>現在位置：</span> <a class="crumbs_home"
						href="${pageContext.request.contextPath}/">首頁</a>
				</div>
			</div>
		</div>
	</c:when>
	<c:when
		test="${(not empty param.position) && (empty param.subPosition) }">
		<div class="crumbs">
			<div class="innerwrapper">
				<div class="crumbs_box">
					<a accesskey="C"
						href="${pageContext.request.contextPath}/sitemap.jsp"
						title="主要內容區">:::</a> <span>現在位置：</span> <a class="crumbs_home"
						href="${pageContext.request.contextPath}/">首頁</a> &gt; <a
						href="<%=ESAPI.encoder().encodeForHTMLAttribute(
							request.getParameter("position"))%>"><%=ESAPI.encoder().encodeForHTML(
							request.getParameter("aName"))%></a>
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div class="crumbs">
			<div class="innerwrapper">
				<div class="crumbs_box">
					<a accesskey="C"
						href="${pageContext.request.contextPath}/sitemap.jsp"
						title="主要內容區">:::</a> <span>現在位置：</span> <a class="crumbs_home"
						href="${pageContext.request.contextPath}/">首頁</a> &gt; <a
						href="${pageContext.request.contextPath}/<%=ESAPI.encoder().encodeForHTMLAttribute(
							request.getParameter("position"))%>"><%=ESAPI.encoder().encodeForHTML(
							request.getParameter("aName"))%></a> &gt; <a
						href="<%=ESAPI.encoder().encodeForHTMLAttribute(
							request.getParameter("subPosition"))%>"><%=ESAPI.encoder().encodeForHTML(
							request.getParameter("subAName"))%></a>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>