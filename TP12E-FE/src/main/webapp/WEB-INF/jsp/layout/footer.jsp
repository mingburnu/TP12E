<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="footer">
    <div class="innerwrapper">
        <table width="100%" border="0" cellspacing="0" cellpadding="0"
               summary="版型表格：頁尾區塊" class="layout_02">
            <tr valign="top">
                <td class="layout_02_01"><a
                        href="http://www.handicap-free.nat.gov.tw/Applications/Detail?category=20140319092543"
                        title="無障礙網站"> <img
                        src="${pageContext.request.contextPath}/templates/images/aplus.jpg"
                        width="88" height="31" alt="通過A+優先等級無障礙網頁檢測"></a></td>
                <td class="layout_02_02">
                    <div class="footer_03">
                        累計瀏覽人次：${pageContext.request.getAttribute("totalVisitor")}
                        <BR/>
                        本月累計人數：${pageContext.request.getAttribute("thisMonthVisitor")}
                        <BR/>
                        十二年國教諮詢專線：02-27668812
                    </div>
                    <div class="footer_01">
                        臺北市政府教育局 地址：11008 臺北市市府路一號8F <a target="_blank"
                                                        href="http://www.edunet.taipei.gov.tw/content.asp?Cuitem=1288454&amp;mp=104001">本局電話分機一覽表</a>&nbsp;&nbsp;&nbsp;
                        <a target="_blank"
                           href="https://www.gov.taipei/News_Content.aspx?n=BD1A977D89DE972B&sms=AA987E1C50412097&s=6ED537C23771820B">隱私權政策</a>&nbsp;&nbsp;&nbsp;
                        <a target="_blank"
                           href="http://www.doe.taipei.gov.tw/ct.asp?xItem=1187337&amp;ctNode=37341&amp;mp=104001">著作權聲明</a>&nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="footer_02">
                        臺北市政府教育局 版權所有 Copyright &copy; 2017. All Rights Reserved.
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>