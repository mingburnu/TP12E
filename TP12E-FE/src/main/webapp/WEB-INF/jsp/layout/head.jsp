<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, maximum-scale=1, initial-scale=1">
<meta http-equiv="expires" content="0">
<meta name="_globalsign-domain-verification" content="3sQmtPS544fNDYQISm1dGy7lZXrCqo6wVqGoDYtS-g" />
<title>臺北市十二年國民基本教育資訊網</title>
<link rel="icon"
	href="${pageContext.request.contextPath}/templates/images/favicon.ico">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/templates/art.css">
</head>
<style type='text/css'>
input.gsc-input,.gsc-input-box,.gsc-input-box-hover,.gsc-input-box-focus
	{
	border-color: #333333;
	margin: 0;
	padding: 0;
	font-size: 12px;
	line-height: 15px;
}

input.gsc-search-button,input.gsc-search-button:hover,input.gsc-search-button:focus
	{
	border-color: #666666;
	background-color: #333333;
	background-image: none;
	filter: none;
	margin: 0;
	padding: 6px 10px;
	font-size: 12px;
	line-height: 15px;
}
</style>