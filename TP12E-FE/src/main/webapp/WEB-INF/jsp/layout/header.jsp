<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${0 eq param.detail }">
        <div class="header_home">
            <div class="innerwrapper">
                <div class="banner_box">&nbsp;</div>
            </div>
        </div>
    </c:when>

    <c:when test="${1 eq param.detail }">
        <div class="header">
            <div class="innerwrapper">
                <div class="banner_box">
                    <div class="subject_link">
                        <a href="${pageContext.request.contextPath}/bctest.jsp">
                            <img src="${pageContext.request.contextPath}/templates/images/btn_21.png"
                                 width="140" height="104" alt="國中教育會考"></a>
                        <a href="${pageContext.request.contextPath}/link.jsp">
                            <img src="${pageContext.request.contextPath}/templates/images/btn_08.png"
                                 width="140" height="104" alt="相關連結"></a>
                        <a href="${pageContext.request.contextPath}/law.jsp">
                            <img src="${pageContext.request.contextPath}/templates/images/btn_09.png"
                                 width="140" height="104" alt="相關法規"></a>
                        <a href="${pageContext.request.contextPath}/faq.jsp">
                            <img src="${pageContext.request.contextPath}/templates/images/btn_10.png"
                                 width="140" height="104" alt="常見問答集"></a>
                    </div>
                </div>
            </div>
        </div>

    </c:when>
</c:choose>
