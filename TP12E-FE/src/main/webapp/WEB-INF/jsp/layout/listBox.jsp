<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="catalog_list_box">
    <c:choose>
        <c:when test="${1 eq param.hover }">
            <a href="${pageContext.request.contextPath}/set_01.jsp" title="目標及配套">
                <img src="${pageContext.request.contextPath}/templates/images/btn_15_hover.png"
                     width="190" height="100" alt="目標及配套"></a>
        </c:when>
        <c:otherwise>
            <a href="${pageContext.request.contextPath}/set_01.jsp" title="目標及配套">
                <img src="${pageContext.request.contextPath}/templates/images/btn_15.png"
                     width="190" height="100" alt="目標及配套"></a>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${2 eq param.hover }">
            <a href="${pageContext.request.contextPath}/start_school_01.jsp" title="入學方式">
                <img src="${pageContext.request.contextPath}/templates/images/btn_16_hover.png"
                     width="190" height="100" alt="入學方式"></a>
        </c:when>
        <c:otherwise>
            <a href="${pageContext.request.contextPath}/start_school_01.jsp" title="入學方式">
                <img src="${pageContext.request.contextPath}/templates/images/btn_16.png"
                     width="190" height="100" alt="入學方式"></a>
        </c:otherwise>
    </c:choose>

    <c:choose>
        <c:when test="${3 eq param.hover }">
            <a href="${pageContext.request.contextPath}/seek_01.jsp" title="宣導諮詢">
                <img src="${pageContext.request.contextPath}/templates/images/btn_17_hover.png"
                     width="190" height="100" alt="宣導諮詢"></a>
        </c:when>
        <c:otherwise>
            <a href="${pageContext.request.contextPath}/seek_01.jsp" title="宣導諮詢">
                <img src="${pageContext.request.contextPath}/templates/images/btn_17.png"
                     width="190" height="100" alt="宣導諮詢"></a>
        </c:otherwise>
    </c:choose>

    <c:choose>
        <c:when test="${4 eq param.hover }">
            <a href="${pageContext.request.contextPath}/attachment/more/brochures" title="簡章下載">
                <img src="${pageContext.request.contextPath}/templates/images/btn_18_hover.png"
                     width="190" height="100" alt="簡章下載"></a>
        </c:when>
        <c:otherwise>
            <a href="${pageContext.request.contextPath}/attachment/more/brochures" title="簡章下載">
                <img src="${pageContext.request.contextPath}/templates/images/btn_18.png"
                     width="190" height="100" alt="簡章下載"></a>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${5 eq param.hover }">
            <a href="${pageContext.request.contextPath}/attachment/more/briefings" title="簡報講綱">
                <img src="${pageContext.request.contextPath}/templates/images/btn_19_hover.png"
                     width="190" height="100" alt="簡報講綱"></a>
        </c:when>
        <c:otherwise>
            <a href="${pageContext.request.contextPath}/attachment/more/briefings" title="簡報講綱">
                <img src="${pageContext.request.contextPath}/templates/images/btn_19.png"
                     width="190" height="100" alt="簡報講綱"></a>
        </c:otherwise>
    </c:choose>
</div>