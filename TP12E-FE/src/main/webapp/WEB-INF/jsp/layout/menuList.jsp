<%@page import="org.owasp.esapi.ESAPI" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${0 eq param.home }">
        <div class="catalog">
            <div class="innerwrapper">
                <div class="title">
                    <img src="${pageContext.request.contextPath}/templates/images/titles_04.png"
                         width="100" height="20" alt="選單列表">
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <a href="${pageContext.request.contextPath}/set_01.jsp">
                            <img src="${pageContext.request.contextPath}/templates/images/catalog_icon_A.png"
                                 width="150" height="100" alt="目標及配套"></a>
                    </div>
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <a href="${pageContext.request.contextPath}/start_school_01.jsp">
                            <img src="${pageContext.request.contextPath}/templates/images/catalog_icon_B.png"
                                 width="150" height="100" alt="入學方式"></a>
                    </div>
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <a href="${pageContext.request.contextPath}/seek_01.jsp">
                            <img src="${pageContext.request.contextPath}/templates/images/catalog_icon_C.png"
                                 width="150" height="100" alt="宣導諮詢"></a>
                    </div>
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <a href="${pageContext.request.contextPath}/attachment/more/brochures">
                            <img src="${pageContext.request.contextPath}/templates/images/catalog_icon_D.png"
                                 width="150" height="100" alt="簡章下載"></a>
                    </div>
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <a href="${pageContext.request.contextPath}/attachment/more/briefings">
                            <img src="${pageContext.request.contextPath}/templates/images/catalog_icon_E.png"
                                 width="150" height="100" alt="簡報講綱"></a>
                    </div>
                </div>

            </div>
        </div>
    </c:when>
    <c:when test="${1 eq param.home }">
        <div class="catalog">
            <div class="innerwrapper">
                <div class="title">
                    <img src="${pageContext.request.contextPath}/templates/images/titles_04.png"
                         width="100" height="20" alt="選單列表">
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <img src="${pageContext.request.contextPath}/templates/images/catalog_icon_A.png"
                             width="150" height="100" alt="目標及配套">
                    </div>
                    <div class="catalog_list">
                        <div>
                            <a href="${pageContext.request.contextPath}/set_01.jsp">●
                                目標策略</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/set_02.jsp">●
                                辦理時程（重要日程）</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/set_03.jsp">●
                                國中活化教學</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/set_04.jsp">●
                                高中職領先計畫</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/set_05.jsp">●
                                課程與教學領導</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/set_06.jsp">●
                                教師專業學習社群</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/set_07.jsp">●
                                學習共同體試辦</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/set_08.jsp">●
                                素養評量工作坊</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/set_09.jsp">●
                                跨國參訪報告</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/set_10.jsp">●
                                學生生涯輔導</a>
                        </div>
                    </div>
                    <a class="btn_07"
                       href="${pageContext.request.contextPath}/set_01.jsp">更多...</a>
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <img src="${pageContext.request.contextPath}/templates/images/catalog_icon_B.png"
                             width="150" height="100" alt="入學方式">
                    </div>
                    <div class="catalog_list">
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_01.jsp">●
                                基北區免試入學</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_02.jsp">●
                                臺北市優先免試入學</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_03.jsp">●
                                直升入學</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_04.jsp">●
                                技優甄審入學</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_05.jsp">●
                                實用技能學程</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_06.jsp">●
                                特色招生考試分發</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_07.jsp">●
                                科學班</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_08.jsp">●
                                體育班及運動績優</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_09.jsp">●
                                專業群科（高職）</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/start_school_10.jsp">●
                                藝術才能班</a>
                        </div>
                    </div>
                    <a class="btn_07"
                       href="${pageContext.request.contextPath}/start_school_01.jsp">更多...</a>
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <img src="${pageContext.request.contextPath}/templates/images/catalog_icon_C.png"
                             width="150" height="100" alt="宣導諮詢">
                    </div>
                    <div class="catalog_list">
                        <div>
                            <a href="${pageContext.request.contextPath}/seek_01.jsp">●
                                各入學管道主委學校</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/seek_02.jsp">●
                                服務學習</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/seek_03.jsp">●
                                超額比序</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/seek_04.jsp">●
                                免學費政策</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/seek_05.jsp">●
                                高中職及五專學校</a>
                        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/seek_06.jsp">●
                                志願地圖</a>
                        </div>
                        <div>
                            <a href="https://ttk.entry.edu.tw/NoExamImitate_TP/NoExamImitate/Apps/Page/Public/01/EnrollmentInquiry.aspx"
                               target="_blank">● 招生資訊查詢</a>
                        </div>
                    </div>
                    <a class="btn_07"
                       href="${pageContext.request.contextPath}/seek_01.jsp">更多...</a>
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <img
                                src="${pageContext.request.contextPath}/templates/images/catalog_icon_D.png"
                                width="150" height="100" alt="簡章下載">
                    </div>
                    <div class="catalog_list">
                        <%=ESAPI.encoder().decodeForHTML(
                                request.getParameter("brochures"))%>
                    </div>
                    <a class="btn_07"
                       href="${pageContext.request.contextPath}/attachment/more/brochures">更多...</a>
                </div>

                <div class="catalog_box">
                    <div class="catalog_icon">
                        <img
                                src="${pageContext.request.contextPath}/templates/images/catalog_icon_E.png"
                                width="150" height="100" alt="簡報講綱">
                    </div>
                    <div class="catalog_list"><%=ESAPI.encoder().decodeForHTML(
                            request.getParameter("briefings"))%>
                    </div>
                    <a class="btn_07"
                       href="${pageContext.request.contextPath}/attachment/more/briefings">更多...</a>
                </div>

            </div>
        </div>
    </c:when>
</c:choose>