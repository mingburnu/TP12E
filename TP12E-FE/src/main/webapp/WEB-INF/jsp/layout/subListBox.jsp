<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${13 eq param.title  }">
        <div class="title">
            <img src="${pageContext.request.contextPath}/templates/images/titles_13.png"
                 width="100" height="20" alt="目標及配套">
        </div>
    </c:when>
    <c:when test="${7 eq param.title  }">
        <div class="title">
            <img src="${pageContext.request.contextPath}/templates/images/titles_07.png"
                 width="100" height="20" alt="入學方式">
        </div>
    </c:when>
    <c:when test="${8 eq param.title  }">
        <div class="title">
            <img src="${pageContext.request.contextPath}/templates/images/titles_08.png"
                 width="100" height="20" alt="宣導諮詢">
        </div>
    </c:when>
    <c:when test="${16 eq param.title  }">
        <div class="title">
            <img src="${pageContext.request.contextPath}/templates/images/titles_16.png"
                 width="100" height="20" alt="簡章下載">
        </div>
    </c:when>
    <c:when test="${17 eq param.title  }">
        <div class="title">
            <img src="${pageContext.request.contextPath}/templates/images/titles_17.png"
                 width="100" height="20" alt="簡報講綱">
        </div>
    </c:when>
</c:choose>

<div class="catalog_sublist_box">
    <c:choose>
        <c:when test="${13 eq param.title  }">
            <c:choose>
                <c:when test="${1 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_01.jsp"
                       title="目標策略" class="hover">目標策略</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_01.jsp"
                       title="目標策略">目標策略</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${2 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_02.jsp"
                       title="辦理時程（重要日程）" class="hover">辦理時程（重要日程）</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_02.jsp"
                       title="辦理時程（重要日程）">辦理時程（重要日程）</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${3 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_03.jsp"
                       title="國中活化教學" class="hover">國中活化教學</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_03.jsp"
                       title="國中活化教學">國中活化教學</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${4 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_04.jsp"
                       title="高中職領先計畫" class="hover">高中職領先計畫</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_04.jsp"
                       title="高中職領先計畫">高中職領先計畫</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${5 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_05.jsp"
                       title="課程與教學領導" class="hover">課程與教學領導</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_05.jsp"
                       title="課程與教學領導">課程與教學領導</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${6 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_06.jsp"
                       title="教師專業學習社群" class="hover">教師專業學習社群</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_06.jsp"
                       title="教師專業學習社群">教師專業學習社群</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${7 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_07.jsp"
                       title="學習共同體試辦" class="hover">學習共同體試辦</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_07.jsp"
                       title="學習共同體試辦">學習共同體試辦</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${8 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_08.jsp"
                       title="素養評量工作坊" class="hover">素養評量工作坊</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_08.jsp"
                       title="素養評量工作坊">素養評量工作坊</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${9 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_09.jsp"
                       title="跨國參訪報告" class="hover">跨國參訪報告</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_09.jsp"
                       title="跨國參訪報告">跨國參訪報告</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${10 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/set_10.jsp"
                       title="學生生涯輔導" class="hover">學生生涯輔導</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/set_10.jsp"
                       title="學生生涯輔導">學生生涯輔導</a>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:when test="${7 eq param.title  }">
            <c:choose>
                <c:when test="${1 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_01.jsp"
                       title="基北區免試入學" class="hover">基北區免試入學</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_01.jsp"
                       title="基北區免試入學">基北區免試入學</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${2 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_02.jsp"
                       title="臺北市優先免試入學" class="hover">臺北市優先免試入學</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_02.jsp"
                       title="臺北市優先免試入學">臺北市優先免試入學</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${3 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_03.jsp"
                       title="直升入學" class="hover">直升入學</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_03.jsp"
                       title="直升入學">直升入學</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${4 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_04.jsp"
                       title="技優甄審入學" class="hover">技優甄審入學</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_04.jsp"
                       title="技優甄審入學">技優甄審入學</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${5 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_05.jsp"
                       title="實用技能學程" class="hover">實用技能學程</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_05.jsp"
                       title="實用技能學程">實用技能學程</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${6 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_06.jsp"
                       title="特色招生考試分發" class="hover">特色招生考試分發</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_06.jsp"
                       title="特色招生考試分發">特色招生考試分發</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${7 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_07.jsp"
                       title="科學班" class="hover">科學班</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_07.jsp"
                       title="科學班">科學班</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${8 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_08.jsp"
                       title="體育班及運動績優" class="hover">體育班及運動績優</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_08.jsp"
                       title="體育班及運動績優">體育班及運動績優</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${9 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_09.jsp"
                       title="專業群科（高職）" class="hover">專業群科（高職）</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_09.jsp"
                       title="專業群科（高職）">專業群科（高職）</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${10 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/start_school_10.jsp"
                       title="藝術才能班" class="hover">藝術才能班</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/start_school_10.jsp"
                       title="藝術才能班">藝術才能班</a>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:when test="${8 eq param.title  }">
            <c:choose>
                <c:when test="${1 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/seek_01.jsp"
                       title="各入學管道主委學校" class="hover">各入學管道主委學校</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/seek_01.jsp"
                       title="各入學管道主委學校">各入學管道主委學校</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${2 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/seek_02.jsp"
                       title="服務學習" class="hover">服務學習</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/seek_02.jsp"
                       title="服務學習">服務學習</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${3 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/seek_03.jsp"
                       title="超額比序" class="hover">超額比序</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/seek_03.jsp"
                       title="超額比序">超額比序</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${4 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/seek_04.jsp"
                       title="免學費政策" class="hover">免學費政策</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/seek_04.jsp"
                       title="免學費政策">免學費政策</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${5 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/seek_05.jsp"
                       title="臺北市高中職及五專學校" class="hover">臺北市高中職及五專學校</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/seek_05.jsp"
                       title="臺北市高中職及五專學校">臺北市高中職及五專學校</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${6 eq param.hover }">
                    <a href="${pageContext.request.contextPath}/seek_06.jsp"
                       title="志願地圖" class="hover">志願地圖</a>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/seek_06.jsp"
                       title="志願地圖">志願地圖</a>
                </c:otherwise>
            </c:choose>
            <a href="https://ttk.entry.edu.tw/NoExamImitate_TP/NoExamImitate/Apps/Page/Public/01/EnrollmentInquiry.aspx" title="招生資訊查詢"
               target="_blank">招生資訊查詢</a>
        </c:when>
        <c:when test="${9 eq param.title  }">
            <div class="title">
                <img src="${pageContext.request.contextPath}/templates/images/titles_09.png"
                     width="100" height="20" alt="簡章下載">
            </div>
        </c:when>
        <c:when test="${14 eq param.title  }">
            <div class="title">
                <img src="${pageContext.request.contextPath}/templates/images/titles_14.png"
                     width="100" height="20" alt="簡報講綱">
            </div>
        </c:when>
    </c:choose>
</div>

