<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="top">
    <div class="innerwrapper">
        <table width="100%" border="0" cellspacing="0" cellpadding="0"
               summary="版型表格：上方選單區塊" class="layout_01">
            <tr>
                <td class="layout_01_01">
                    <div class="logo_box">
                        <a class="btn_01" href="${pageContext.request.contextPath}/">
                            <img src="${pageContext.request.contextPath}/templates/images/logo.png"
                                 width="360" height="70"></a>
                    </div>
                </td>
                <td class="layout_01_02">
                    <div class="nav_box">
                        <a accesskey="U"
                           href="${pageContext.request.contextPath}/sitemap.jsp"
                           title="上方選單區">:::</a> | <a
                            href="${pageContext.request.contextPath}/">首頁</a> | <a
                            href="${pageContext.request.contextPath}/sitemap.jsp">網站導覽</a> |
                        <a target="_blank" class="btn_02"
                           href="https://ttk.entry.edu.tw/NoExamImitate_TP/NoExamImitate/Apps/Page/Public/01/EnrollmentInquiry.aspx"><span>招生資訊查詢</span></a>
                        |

                    </div>
                </td>
                <td class="layout_01_03">
                    <div class="search_box">
                        <div id='cse-search-form' style='width: 100%;'>Loading</div>
                        <script src='//www.google.com/jsapi' type='text/javascript'></script>
                        <script type='text/javascript'>
                            google.load('search', '1', {
                                language: 'zh-Hant',
                                style: google.loader.themes.V2_DEFAULT
                            });
                            google
                                    .setOnLoadCallback(
                                            function () {
                                                var customSearchOptions = {};
                                                var orderByOptions = {};
                                                orderByOptions['keys'] = [{
                                                    label: 'Relevance',
                                                    key: ''
                                                }, {
                                                    label: 'Date',
                                                    key: 'date'
                                                }];
                                                customSearchOptions['enableOrderBy'] = true;
                                                customSearchOptions['orderByOptions'] = orderByOptions;
                                                var customSearchControl = new google.search.CustomSearchControl(
                                                        '000522422006796848665:lihbkxrkwf4',
                                                        customSearchOptions);
                                                customSearchControl
                                                        .setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
                                                var options = new google.search.DrawOptions();
                                                options
                                                        .enableSearchboxOnly(
                                                                'https://www.google.com/cse?cx=000522422006796848665:lihbkxrkwf4',
                                                                null, true);
                                                options.setAutoComplete(true);
                                                customSearchControl.draw(
                                                        'cse-search-form',
                                                        options);
                                            }, true);
                        </script>

                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<style type="text/css">
    input.gsc-input, .gsc-input-box, .gsc-input-box-hover, .gsc-input-box-focus {
        border-color: #333333;
        margin: 0;
        padding: 0;
        font-size: 12px;
        line-height: 15px;
    }

    input.gsc-search-button, input.gsc-search-button:hover, input.gsc-search-button:focus {
        border-color: #666666;
        background-color: #333333;
        background-image: none;
        filter: none;
        margin: 0;
        padding: 6px 10px;
        font-size: 12px;
        line-height: 15px;
    }
</style>