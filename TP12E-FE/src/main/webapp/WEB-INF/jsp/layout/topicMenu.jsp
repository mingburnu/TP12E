<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="subject_home">
    <div class="title">
        <img src="${pageContext.request.contextPath}/templates/images/titles_01.png" width="100" height="20" alt="主題選單">
    </div>
    <div class="subject_link">
        <a href="${pageContext.request.contextPath}/bctest.jsp">
            <img src="${pageContext.request.contextPath}/templates/images/btn_22.png"
                 width="160" height="40" alt="國中教育會考"> </a>
        <a href="${pageContext.request.contextPath}/link.jsp">
            <img src="${pageContext.request.contextPath}/templates/images/btn_04.png"
                 width="160" height="40" alt="相關連結"></a>
        <a href="${pageContext.request.contextPath}/law.jsp">
            <img src="${pageContext.request.contextPath}/templates/images/btn_05.png"
                 width="160" height="40" alt="相關法規"></a>
        <a href="${pageContext.request.contextPath}/faq.jsp">
            <img src="${pageContext.request.contextPath}/templates/images/btn_06.png"
                 width="160" height="40" alt="常見問答集"></a>
    </div>
</div>