<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="aName" value="國中教育會考"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <div class="main_link">
                <div class="title">
                    <img src="${pageContext.request.contextPath}/templates/images/titles_15.png"
                         width="150" height="20" alt="國中教育會考">
                </div>

                <div class="catalog_content">
                    <div class="cc_Lv01_table">
                        <table width="100%" border="0" cellspacing="1" cellpadding="0"
                               class="cc_table_01">
                            <caption>國中教育會考各考試科目之時間及題數</caption>
                            <tr>
                                <th colspan="2">考試科目</th>
                                <th>時間</th>
                                <th>題數</th>
                                <th width="200">結果呈現</th>
                            </tr>
                            <tr>
                                <th colspan="2">國文</th>
                                <td>70分鐘</td>
                                <td>45～50題</td>
                                <td rowspan="6">分為「精熟」、「基礎」和「待加強」3
                                    等級成績計算方式可詳見教育部國中教育會考網站：http://cap.ntnu.edu.tw/
                                </td>
                            </tr>
                            <tr>
                                <th rowspan="2">英語</th>
                                <th>閱讀</th>
                                <td>60分鐘</td>
                                <td>閱讀40～45 題（占總成績80%）</td>
                            </tr>
                            <tr>
                                <th>聽力</th>
                                <td>25分鐘</td>
                                <td>20 ～ 30題（占總成績20%）</td>
                            </tr>
                            <tr>
                                <th colspan="2">數學</th>
                                <td>80分鐘</td>
                                <td>27～33題 <br/> 選擇題25～30題（占總成績85%） <br/>
                                    非選擇題2～3題（占總成績15%）
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">社會</th>
                                <td>70分鐘</td>
                                <td>60～70題</td>
                            </tr>
                            <tr>
                                <th colspan="2">自然</th>
                                <td>70分鐘</td>
                                <td>50～60題</td>
                            </tr>
                            <tr>
                                <th colspan="2">寫作測驗</th>
                                <td>50分鐘</td>
                                <td>1題</td>
                                <td>分為一至六級分</td>
                            </tr>
                        </table>
                    </div>
                    <div class="cc_Lv01_txt_b">※「寫作測驗」及「數學非選擇題」必須要用黑色墨水筆作答。</div>

                    <BR/>
                    <div class="cc_Lv01_table">
                        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                            <caption>
                                107年國中教育會考考試日期和時間
                            </caption>
                            <tr>
                                <th>&nbsp;</th>
                                <th colspan="2">107年5月19日（六）</th>
                                <th colspan="2">107年5月20日（日）</th>
                            </tr>
                            <tr>
                                <th rowspan="8">上午</th>
                                <td>08：20～08：30</td>
                                <td>考試說明</td>
                                <td>08：20～08：30</td>
                                <td>考試說明</td>
                            </tr>
                            <tr>
                                <td>08：30～09：40</td>
                                <td>社會</td>
                                <td>08：30～09：40</td>
                                <td>自然</td>
                            </tr>
                            <tr>
                                <td>09：40～10：20</td>
                                <td>休息</td>
                                <td>09：40～10：20</td>
                                <td>休息</td>
                            </tr>
                            <tr>
                                <td>10：20～10：30</td>
                                <td>考試說明</td>
                                <td>10：20～10：30</td>
                                <td>考試說明</td>
                            </tr>
                            <tr>
                                <td rowspan="4">10：30～11：50</td>
                                <td rowspan="4">數學</td>
                                <td>10：30～11：30</td>
                                <td>英語（閱讀）</td>
                            </tr>
                            <tr>
                                <td>11：30～12：00</td>
                                <td>休息</td>
                            </tr>
                            <tr>
                                <td>12：00～12：05</td>
                                <td>考試說明</td>
                            </tr>
                            <tr>
                                <td>12：05～12：30</td>
                                <td>英語（聽力）</td>
                            </tr>
                            <tr>
                                <th rowspan="5">下午</th>
                                <td>13：40～13：50</td>
                                <td>考試說明</td>
                                <td colspan="2" rowspan="5">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>13：50～15：00</td>
                                <td>國文</td>
                            </tr>
                            <tr>
                                <td>15：00～15：40</td>
                                <td>休息</td>
                            </tr>
                            <tr>
                                <td>15：40～15：50</td>
                                <td>考試說明</td>
                            </tr>
                            <tr>
                                <td>15：50～16：40</td>
                                <td>寫作測驗</td>
                            </tr>
                        </table>
                    </div>

                    <BR/>

                    <div class="cc_Lv01_title">國中教育會考評量結果</div>
                    <div class="cc_Lv01_txt">
                        1.各科評量結果分為「精熟」、「基礎」及「待加強」3
                        個等級。整體而言，成績「精熟」表示學生精通熟習該科目國中階段所學習的知識與能力；「基礎」表示學生具備該科目國中階段之基本學力；「待加強」表示學生尚未具備該科目國中教育階段之基本學力。<BR/>
                        <BR/>
                        2.三等級加註標示：為解決升學競爭較激烈區域免試超額時的抽籤問題，各科在維持三等級計分標準下，在精熟（A）等級前50%，分別標示A++（精熟級前25%）及A+（精熟級前26%～50%），並在基礎（B）前50%
                        分別標示B++（基礎級前25%）及B+（基礎級前26%～50%）。
                    </div>
                    <div class="cc_Lv01_txt_b">
                        重要提醒：<BR/>
                        1.107年6月8日公布會考成績，並可上網查詢。<BR/>
                        2.107年6月21日-6月28日提供個人序位區間查詢（個人序位區間百分比以 0.3% 為一區間）。
                    </div>

                </div>

                <div class="link_box">
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/臺北考區107年國中教育會考簡章.pdf"
                       title="臺北考區107年國中教育會考簡章">● 臺北考區107年國中教育會考簡章</a>
                </div>

            </div>


            <!-- 內容 區塊 End -->

        </div>
    </div>
    <!-- 主內容 區塊 End -->

    <!-- 選單列表 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/menuList.jsp">
        <jsp:param name="home" value="0"/>
    </jsp:include>
    <!-- 選單列表 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>