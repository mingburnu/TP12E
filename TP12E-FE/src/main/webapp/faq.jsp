<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="aName" value="常見問答集"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <div class="main_faq">
                <div class="title">
                    <img src="${pageContext.request.contextPath}/templates/images/titles_12.png"
                         width="100" height="20" alt="常見問答集">
                </div>
                <div class="faq_title">免試入學</div>

                <div class="faq_box">
                    <div class="faq_q">問題：所有國中畢業生都可以參加免試入學申請嗎?</div>
                    <div class="faq_a">是的。所有國中畢業生（包含非應屆畢業生
                        、歸國學生或同等學力者）都可報名參加高級中等學校及五專免試入學。「免試入學」係指國中畢業生不必參加任何入學考試，就可向其所屬免試就學區內任一學校申請入學。
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：107學年度高級中等學校免試入學和之前的免試入學管道有何不同?</div>
                    <div class="faq_a">1.自103 學年度起，配合十二年國民基本教育之實施，免試入學停止參酌或採計國中學生學習領域評量。<br>
                        2.學生依國中適性輔導建議、個人性向、興趣及能力，原則上於就讀或畢業之國中所在地之免試就學區，選擇適合高級中等學校或五專。<br>
                        3.各高級中等學校及五專辦理免試入學，不得訂定入學門檻或條件。 特色招生考試分發入學得訂定入學門檻或條件。（請特別注意）<br>
                        4.免試入學的分發方式是由報名學生上網選填志願，每生依志願序以錄取 1
                        個校（科）為限。如各高級中等學校（科）報名人數未超過招生名額時，全額錄取；當報名人數超過招生名額時，依照「超額比序項目積分對照表」換算積分以進行比序分發。<br>
                        5.107學年度免試入學與特色招生考試分發入學報名與分發一次到位。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：所有的高級中等學校、五專都參與免試入學嗎?如何得知相關資訊?</div>
                    <div class="faq_a">
                        1. 基北區免試委員會網站：<a href="https://ttk.entry.edu.tw/"
                                         target="_blank">https://ttk.entry.edu.tw/</a><br>
                        2. 新北市十二年國教資訊網站：<a href="http://12basic.ntpc.edu.tw/" target="_blank">http://12basic.ntpc.edu.tw/</a><br>
                        3. 臺北市十二年國教資訊網站：<a href="http://12basic.tp.edu.tw/"
                                           target="_blank">http://12basic.tp.edu.tw/</a><br>
                        4. 基隆市十二年國教資訊網站：<a href="http://12basic.kl.edu.tw/"
                                           target="_blank">http://12basic.kl.edu.tw/</a><br>
                        5. 107年度國中畢業生適性入學宣導網站：<a href="http://adapt.k12ea.gov.tw/" target="_blank">http://adapt.k12ea.gov.tw</a><br>
                        6. 教育部十二年國教資訊網站：<a href="http://12basic.edu.tw/" target="_blank">http://12basic.edu.tw</a>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：107學年度基北區高級中等學校免試入學承辦學校為何?</div>
                    <div class="faq_a">
                        1.107學年度基北區高級中等學校免試入學委員會(以下簡稱本委員會)主委學校為臺北市立松山高級中學。協辦學校有：<br>
                        臺北市立木柵高級工業職業學校（負責分發作業、兼售簡章）。<br>
                        <p>2.本委員會專線（02）2753-5968 轉 222、229，網址為 <a href="https://ttk.entry.edu.tw/" target="_blank">https://ttk.entry.edu.tw/</a>，<br>
                            傳真（02）2766-2458，校址：11070臺北市信義區基隆路1段156號。</p>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：107學年度基北區高級中等學校免試入學網站（https://ttk.entry.edu.tw/）提供哪方面訊息或服務?</div>
                    <div class="faq_a">
                        1.免試入學簡章電子檔案網路下載<br>
                        2.免試入學實際招生名額之公告<br>
                        3.學生志願選填系統之入口網站<br>
                        4.各項免試入學管道連結、最新訊息公告及常見問答
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：免試入學的就學區是如何劃分的?</div>
                    <div class="faq_a">1.十二年國民基本教育的學區規劃是以直轄市、縣（市）行政區為基礎，全國共分為15個免試就學區，其中基隆市、臺北市、新北市合稱為｢基北區｣。國中學生若畢
                        業於此三市，即可參加基北區免試入學。<br>
                        <p>2.桃園市蘆竹區、龜山區、八德區與新北市之林口區
                            、樹林區、鶯歌區、新莊區及泰山區，因共同生活圈、交通便利性等因素，經相關縣（市）主管機關協調劃定為本區之共同就學區，得跨區就近入學。<br>
                        <p>3.新北市貢寮區、雙溪區及坪林區，因共同生活圈、交通便利性等因素，經相關縣（市）主管機關協調，所屬國中學生得列入宜蘭區 107 學年度共同就學區範圍。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：107學年度免試入學招生對象為何?</div>
                    <div class="faq_a">
                        1.本區及共同就學區各公私立國民中學（含高級中等學校附設國民中學部）畢業或具同等學力資格者，及海外設立之臺灣學校（含大陸臺商子女(弟)學校）國中部設籍於本區之畢業或具同等學力資格者。<br>
                        <p>2.獲准變更就學區者。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：完全中學是否仍保留直升入學?</div>
                    <div class="faq_a">是的。 關於本區提供直升入學之學校列表，請參閱簡章。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：107學年度基北區免試入學名額是如何規劃的?</div>
                    <div class="faq_a">1.107學年度基北區免試入學名額（含直升入學）比例應達全區核定招生總名額 80％
                        以上。基北區各高中高職學校均有提供免試入學名額。各校所提供之名額均依《高級中等學校多元入學招生辦法》辦理。各校提供之招生科別及名額，會於簡章中詳細載明。<br>
                        <p>
                            2.免試入學之前，會先進行技優甄審、產業特殊需求類科等入學管道招生作業(需具特殊資格且不採計國中教育會考成績)；後續進行需採計國中教育會考成績的優先免試入學及直升入學，前述各管道招生若有餘額時，將流用至免試入學(不含私立技優名額)。因此，免試入學實際招生名額可能高於簡章所列名額，本委員會於
                            107 年 6 月 21 日 會另行公告實際招生名額，請參閱<a
                                href="https://ttk.entry.edu.tw/">https://ttk.entry.edu.tw/</a>之最新消息。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：考生是否能跨區就學?應如何辦理?</div>
                    <div class="faq_a">因學生所屬免試就學區係依該生畢業國中所在地作認定，非本區國中畢業學生如欲參與本區免試入學，須辦理變更就學區作業，申請資訊如下：</p>
                        <p><strong>（一）申請對象與資格：</strong><br>
                            非本區國中畢業生（含非學校型態實驗教育學生）或非本區具同等學力資格者或東莞、華東、上海臺商子女(弟)學校國中部非設籍於本區之畢業生如有下列特殊因素之一者，得向本委員會提出變更就學區之申請：<br>
                            1.學生就讀或畢業國中學籍所在免試就學區，未設置學生適性選擇之高級中等學校課程群別或產業特殊需求類科者。<br>
                            2.學生因搬家遷徙至本區居住者（檢附具體證明文件）。<br>
                            3.學生在國中階段跨區就學，擬申請返回本區戶籍所在地就讀高級中等學校者（檢附具體證明文件）。<br>
                            4.其他經核定之特殊情形。自大陸地區及其他國家返國就學學生無論是否設籍於基北區，皆須向本委員會提出
                            變更就學區之申請，惟非設籍於基北區之學生仍須符合上述之特殊因素之一者，經審核通過後始可參加本區免試入學管道。</p>
                        <p><strong>（二）申請方式：</strong><br>
                            1.應屆畢業生（含非學校型態實驗教育學生）：<br>
                            由申請學生於 107 年 4 月 30 日（星期一）上午 9 時至 5 月 4 日（星期五）下午 4 時上網填寫 變更就學區申請表（詳見簡章附表 1，第
                            115-117頁）後，列印申請表並由申請學生及家長雙方（或監護人）簽章，檢附相關證明文件向就讀學校提出申請，並由其就讀學校彙整各學生之申請表及相關證明文件後，於 107 年 5 月 8
                            日（星期二）前函送臺北市立松山高級中學（11070臺北市信義區基隆路1段156號，聯絡電話：02-2753-5968轉 229、222）提出申請，審查結果於 107 年 5 月 21
                            日（星期一）前以書面方式通知，並公告於本委員會網頁。申請網址：<a
                                    href="https://ttk.entry.edu.tw/">https://ttk.entry.edu.tw/</a>（由本委員會網站提供連結至國立臺灣師範大學心理與教育測驗研究發展中心之變更就學區申請系統平臺）<br>
                            <br>
                            2.非應屆畢業生及其他需變更就學區者：<br>
                            由申請人於 107 年 4 月 30 日（星期一）上午 9 時至 5 月 4 日（星期五）下午 4 時上網填寫變更就學區申請表（詳見簡章附表 1，第 115-117
                            頁）後，列印申請表並由申請人及家長雙方（或監護人）簽章，檢附相關證明文件，於 107 年 5 月 8
                            日（星期二）前掛號郵寄或親送至臺北市立松山高級中學（11070臺北市信義區基隆路1段156號，聯絡電話：02-2753-5968轉 229、222）提出申請，審查結果於 107年 5
                            月 21 日（星期一）前以書面方式通知，並公告於本委員會網頁。申請網址：<a href="https://ttk.entry.edu.tw/">https://ttk.entry.edu.tw/</a>（由本委員會網站提供連結至國立臺灣師範大學心理與教育測驗研究發展中心之變更就學區申請系統平臺）
                        </p>
                        <p><strong>（三）申請變更就學區者同時於 107 年 4 月 30 日（星期一）上午 9 時至 5 月 11 日（星期 五）下午 4
                            時必須完成多元學習表現成績審查之申請，申請方式如下：</strong><br>
                            1.變更就學區學生，請回原就讀國中學校辦理在校多元學習表現成績之審核及證明，由學校填寫多元學習表現成績審查申請表（詳見簡章附表 2，第
                            118頁）之多元學習表現成績，檢附相關證明文件，連同變更就學區申請文件，一併送至臺北市立松山高級中學（11070臺北市信義區基隆路1段156號，聯絡電話：02-2753-5968轉
                            229、222）辦理。（應屆畢業生（含非學校型態實驗教育學生）由國中學校函送方式辦理；非應屆畢業生及其他需變更就學區者由申請人以掛 號郵寄或親送方式辦理）<br>
                        </p>
                        <p>2.自大陸地區國中學校返國就學者，請另檢附大陸地區公證處公證屬實之歷年成績證明及公證書、財團法人海峽交流基金會證明、服務學習證明文件正本，並填寫多元學習表現成績審查申請表（詳見簡章附表
                            2，第 118 頁）後，連同變更就學區申請文件，一併掛號郵寄或親送臺北市立松山高級中學（11070臺北市信義區基隆路1段156號，聯絡電話：02-2753-5968轉
                            229、222）辦理，無須由大陸地區就讀國中審核。<br>
                        </p>
                        <p>3.自國外返國就學之應屆或非應屆學生，請另檢附經駐外館處驗證之就讀國外學校歷年成績單與中譯本、服務學習證明文件正本與中譯本，並填寫多元學習表現成績審查申請表（詳見簡章附表 2，第 118
                            頁）後，連同變更就學區申請文件，一併掛號郵寄或親送臺北市立松山高級中學（11070臺北市信義區基隆路1段156號，聯絡電話：02-2753-5968轉
                            229、222）辦理，無須由國外就讀學校審核。<br>
                        </p>
                        <p>4.多元學習表現成績審查結果通知： 107 年 6 月 7日（星期四）中午 12 時以後，通過審查之申請人可於本委員會網站（<a
                                href="https://ttk.entry.edu.tw/">https://ttk.entry.edu.tw/</a>）查詢。
                            查詢帳號為申請人身分證統一編號（首字英文字母大寫），預設密碼為身分證統一編號後 4 碼 加出生月日 4 碼（MMDD），共計 8
                            碼。第一次登入時系統即會強制更換密碼，此組帳號密碼同時作為志願選填系統登入使用，請謹慎保管帳號資訊。<br>

                        </p>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：基北區免試入學名額是否先分配三市之名額後，再行各自比序分發?</div>
                    <div class="faq_a">103 學年度十二年國教實施之後，免試入學不像以往先分配三市之名額，再各自進行比序分發，而是將基北區所有學生一起進行比序分發。</div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：107 學年度基北區免試入學與其他招生階段之銜接?</div>
                    <div class="faq_a">1.免試入學分發於特色招生甄選入學（術科測驗）、國中教育會考後報名辦理。免試
                        入學、五專免試入學以及特色招生甄選入學（音樂、美術、舞蹈、體育、科學班等）可同時報名，但錄取後僅得擇一校報到。</p>
                        <p>2.免試入學分發報到後仍未額滿之學校，經主管教育行政機關核准得各自辦理續招。</p>
                        <p>3.各項入學管道，如已獲學校錄取者，均需放棄錄取資格後，始得繼續參加下一入學管道，學生及家長請慎重考慮。</p>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：107 學年度「免試入學」與「特色招生考試分發入學」選填志願時間期程部份重疊，其辦理方式是否不同?</div>
                    <div class="faq_a">1.本區免試入學採一階段分發，另國立臺灣師範大學附屬高級中學及國立政治大學附屬高級中學、臺北市立麗山高級中學為本區 107
                        學年度辦理特色招生考試分發入學（以下簡稱特色招生）學校。免試入學與特色招生同時進行分發作業，報名本區特色招生考試分發入學之本區學生，可另加選填特色招生 1 個志願。
                        </p>
                        <p>2. 免試入學志願選填時間為 107 年 6 月 21日 12：00~6 月 28 日 12：00，特色招生志願選填時間為 107 年 6 月 26 日 12：00~6 月 28 日
                            12：00，免試入學與特色招生的招生名額是分別提列，然為使免試入學與特色招生考試分發入學一次分發到位，兩者志願選填系統、分發系統是相同，並同時填寫於同一志願報名表，以利學生及家長辦理報名作業。</p>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：107 學年度基北區免試入學報名分發作業流程為何?</div>
                    <div class="faq_a">1.考生依招生簡章規定，於網路辦理報名、登記選填志願。<br>
                        2.自行列印選填後的志願報名表，由考生及家長雙方簽名確認。<br>
                        3.繳交報名資料（集體報名或個別報名）。<br>
                        4.進行資料檢核作業。<br>
                        5.電腦分發作業。<br>
                        6.放榜。<br>
                        7.複查。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：如果不參加國中教育會考，可以報名參加免試入學嗎?</div>
                    <div class="faq_a">可以。 惟基北區免試入學之超額比序項目包括「志願序」、「多元學習表現（含均衡學習、服務學習） 」及「國中教育會考」三項(每項各占 36 分，總積分為 108
                        分)。因此進行超額比序時，未參加國中教育會考者，於「國中教育會考」之超額比序項次積分為0分。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：何謂超額比序?</div>
                    <div class="faq_a">1.免試入學的分發方式是由報名學生上網選填志願，每生依志願序以錄取 1
                        個校（科）為限。如各高級中等學校（科）報名人數未超過招生名額時，全額錄取；當報名人數超過招生名額時，才會依照「超額比序項目積分對照表」換算積分以進行比序分發。</p>
                        <p>2.基北區的超額比序項目包括「志願序」、「多元學習表現（含均衡學習、服務學習） 」及「國中教育會考」三項。三項各占 36 分，總積分為 108 分。</p>
                        <p>3.超額比序的各順次內容如下列簡表說明，或另請詳細參閱《基北區高級中等學校免試入學作業要點》或免試入學簡章。</p>
                        <table width="437" border="0">
                            <tr>
                                <th bgcolor="#FF9966" scope="col">順次</th>
                                <th bgcolor="#FF9966" scope="col">比序內容</th>
                                <th bgcolor="#FF9966" scope="col">順次</th>
                                <th bgcolor="#FF9966" scope="col">比序內容</th>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" bgcolor="#FFFFCC">1</td>
                                <td align="center" bgcolor="#FFFFCC">總積分（108 分）</td>
                                <td align="center" bgcolor="#FFFFCC">6</td>
                                <td align="center" bgcolor="#FFFFCC">數學科等級加標示</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">2</td>
                                <td align="center">多元學習表現積分（36 分）</td>
                                <td align="center">7</td>
                                <td align="center">英文科等級加標示</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" bgcolor="#FFFFCC">3</td>
                                <td align="center" bgcolor="#FFFFCC">國中教育會考積分（36 分）</td>
                                <td align="center" bgcolor="#FFFFCC">8</td>
                                <td align="center" bgcolor="#FFFFCC">社會科等級加標示</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">4</td>
                                <td align="center">志願序積分（36 分）</td>
                                <td align="center">9</td>
                                <td align="center">自然科等級加標示</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" bgcolor="#FFFFCC">5</td>
                                <td align="center" bgcolor="#FFFFCC">國文科等級加標示</td>
                                <td align="center" bgcolor="#FFFFCC">10</td>
                                <td align="center" bgcolor="#FFFFCC">寫作測驗</td>
                            </tr>
                            <tr>

                        </table>
                        <p>如經上述比序仍同分時，報各該主管教育行政機關核准增加名額，增額人數以不超過該校（科）之招生名額
                            5%為原則；如增額人數逾該校（科）之招生名額5%，增額部分依學生實際選填之志願順序於招生名額5%以內依序錄取。</p>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：學生選填志願序時，應注意什麼?志願序是如何換算成績分的?</div>
                    <div class="faq_a">1.參與基北區免試入學的每位學生至多可以選填 30 個志願。<br>
                        2.同時參與基北區免試入學及特色招生考試入學的每位學生至多可以選填 30+1 個志願。&nbsp;<br>
                        3.只參與本區特色招生考試入學的每位學生僅能選填 1 個志願。<br>
                        4.普通型高中、 技術型高中可以混合填選在不同志願序。<br>
                        5.選填技術型高中或普通型高中附設職業類科學校，可填選同校多個類科，其志願序連續填寫時視為同一志願，積分相同。例如：選填某職校為第一志願後，並連續選填該校之電機科、電子科、鑄造科、綜合高中科等
                        4 科組，則該職校此四科組都視同第一志願序，都可獲得志願序積分 36 分，系統分發按電機科、電子科、鑄造科、綜合高中科先後順序辦理。<br>
                        6.特別提醒，若其中穿插其他學校科組時，則視為不同志願，志願序積分不同。其次，各高級中等學校所附設之進修學校視為獨立學校，無法與其本校其他類科視作同一志願序而連續填寫之。<br>
                        7.學生報名及志願資料送出後，不得以任何理由要求更改報名及志願資料；同時無論錄取與否，報名資料概不退還。<br>
                        8.志願序積分換算方式：
                        <table width="555" border="0">
                            <tr>
                                <th align="center" scope="col">志願</th>
                                <td align="center" scope="col">1~5 志願</td>
                                <td align="center" scope="col">6~10 志願</td>
                                <td align="center" scope="col">11~15 志願</td>
                                <td align="center" scope="col">16~20 志願</td>
                                <td align="center" scope="col">21~30 志願</td>
                            </tr>
                            <tr>
                                <th align="center">積分</th>
                                <td align="center">36 分</td>
                                <td align="center">35 分</td>
                                <td align="center">34 分</td>
                                <td align="center">33 分</td>
                                <td align="center">32 分</td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：多元學習表現積分」採計哪些項目?</div>
                    <div class="faq_a">1.基北區免試入學之超額比序項目，多元學習表現積分部分僅採計「均衡學習」和「服務學習」兩項，並無採計獎懲。
                        <p>2.「均衡學習」僅採計「健康與體育」、「藝術與人文」及「綜合活動」3 領域，並無採計其他在校學習領域評量成績。</p>
                        <p>3.多元學習表現的各項積分達到標準就給分，並無人數比例的限制。此項積分換算採計係以學期為單位單獨計算，只要達到及格標準就給分，轉學生不論是否跨區，其權益不受影響。</p>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：多元學習表現中的「均衡學習」如何採計?</div>
                    <div class="faq_a">1.均衡學習部分，107 學年度基北區採計方式，對於應屆或非應屆學生的「健康與 體育」、「藝術與人文」及「綜合活動」等 3 個領域，為採計前 5
                        學期（七上~九上）的平均成績，每 1 領域平均成績及格得 7 分（未達及格標準者不計分），最高可得積分為 21 分。</p>
                        <p>2.歸國學生的採計方式由本委員會另專案處理，於個別報名時提出。特別提醒歸國學生要備妥經我國駐外館處驗證之各項證明文件及中譯本。</p>
                        <p>3.轉學生多元學習表現之均衡學習健體、藝文、綜合活動等三領域之採計（認）由轉入之國中學校內召開專案會議討論之，本從寬從優原則認定之。</p>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：多元學習表現中的「服務學習」如何採計?</div>
                    <div class="faq_a"> 1.服務學習部分，每學期服務滿 6 小時可得 5 分，未滿 6 小時則不計分，寒、暑假期之服務學習，依學年度上下學期起迄月份計算。（8/1 至隔年 1/31
                        為上學期，2/1 至 7/31 為下學期；五學期採計三學期，上限 15 分）</p>
                        <p>2.依據「基北區高級中等學校免試入學作業要點」，採計期間為 104 學年度上學期（七年級） 至 106 學年度上學期（九年級），採計原則依「107 學年度基北區免試入學服務學習時數認
                            證及轉換採計原則」辦理。</p>
                        <p>3.非應屆畢（結）業生服務學習時數採計，除上開採計期間外，亦得選擇國民中學在學期間前 5 學期選 3 學期進行採計。</p>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：「服務學習」如何認證?</div>
                    <div class="faq_a"> 1.應屆畢業生
                        依據「基北區十二年國民基本教育免試入學超額比序『服務學習』採計規定」（以下簡稱採計規定），以學校規劃之服務學習課程或活動，由就讀學校認證採計；非學校規劃之服務學習課程或活動，由服務機關(構)、法人、經政府立案之人民團
                        體發給服務學習時數證明，再由就讀學校認證採計。</p>
                        <p>2.非應屆畢（結）業學生 依據採計規定，由原畢（結）業學校進行服務時數採計，並得採計畢(結)業後服務時數，比照在校生方式辦理。</p>
                        <p>3.轉入基北區就讀之學生（含歸國及政府派赴國外工作人員子女）
                            轉入基北區就讀前已完成之服務學習時數，依據採計規定，由轉出學校進行服務時數認證，並由轉入學校進行採計；倘若未完成服務學習時數，則由轉入學校進行服務學習時數認證採計。</p>
                        <p>4.跨就學區參加基北區免試入學學生（含歸國及政府派赴國外工作人員子女） 由本委員會籌組服務學習時數認證採計小組，依據採計規定，進行服務學習時數認證採計。</p>
                        <p>前開認證及轉換採計原則，詳細內容請參閱本網站公告訊息或免試入學簡章。</p>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：會考成績如何採計?</div>
                    <div class="faq_a">基北區免試入學教育會考採計國文、數學、英語、社會、自然等 5 科之成績，各科按 等級加標示轉換積分 1-7 分；寫作測驗 1-6 級分轉換積分 0.1-1
                        分。最多合計可得「總積分 36 分」。</p>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：報名費相關規定。</div>
                    <div class="faq_a">1.報名學生每人繳交報名費：230 元 。<br>
                        2.中低收入戶子女之報名費：92元 ；低收入戶子女或其直系血親尊親屬支領失業給付者，報名費全部減免。符合前開減收或免收條件者，請按簡章內相關規定於報名時檢齊證明文件。<br>
                        3.報名受理後概不退還報名費。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：學生參加 107 年國中教育會考違反試場規則時，於免試入學超額比序項目之積分處理方式為何?</div>
                    <div class="faq_a">學生參與 107 年國中教育會考，若違反《107 年國中教育會考違反試場規則處理方式一覽表》之相關規定（詳細資料請參閱 107
                        年國中教育會考簡章），學生之超額比序國中教育會考積分採計方式依下列方式處理：<br>
                        1.違反《第一類：嚴重舞弊行為》： 取消學生於該次之考試資格，且國中教育會考總積分逕以零分計算，無扣減積分與否之情事。<br>
                        2.違反《第二類：一般舞弊或嚴重違規行為》： 學生於該科（國、英、數、社、自或寫作測驗）不予計列等級方式處理，且該科國中教育會考積分逕以零分計算，無扣減積分與否之情事。<br>
                        3.違反《第三類：一般違規行為》： 學生於該科（國、英、數、社、自）以違規記點方式處理：<br>
                        （1） 各科（國、英、數、社、自）違規記點扣分算法：記違規 1 點者，扣其國中教育會考總積分的百分之一（即 0.36 分）、記違規 2 點者，扣其國中教育會考總積分的百分之二（即 0.72
                        分）；違規計點扣減分數僅採扣其國中教育會考總積分，不另行扣其單科積分。<br>
                        （2） 寫作測驗之違規，均於會考成績計算時扣減級分，不另記點，無扣減積分與否之情事。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：107 學年度基北區免試入學特殊身分學生之入學因應為何?</div>
                    <div class="faq_a">
                        身心障礙學生、原住民學生、蒙藏學生、政府派赴國外工作人員子女、境外優秀科學技術人才子女、僑生及退伍軍人等法律授權訂定升學優待辦法之特殊身分學生，依其相關升學辦法辦理。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：107 學年度基北區免試入學非應屆國中畢業生之入學因應為何?</div>
                    <div class="faq_a">非應屆國中畢業生得向本委員會個別報名申請參加免試入學。學生得參加 107
                        年度國中教育會考，返回原就讀國中學校申請就學期間多元學習表現之紀錄，於簡章所訂時間至承辦學校（臺北市立松山高級中學）現場辦理報名。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：107 學年度基北區免試入學轉學生（轉入本區）之入學因應為何?</div>
                    <div class="faq_a">轉學生參加各該區免試入學時，其比序項目，由新轉入學校就其轉出國中原有之學習表現或紀錄，本從優、從寬原則加以認定。其餘作法同學校集體報名作業。</div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：107 學年度基北區免試入學簡章可網路下載使用?</div>
                    <div class="faq_a">可以，免費下載。107 年 1 月 15 日公告，請留意本委員會官方網站（<a href="https://ttk.entry.edu.tw/">https://ttk.entry.edu.tw/</a>）之最新消息。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：如何辦理複查或申訴?</div>
                    <div class="faq_a">1.複查僅針對分發錄取之結果，如對免試入學報名或分發作業流程、方式有其他疑義者，請提出申訴。</p>
                        <p>2.複查：由學生或家長直接向本委員會承辦學校（臺北市立木柵高級商工職業學校）提出申請，僅接受現場或委託辦理，不受理郵寄及電話申請。申請複查以 1 次為限，複查費用為 50
                            元，一經收件，不得以任何理由申請退件及退費。其他相關規定請參閱簡章（詳見簡章附表7，第 123頁）。</p>
                        <p>3.申訴：報名學生個人及家長若有疑義事項，於指定期限內，以限時掛號或快遞郵件向本委員會主委學校（臺北市立松山高級中學）提出申訴，若其他入學管道另
                            有申訴規定者，從其規定。其他相關規定請參閱簡章（詳見簡章附表9，第 125頁）。</p>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：如何辦理錄取資格之放棄?</div>
                    <div class="faq_a">已於本次免試入學錄取並報到之學生，如欲參加後續其他入學管道之招生，應於 107 年 7 月 16 日（星期一）下午 2
                        時前填具「放棄錄取資格聲明書」（詳見簡章附表 8， 第 124頁），由學生或家長親至錄取學校辦妥放棄錄取資格手續（恕不接受通訊或傳真方式辦理，但得委託辦理，委託書如簡章附表 10，第 126
                        頁），始得再報名參加 107 學年度其他入學管道招生。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：跨區參加特色招生測驗（含分發入學），能否在原來免試就學區作免試入學分發?</div>
                    <div class="faq_a">跨區參加特色招生測驗（含分發入學），不影響學生原來所屬之免試入學就學區之就學 權益，仍得於簡章所訂時間內志願選填及報名。</div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：在家教育之學生如何參加免試入學?</div>
                    <div class="faq_a">
                        非學校型態實驗教育學生參加免試入學，比照同等學力者辦理。關於超額比序項中「多元學習表現」之採計方式，則依教育部頒《十二年國民基本教育免試入學超額比序『多元學習表現』採計原則》之規定辦理，由設籍學校進行服務時數採計。其餘作法均同一般學生之報名作業。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：歸國學生如何參加免試入學?</div>
                    <div class="faq_a">1.歸國學生請上網申請變更就學區，按簡章規定之個別報名時間上網選填基北區高級中等學校志願，並現場（或委託）繳送相關資料。<br>
                        2.特別提醒歸國學生要備妥經我國駐外館處驗證之各項證明文件及中譯本，再由本委員會專案處理多元學習表現之超額比序積分採計（認）。
                    </div>
                </div>


                <div class="faq_title">優先免試入學</div>
                <div class="faq_box">
                    <div class="faq_q">問題：<strong>請問報名臺北市優先免試入學的資格為何</strong>?</div>
                    <div class="faq_a">臺北市國民中學應屆畢業生，另補充說明如下：<br>
                        <strong>(1)第一類及第二類基本資格：</strong><br>
                        <strong><u>臺北市</u></strong>國民中學(含高級中等學校附設國民中學部)應屆畢業生。以107學年度基北區高級中等學校免試入學學生多元學習表現資料上傳期限(107年3月23日)前就讀臺北市國中者為限。
                        <br>
                        <strong>(2)第二類之報名資格限制：</strong><br>
                        經直升入學管道或第一類優先免試入學錄取且報到之學生，需於該招生簡章所規定之放棄錄取資格期限內，完成放棄錄取資格，始得報名參加臺北市107學年度高級中等學校第二類優先免試入學。
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：107學年度與前學年度有何重大變革?</div>
                    <div class="faq_a">(1)各校招生名額比例提高：由5%至20%，提高至5%至30%<br>
                        <strong><u>(2)區分為第一類及第二類學校</u></strong><br>
                        <strong><u>(3)第二類學校公告分發序名額由1.5倍增至2倍</u></strong>(例如,招生名額為10名，即可有20名學生納入分發序)。 </p>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：何謂第一類學校?第二類學校?</div>
                    <div class="faq_a"><strong>第一類招生學校皆為私立學校，不進行比序，若超額則以公開抽籤方式決定；第二類招生學校包含部分私立學校和公立學校，</strong>比序方式與基北區免試入學相同，採108方案。
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：學生的戶籍地或居住地在臺北市，可是在其他縣市國中就讀，可以參加臺北市優先免試入學嗎?</div>
                    <div class="faq_a">不可以。僅限臺北市國民中學應屆畢業生，不限戶籍地或居住地。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：我是去年的臺北市國中畢業生，可以參加臺北市優先免試入學嗎?</div>
                    <div class="faq_a">不可以。僅限臺北市國民中學<strong><u>應屆</u></strong>畢業生。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：沒有購買簡章，可以報名臺北市優先免試入學嗎?</div>
                    <div class="faq_a">可以。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：臺北市優先免試入學可選填志願數量為何?</div>
                    <div class="faq_a">限擇定一校報名(單一志願)；僅選填學校，不需選填志願科別，將以現場撕榜結果決定錄取科別。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：臺北市優先免試入學報名與選填志願日期？報名方式為何?</div>
                    <div class="faq_a">
                        1.報名與選填志願日期：<br>
                        <strong>第一類：</strong>107年5月21日(星期一)至<strong><u>107</u></strong><strong><u>年</u></strong><strong><u>5</u></strong><strong><u>月</u></strong><strong><u>22</u></strong><strong><u>日</u></strong><strong><u>(</u></strong><strong><u>星期二</u></strong><strong><u>)</u></strong><strong><u>中午</u></strong><strong><u>12:00</u></strong><strong><u>止</u></strong><strong><u>(依國中規定時間內完成)</u></strong><br>
                        <strong>第二類：</strong>107年6月4日(星期一)至<strong><u>107</u></strong><strong><u>年</u></strong><strong><u>6</u></strong><strong><u>月</u></strong><strong><u>11</u></strong><strong><u>日</u></strong><strong><u>(</u></strong><strong><u>星期一</u></strong><strong><u>)</u></strong><strong><u>中午</u></strong><strong><u>12:00</u></strong><strong><u>止</u></strong><strong><u>(依國中規定時間內完成)</u></strong><br>
                        2.報名方式：需在上述期限內完成系統報名(<a href="http://107priorefa.tp.edu.tw" target="_blank">http://107priorefa.tp.edu.tw</a>)，並自行列印報名志願表由學生及家長簽名後再交至原就讀國中辦理報名；主委學校(松山工農)不受理個別報名。<br>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：有關簡章中提到報名資格，以多元學習表現上傳期限(107/3/23)上傳前就讀本市國中者為限，家長應如何辦理？是否要在該日期前報名?</div>
                    <div class="faq_a">1.家長不需辦理，該作業日程係由國中註冊組長上傳多元學習表現資料。<br>
                        2.該作業僅為提醒國九下轉入臺北市國中的學生，需在該上傳名單中，方能參加臺北市優先免試入學。
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：已在直升入學或第一類優先免試錄取者，是否仍可參加第二類優先免試入學?</div>
                    <div class="faq_a">已錄取但未報到者可直接報名；若已報到者需於該招生簡章規定時間內完成放棄者才可報名。</div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：如果不參加國中教育會考，可以報名參加臺北市優先免試入學嗎?</div>
                    <div class="faq_a">可以。第一類優先免試不採計會考成績；第二類優先免試之超額比序會考積分將以0分計算。</div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：臺北市優先免試入學報名費規定為何?</div>
                    <div class="faq_a">第一類及第二類報名費皆相同，如下：<br>(1) 一般生報名費：<strong>100 元</strong>。<br>
                        (2) 中低收入戶子女之報名費：40元；低收入戶子女或其直系血親尊親屬支領失業給付者，報名費0元。符合前開減收或免收條件者，請按簡章內相關規定於報名時檢齊證明文件。
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：請問臺北市優先免試入學與其基北區免試入學有何不同?</div>
                    <div class="faq_a">如下表。</p>
                        <table width="551" border="0">
                            <tr>
                                <th width="82" rowspan="2" bgcolor="#FF9966" scope="col">項目</th>
                                <th colspan="2" bgcolor="#FF9966" scope="col">臺北市優先免試入學</th>
                                <th width="174" rowspan="2" bgcolor="#FF9966" scope="col">基北區免試入學</th>
                            </tr>
                            <tr>
                                <td width="139" align="center" bgcolor="#FF9966">第一類</td>
                                <td width="138" align="center" bgcolor="#FF9966">第二類</td>
                            </tr>
                            <tr>
                                <th bgcolor="#FFFFCC">報名資格</th>
                                <td colspan="2" align="center" bgcolor="#FFFFCC">
                                    限就讀<strong><u>臺北市</u></strong>國民中學<strong><u>應屆</u></strong>畢業生
                                </td>
                                <td align="center" bgcolor="#FFFFCC">就讀<strong><u>基北區</u></strong>國民中學畢業生(其他資格詳見簡章)</td>
                            </tr>
                            <tr>
                                <th>報名日期</th>
                                <td align="center">5/21~5/22(依國中規定時間內完成)</td>
                                <td align="center">6/4~6/11(依國中規定時間內完成)</td>
                                <td align="center">以簡章及原就讀國中規定為準</td>
                            </tr>
                            <tr>
                                <th bgcolor="#FFFFCC">報名方式</th>
                                <td colspan="2" align="center" bgcolor="#FFFFCC">一律採集體報名</td>
                                <td align="center" bgcolor="#FFFFCC">集體報名、個別報名</td>
                            </tr>
                            <tr>
                                <th rowspan="2">超額比序及選填志願數量</th>
                                <td align="center">不需比序，超額時採公開抽籤</td>
                                <td colspan="2" align="center"><strong>採 108 方案 </strong><br>
                                    志願序 36 分+多元學習表現 36 分+國中教育會考 36 分=108分
                                </td>
                            </tr>
                            <tr>
                                <td> 限填 1 志願</td>
                                <td>限填 1 志願 ，志願序積分為 36 分</td>
                                <td>可填 30 志願，志願序積分以群組方式依序遞減</td>
                            </tr>
                            <tr>
                                <th bgcolor="#FFFFCC">是否需要撕榜</th>
                                <td align="center" bgcolor="#FFFFCC">需要</td>
                                <td align="center" bgcolor="#FFFFCC">需要</td>
                                <td align="center" bgcolor="#FFFFCC">不需要</td>
                            </tr>
                            <tr>
                                <th>放榜方式</th>
                                <td align="center"><strong>只公告報名結果，不代表錄取該校(科)，</strong>是否錄取需以撕榜結果決定</td>
                                <td align="center"><strong>只公告招生名額2倍之分發序名單，不代表錄取該校(科)，</strong>是否錄取需以撕榜結果決定</td>
                                <td align="center">公告榜單即視為錄取該校(科)</td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：臺北市優先免試入學超額比序方式為何?</div>
                    <div class="faq_a">第一類：不需比序，若超額則以公開抽籤辦理。<br>
                        第二類：比序方式與基北區免試入學相同，簡述如下：<br>
                        1.採 108 方案 ，積分計算如下：志願序 36 分+多元學習表現 36 分+國中教育會考 36 分=108 分<br>
                        2.超額比序順次</p>
                        <table width="437" border="0">
                            <tr>
                                <th bgcolor="#FF9966" scope="col">順次</th>
                                <th bgcolor="#FF9966" scope="col">比序內容</th>
                                <th bgcolor="#FF9966" scope="col">順次</th>
                                <th bgcolor="#FF9966" scope="col">比序內容</th>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" bgcolor="#FFFFCC">1</td>
                                <td align="center" bgcolor="#FFFFCC">總積分（108 分）</td>
                                <td align="center" bgcolor="#FFFFCC">6</td>
                                <td align="center" bgcolor="#FFFFCC">數學科等級加標示</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">2</td>
                                <td align="center">多元學習表現積分（36 分）</td>
                                <td align="center">7</td>
                                <td align="center">英文科等級加標示</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" bgcolor="#FFFFCC">3</td>
                                <td align="center" bgcolor="#FFFFCC">國中教育會考積分（36 分）</td>
                                <td align="center" bgcolor="#FFFFCC">8</td>
                                <td align="center" bgcolor="#FFFFCC">社會科等級加標示</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">4</td>
                                <td align="center">志願序積分（36 分）</td>
                                <td align="center">9</td>
                                <td align="center">自然科等級加標示</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" bgcolor="#FFFFCC">5</td>
                                <td align="center" bgcolor="#FFFFCC">國文科等級加標示</td>
                                <td align="center" bgcolor="#FFFFCC">10</td>
                                <td align="center" bgcolor="#FFFFCC">寫作測驗</td>
                            </tr>
                            <tr>
                        </table>
                        <p>&nbsp;</p>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：臺北市第二類優先免試入學超額比序積分詳細計算方式為何?</div>
                    <div class="faq_a">與基北區免試入學相同，詳如下表：</p>
                        <table width="641" height="269" border="0">
                            <tr>
                                <th width="39" bgcolor="#FF9966" scope="col">類別</th>
                                <th width="32" bgcolor="#FF9966" scope="col">項目</th>
                                <th width="20" bgcolor="#FF9966" scope="col">採計上限</th>
                                <th colspan="7" bgcolor="#FF9966" scope="col">積分換算</th>
                                <th width="237" bgcolor="#FF9966" scope="col">說明</th>
                            </tr>
                            <tr>
                                <th colspan="2"> 志願序</th>
                                <td align="center">36 分</td>
                                <td colspan="2" align="center">36 分</td>
                                <td colspan="6" align="center">採單一志願選填，故志願序積分為 <strong>36 </strong>分。</td>
                            </tr>
                            <tr>
                                <th rowspan="4">多元學習表現</th>
                                <th rowspan="2">均衡學習</th>
                                <td rowspan="2" align="center">21 分</td>
                                <td colspan="2" align="center">7 分</td>
                                <td colspan="5" align="center">符合 1 個領域</td>
                                <td rowspan="2" align="left">健體、藝文、綜合三領域前五學期平均成績及格者。</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">0 分</td>
                                <td colspan="5" align="center">未符合</td>
                            </tr>
                            <tr>
                                <th rowspan="2">服務學習</th>
                                <td rowspan="2" align="center">15 分</td>
                                <td colspan="2" align="center">5 分</td>
                                <td colspan="5" align="center">每學期服務滿 6 小時以上</td>
                                <td rowspan="2" align="left">1.由國民中學學校認證。<br>
                                    2.採計期間為104學年度（七年級）上學期至106學年度（九年級）上學期，採計原則依「基北區免試入學服務學習時數認證及轉換採計原則」辦理。
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">0 分</td>
                                <td colspan="5" align="center">每學期服務未滿 6 小時</td>
                            </tr>
                            <tr>
                                <th rowspan="4">國中教育會考</th>
                                <th rowspan="2">五科</th>
                                <td rowspan="2" align="center">35 分</td>
                                <td width="35" align="center">7 分</td>
                                <td width="37" align="center">6 分</td>
                                <td width="36" align="center">5 分</td>
                                <td width="40" align="center">4 分</td>
                                <td width="42" align="center">3 分</td>
                                <td width="38" align="center">2 分</td>
                                <td width="39" align="center">1 分</td>
                                <td rowspan="2" align="left">國文、數學、英語、社會、自然 五科，各科按等級加標示轉換積分 1-7 分。</td>
                            </tr>
                            <tr>
                                <td align="center">A++</td>
                                <td align="center">A+</td>
                                <td align="center">A</td>
                                <td align="center">B++</td>
                                <td align="center">B+</td>
                                <td align="center">B</td>
                                <td align="center">C</td>
                            </tr>
                            <tr>
                                <th rowspan="2">寫作測驗</th>
                                <td rowspan="2" align="center">1 分<br></td>
                                <td align="center">1 分</td>
                                <td align="center">0.8 分</td>
                                <td align="center">0.6 分</td>
                                <td align="center">0.4 分</td>
                                <td align="center">0.2 分</td>
                                <td align="center">0.1 分</td>
                                <td rowspan="2" align="center">&nbsp;</td>
                                <td rowspan="2" align="left">寫作測驗 1-6 級分轉換積分 0.1-1 分。</td>
                            </tr>
                            <tr>
                                <td align="center">6 級</td>
                                <td align="center">5 級</td>
                                <td align="center">4 級</td>
                                <td align="center">3 級</td>
                                <td align="center">2 級</td>
                                <td align="center">1 級<br></td>
                            </tr>
                            <tr>
                                <th colspan="2" bgcolor="#FFFFCC">總積分</th>
                                <td colspan="9" align="center" bgcolor="#FFFFCC">108 分</td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：臺北市優先免試入學報名分發作業流程為何?</div>
                    <div class="faq_a">第一類：<br>
                        (1) 考生依招生簡章規定，於網路辦理報名、登記選填志願。<br>
                        (2) 自行列印選填後的志願報名表，由考生及家長簽名確認。<br>
                        (3) 繳交報名資料(<strong><u>一律由原就讀國中集體報名</u></strong>)。<br>
                        (4) 招生學校<strong><u>公告報名結果</u></strong>。<br>
                        <strong><u>(5) 至招生學校登記、公開抽籤、撕榜、報到(決定錄取學校/科別)。</u></strong><br>
                        第二類：<br>
                        (1) 考生依招生簡章規定，於網路辦理報名、登記選填志願。<br>
                        (2) 自行列印選填後的志願報名表，由考生及家長簽名確認。<br>
                        (3) 繳交報名資料(<strong><u>一律由原就讀國中集體報名</u></strong>)。<br>
                        (4) 招生學校<strong><u>公告分發序。</u></strong><br>
                        <strong><u>(5) 至招生學校登記、撕榜、報到(決定錄取學校/科別)。</u></strong></div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：何謂公開抽籤?</div>
                    <div class="faq_a"><strong><u>僅適用於第一類學校</u></strong>，若有超額情形(報名人數大於最低招生人數科別之招生人數)，不進行比序，直接以公開抽籤方式進行。抽籤後取得之分發序，做為後續撕榜之順序。(詳見簡章)
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：何謂分發序?</div>
                    <div class="faq_a">學生依第一類招生學校現場公開抽籤取得之分發序，或第二類招生學校公告之分發序，依序進行現場撕榜作業。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：第一類優免公告報名結果及第二類優免公告分發序即視同錄取該學校嗎?</div>
                    <div class="faq_a">否。需以招生學校現場撕榜結果決定是否錄取該校(科)。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：第一類優免公開抽籤及撕榜相關作業如何進行?</div>
                    <div class="faq_a">
                        登記、公開抽籤、撕榜、報到流程如下：
                        <table width="707" border="0">
                            <tr>
                                <th width="108" bgcolor="#FF9966" scope="col">項目</th>
                                <th width="95" bgcolor="#FF9966" scope="col">時間</th>
                                <th width="343" bgcolor="#FF9966" scope="col">說明</th>
                                <th width="143" bgcolor="#FF9966" scope="col">備註</th>
                            </tr>
                            <tr>
                                <th bgcolor="#FFFFCC">登記</th>
                                <td align="center" bgcolor="#FFFFCC">09:00~10:00</td>
                                <td bgcolor="#FFFFCC">各校公告分發序之學生應<strong><u>親自或由家長（或監護人）或委託親友辦理登記</u></strong>，逾時視同放棄撕榜資格。（詳見簡章）
                                </td>
                                <td bgcolor="#FFFFCC">未按時完成登記者，不得參加現場撕榜作業。</td>
                            </tr>
                            <tr>
                                <th>公開抽籤<br>
                                    （取得分發序）
                                </th>
                                <td rowspan="2" align="center">10:00~12:00</td>
                                <td>1.需按時完成登記者方能參加公開抽籤作業。<br>
                                    2.公開抽籤分以下二階段進行：<br>
                                    (1) 第一階段：於現場公開以電腦程式隨機產生抽籤順序。<br>
                                    (2) 第二階段：依抽籤順序進行現場人工公開抽籤，取得分發序。
                                </td>
                                <td rowspan="2">結束時間得依各校實際作業時間而定。</td>
                            </tr>
                            <tr>
                                <th>現場撕榜<br>
                                    (撕榜後視同報到)
                                </th>
                                <td>1.依公開抽籤取得之分發序，依序進行現場撕榜作業（先進行一般生撕榜作業再進行特殊身分學生撕榜作業）。<br>
                                    2.完成撕榜作業並取得招生學校發放之錄取證明，即視為錄取且報到該校(科)。<br></td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：第二類優免撕榜相關作業如何進行?</div>
                    <div class="faq_a">
                        登記、撕榜、報到流程如下：
                        <table width="707" border="0">
                            <tr>
                                <th width="108" bgcolor="#FF9966" scope="col">項目</th>
                                <th width="95" bgcolor="#FF9966" scope="col">時間</th>
                                <th width="343" bgcolor="#FF9966" scope="col">說明</th>
                                <th width="143" bgcolor="#FF9966" scope="col">備註</th>
                            </tr>
                            <tr>
                                <th bgcolor="#FFFFCC">登記</th>
                                <td align="center" bgcolor="#FFFFCC">09:00~10:00</td>
                                <td bgcolor="#FFFFCC">各校公告分發序之學生應<strong><u>親自或由家長（或監護人）或委託親友辦理登記，</u></strong>逾時視同放棄撕榜資格。（詳見簡章）
                                </td>
                                <td bgcolor="#FFFFCC">未按時完成登記者，不得參加現場撕榜作業。</td>
                            </tr>
                            <tr>
                                <th>現場撕榜<br>
                                    (撕榜後視同報到)
                                </th>
                                <td align="center">10:00~12:00</td>
                                <td>1.需按時完成登記者方能參加現場撕榜作業。<br>
                                    2.依各招生學校公告之分發序，依序進行現場撕榜作業（先進行一般生撕榜作業再進行特殊身分學生撕榜作業）。<br>
                                    3.完成撕榜作業並取得招生學校發放之錄取證明，即視為錄取且報到該校(科)。
                                </td>
                                <td>結束時間得依各校實際作業時間而定。</td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>
                    </div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：第一類完成撕榜報到者，可以再參加第二類優先免試入學或基北區免試入學嗎？</div>
                    <div class="faq_a">依簡章規定向錄取學校完成放棄錄取手續者，可參加第二類優先免試入學或基北區免試入學。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：第二類完成撕榜報到者，可以再參加基北區免試入學嗎?</div>
                    <div class="faq_a">依簡章規定向錄取學校完成放棄錄取手續者，可參加基北區免試入學。</div>
                </div>

                <div class="faq_box">
                    <div class="faq_q">問題：請問臺北市優先免試入學簡章可以在那裡購買或下載?</div>
                    <div class="faq_a">(1) 簡章電子檔下載：<a href="http://107priorefa.tp.edu.tw" target="_blank">http://107priorefa.tp.edu.tw</a><br>
                        (2) 紙本簡章購買(每份 50 元、即日起發售至107年6月13日)上班時間<br>
                        　　　購買地點：臺北市立松山高級工業職業學校警衛室(110臺北市信義區忠孝東路五段236巷15號，TEL：：02-2722-6616 分機 851)
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：臺北市優先免試入學主委學校為何?</div>
                    <div class="faq_a">(1) 主委學校為臺北市立松山工農。<br>
                        (2) 聯絡電話(02)2722-6616 轉 201，傳真(02)2722-0672<br>
                        　　　地址：110臺北市信義區忠孝東路五段236巷15號
                    </div>
                </div>


                <div class="faq_box">
                    <div class="faq_q">問題：臺北市優先免試入學網站提供哪些訊息或服務?</div>
                    <div class="faq_a">網址:<a href="http://107priorefa.tp.edu.tw" target="_blank">http://107priorefa.tp.edu.tw</a>，提供以下訊息及服務：<br>
                        1.優先免試入學簡章電子檔下載<br>
                        2.學生選填志願系統之入口網站<br>
                        3.最新消息公告及常見問答
                    </div>
                </div>

                <div class="faq_title">特色招生考試入學分發</div>
                <div class="faq_box">
                    <div class="faq_q">問題：考試分發入學與免試入學之學生會分開編班嗎?</div>
                    <div class="faq_a">不一定。多數學校是分開編班，亦有混合編班者。詳情依各校簡章之說明。</div>
                </div>
                <div class="faq_box">
                    <div class="faq_q">問題：考試分發入學之後若有適應不良，可以轉入普通班嗎?</div>
                    <div class="faq_a">不一定。各校之實施計畫或簡章具有轉出轉入辦法者，依其計畫或簡章辦理轉出即可。</div>
                </div>
                <div class="faq_box">
                    <div class="faq_q">問題：跨區參加特色招生考試入學分發的學生，會不會影響在原區的免試入學分發資格?</div>
                    <div class="faq_a">不會。如某生原參加高雄區免試入學，跨區參加臺南區特招考試分發入學，不影響其在高雄區之分發資格。</div>
                </div>
                <div class="faq_box">
                    <div class="faq_q">問題：如果特色招生考試分發入學招生學校訂有會考成績門檻，如未達到，是否可參加考試?</div>
                    <div class="faq_a">如招生簡章無明確規定，原則上可報名考試，但無法參加該校之分發；選填志願時，無法將該校列入志願。</div>
                </div>

                <div class="catalog_file_list">
                    <div class="cf_Lv01_title">相關檔案</div>
                    <div class="cf_Lv01_list">
                        <a class="download_icon" target="_blank"
                           href="${pageContext.request.contextPath}/resources/download/107-1懶人包-就讀基北區國中之非應屆學生v3_1070213.pdf">●
                            107原就讀基北區國中之非應屆學生免試入學個別報名流程圖</a>
                        <a class="download_icon" target="_blank"
                           href="${pageContext.request.contextPath}/resources/download/107-2懶人包-非就讀基北區國中之應屆學生v3_1070213.pdf">●
                            107非就讀基北區國中之應屆學生免試入學個別報名流程圖</a>
                        <a class="download_icon" target="_blank"
                           href="${pageContext.request.contextPath}/resources/download/107-3懶人包-非就讀基北區國中之非應屆學生v3_1070213.pdf">●
                            107非就讀基北區國中之非應屆學生免試入學個別報名流程圖</a>
                        <a class="download_icon" target="_blank"
                           href="${pageContext.request.contextPath}/resources/download/107-4懶人包-大陸及國外返臺就學者v3_1070213.pdf">●
                            107大陸地區或國外返臺就學學生免試入學個別報名流程圖</a>

                    </div>
                </div>

            </div>
            <!-- 內容 區塊 End -->

        </div>
    </div>
    <!-- 主內容 區塊 End -->

    <!-- 選單列表 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/menuList.jsp">
        <jsp:param name="home" value="0"/>
    </jsp:include>
    <!-- 選單列表 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>