<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="aName" value="相關法規"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <div class="main_law">
                <div class="title">
                    <img src="${pageContext.request.contextPath}/templates/images/titles_10.png"
                         width="100" height="20" alt="相關法規">
                </div>

                <div class="link_box">
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/1.高級中等教育法.pdf" title="1.高級中等教育法.pdf">●
                        1.高級中等教育法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/2.高級中等學校多元入學招生辦法.pdf"
                       title="2.高級中等學校多元入學招生辦法.pdf">● 2.高級中等學校多元入學招生辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/3.基北區高級中等學校免試入學作業要點.pdf"
                       title="3.基北區高級中等學校免試入學作業要點.pdf">● 3.基北區高級中等學校免試入學作業要點</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/4.基北區高級中等學校特色招生核定作業要點.pdf"
                       title="4.基北區特招核定作業要點1014.pdf">● 4.基北區高級中等學校特色招生核定作業要點</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/5.五專多元入學方案.pdf"
                       title="5.五專多元入學方案 ( 民國 104 年 04 月 21 日 修正 ).pdf">● 5.五專多元入學方案</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/6.身心障礙學生升學輔導辦法.pdf"
                       title="6.身心障礙學生升學輔導辦法.pdf">● 6.身心障礙學生升學輔導辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/7.原住民學生升學保障及原住民公費留學辦法.pdf"
                       title="7.原住民學生升學保障及原住民公費留學辦法.pdf">● 7.原住民學生升學保障及原住民公費留學辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/8.蒙藏學生升學優待辦法.pdf"
                       title="8.蒙藏學生升學優待辦法.pdf">● 8.蒙藏學生升學優待辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/9.境外優秀科學技術人才子女來臺就學辦法.pdf"
                       title="9.境外優秀科學技術人才子女來臺就學辦法.pdf">● 9.境外優秀科學技術人才子女來臺就學辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/10.退伍軍人報考高級中等以上學校優待辦法.pdf"
                       title="10-1.退伍軍人報考高級中等以上學校優待辦法修正總說明.pdf">● 10.退伍軍人報考高級中等以上學校優待辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/11.僑生回國就學及輔導辦法.pdf"
                       title="11.僑生回國就學及輔導辦法.pdf">● 11.僑生回國就學及輔導辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/12.外國學生來臺就學辦法.pdf"
                       title="12.外國學生來臺就學辦法.pdf">● 12.外國學生來臺就學辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/13.國際競賽成績優良升學優待.pdf"
                       title="13.國際競賽成績優良升學優待.pdf">● 13.國際競賽成績優良升學優待</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/14.高級中等學校辦理國民中學技藝技能優良學生甄審入學實施要點.pdf"
                       title="14.技藝技能優良學生甄審入學實施要點.pdf">● 14.技藝技能優良學生甄審入學實施要點</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/15.香港澳門居民來臺就學辦法.pdf"
                       title="15.香港澳門居民來臺就學辦法.pdf">● 15.香港澳門居民來臺就學辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/16.政府派赴國外工作人員子女返國入學辦法.pdf"
                       title="16.政府派赴國外工作人員子女返國入學辦法.pdf">● 16.政府派赴國外工作人員子女返國入學辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/17.中等以上學校技藝技能優良學生甄審及保送入學辦法.pdf"
                       title="17.中等以上學校技藝技能優良學生甄審及保送入學辦法.pdf">● 17.中等以上學校技藝技能優良學生甄審及保送入學辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/18.中等以上學校運動成績優良學生升學輔導辦法.pdf"
                       title="18.中等以上學校運動成績優良學生升學輔導辦法.pdf">● 18.中等以上學校運動成績優良學生升學輔導辦法</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/19-1.普通型高級中等學校科學班辦理要點.pdf"
                       title="19-1.普通型高級中等學校科學班辦理要點.pdf">● 19-1.普通型高級中等學校科學班辦理要點</a>
                    <a class="download_icon" target="_blank"
                       href="${pageContext.request.contextPath}/resources/download/19-2.普通型高級中等學校科學班辦理要點附件.pdf"
                       title="19-2.普通型高級中等學校科學班辦理要點附件.pdf">● 19-2.普通型高級中等學校科學班辦理要點附件</a>
                </div>
            </div>
            <!-- 內容 區塊 End -->

        </div>
    </div>
    <!-- 主內容 區塊 End -->

    <!-- 選單列表 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/menuList.jsp">
        <jsp:param name="home" value="0"/>
    </jsp:include>
    <!-- 選單列表 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>