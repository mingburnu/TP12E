<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="aName" value="相關連結"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <div class="main_link">
                <div class="title"><img src="${pageContext.request.contextPath}/templates/images/titles_05.png"
                                        width="100" height="20" alt="相關連結"></div>
                <div class="link_title">了解12年國教</div>
                <div class="link_box">
                    <a class="link_icon" target="_blank" href="http://12basic.tp.edu.tw" title="臺北市十二年國教資訊網 (另開新視窗)">●
                        臺北市十二年國教資訊網</a>
                    <a class="link_icon" target="_blank" href="http://12basic.edu.tw" title="教育部十二年國教資訊網 (另開新視窗)">●
                        教育部十二年國教資訊網</a>
                </div>

                <div class="link_title">認識高中職、五專</div>
                <div class="link_box">
                    <a class="link_icon" target="_blank" href="http://expo.tp.edu.tw/" title="臺北市107年度高中職網路博覽會 (另開新視窗)">●
                        臺北市107年度高中職網路博覽會</a>
                    <a class="link_icon" target="_blank" href="http://adapt.k12ea.gov.tw/"
                       title="107年國中畢業生適性入學宣導網站 (另開新視窗)">● 107年國中畢業生適性入學宣導網站</a>
                </div>

                <div class="link_title">認識自我與生涯規劃</div>
                <div class="link_box">
                    <a class="link_icon" target="_blank" href="http://cweb.saihs.edu.tw/web/skillcenter/"
                       title="臺北市技藝教育資訊網 (另開新視窗)">● 臺北市技藝教育資訊網</a>
                    <a class="link_icon" target="_blank" href="https://school.tp.edu.tw/Login.action?l=tp"
                       title="臺北市國中學生生涯領航儀表板（二代校務行政系統）(另開新視窗)">● 臺北市國中學生生涯領航儀表板（二代校務行政系統）</a>
                </div>

                <div class="link_title">認識教育會考</div>
                <div class="link_box">
                    <a class="link_icon" target="_blank" href="http://cap.ntnu.edu.tw" title="國中教育會考網站 (另開新視窗)">●
                        國中教育會考網站</a>
                </div>

                <div class="link_title">免試入學暨志願選填</div>
                <div class="link_box">
                    <a class="link_icon" target="_blank"
                       href="https://107priorefa.tp.edu.tw/NoExamImitate_TP/NoExamImitateAB/Apps/Page/Public/News.aspx"
                       title="107年基北區免試入學網站 (另開新視窗)">● 臺北市107學年度優先免試入學網站</a>
                    <a class="link_icon" target="_blank" href="http://ttk.entry.edu.tw" title="107年基北區免試入學網站 (另開新視窗)">●
                        107年基北區免試入學網站</a>
                </div>
            </div>
            <!-- 內容 區塊 End -->

        </div>
    </div>
    <!-- 主內容 區塊 End -->

    <!-- 選單列表 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/menuList.jsp">
        <jsp:param name="home" value="0"/>
    </jsp:include>
    <!-- 選單列表 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>