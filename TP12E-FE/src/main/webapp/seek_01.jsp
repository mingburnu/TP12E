<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="seek_01.jsp"/>
        <jsp:param name="aName" value="宣導諮詢"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="各入學管道主委學校"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="3" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="8" name="title"/>
                            <jsp:param value="1" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">各入學管道主委學校</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0"
                                           class="cc_table_01">
                                        <caption>國中教育會考</caption>
                                        <th width="150">會考</th>
                                        <th width="200">主委學校</th>
                                        <th>網站／聯絡電話</th>
                                        </tr>
                                        <tr>
                                            <td>臺北考區教育會考</td>
                                            <td>臺北市立大同高中</td>
                                            <td>網址：http://www.ttsh.tp.edu.tw/<br/> 電話：(02)
                                                2505-4269
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <BR/>

                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0"
                                           class="cc_table_01">
                                        <caption>免試入學</caption>
                                        <tr>
                                            <th width="150">入學管道</th>
                                            <th width="200">主委學校</th>
                                            <th>網站／聯絡電話</th>
                                        </tr>
                                        <tr>
                                            <td>免試入學</td>
                                            <td>臺北市立松山高中</td>
                                            <td>網址：http://www.sssh.tp.edu.tw/<br/> 電話：(02)
                                                2753-5968
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>臺北市優先免試入學</td>
                                            <td>臺北市立松山工農</td>
                                            <td>網址：http://www.saihs.edu.tw/<br/> 電話：(02)
                                                (02)2722-6616
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>完全中學直升入學</td>
                                            <td>各校自行辦理</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>技優甄審入學</td>
                                            <td>新北市立三重商工</td>
                                            <td>網址：http://www.scvs.ntpc.edu.tw/<br/>
                                                電話：(02)2971-5606
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>產業特殊需求類科</td>
                                            <td>新北市立瑞芳高工</td>
                                            <td>網址：http://www.jfvs.ntpc.edu.tw/<br/>
                                                電話：(02)2497-2516
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>實用技能學程</td>
                                            <td>新北市立淡水商工</td>
                                            <td>網址：http://www.tsvs.ntpc.edu.tw/<br/> 電話：(02)2620-3930
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>高職學校獨立招生</td>
                                            <td>各校自行辦理</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>

                                <BR/>

                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0"
                                           class="cc_table_01">
                                        <caption>特色招生</caption>
                                        <tr>
                                            <th width="150">入學管道</th>
                                            <th width="200">主委學校</th>
                                            <th>網站／聯絡電話</th>
                                        </tr>
                                        <tr>
                                            <td>特色招生考試分發</td>
                                            <td>國立政大附中</td>
                                            <td>網址：http:// www.ahs.nccu.edu.tw<br/> 電話：(02)
                                                8237-7500
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>北市專業群科（高職） <br/> 甄選入學
                                            </td>
                                            <td>臺北市立士林高商</td>
                                            <td>網址：http://earth.slhs.tp.edu.tw/107special/<br/> 電話：(02)
                                                2831-3114
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>甄選入學北區音樂班</td>
                                            <td>國立師大附中</td>
                                            <td>網址：http://www.hs.ntnu.edu.tw/<br/> 電話：(02)2707-5215
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>甄選入學北區舞蹈班</td>
                                            <td>國立竹北高中</td>
                                            <td>網址：http://top.cpshs.hcc.edu.tw/<br/> 電話：(03)
                                                551-7330
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>甄選入學北區美術班</td>
                                            <td>臺北市立中正高中</td>
                                            <td>網址：http://www.ccsh.tp.edu.tw/<br/> 電話：(02)
                                                2823-4811
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>甄選入學北區戲劇班</td>
                                            <td>臺北市立復興高中</td>
                                            <td>網址：http://www.fhsh.tp.edu.tw/<br/> 電話：(02)
                                                2891-4131
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>體育班</td>
                                            <td>各校自行辦理</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="3">科學班</td>
                                            <td>臺北市立建國高中</td>
                                            <td>網址：http://www.ck.tp.edu.tw/~scicla<br/> 電話：(02)
                                                2303-4381
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>國立師大附中</td>
                                            <td>網址：http://www.hs.ntnu.edu.tw/<br/> 電話：(02)
                                                2707-5215
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>臺北市立第一女子高級中學</td>
                                            <td>網址：http://www.fg.tp.edu.tw/<br/> 電話：(02)
                                                2382-0484
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <BR/>


                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>