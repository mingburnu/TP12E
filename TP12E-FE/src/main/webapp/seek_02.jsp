<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->

    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="seek_01.jsp"/>
        <jsp:param name="aName" value="宣導諮詢"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="服務學習"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="3" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="8" name="title"/>
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">服務學習</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">107學年度基北區免試入學服務學習時數認證及轉換採計原則</div>
                                <ul class="cc_Lv01_list">
                                    <li>依據
                                        <ol class="cc_Lv02_list">
                                            <li>臺北市政府教育局106年9月27日北市教中字第10639623600號函修正之《基北區高級中等學校免試入學作業要點》。</li>
                                            <li>
                                                臺北市政府教育局101年8月17日北市教中字第10140376300號函公布修訂之「基北區十二年國民基本教育免試入學超額比序『服務學習』採計規定」。
                                            </li>
                                        </ol>
                                    </li>
                                    <li>認證單位
                                        <ol class="cc_Lv02_list">
                                            <li>應屆畢業生<BR/>
                                                依據「基北區十二年國民基本教育免試入學超額比序『服務學習』採計規定」（以下簡稱採計規定）以學校規劃之服務學習課程或活動，由就讀學校認證採計；非學校規劃之服務學習課程或活動，由服務機關(構)、法人、經政府立案之人民團體發給服務學習時數證明，再由就讀學校認證採計。
                                            </li>
                                            <li>非應屆畢（結）業學生<BR/>
                                                依據採計規定，由原畢（結）業學校進行服務時數採計，並得採計畢(結)業後服務時數，比照在校生方式辦理。
                                            </li>
                                            <li>轉入基北區就讀之學生（含歸國及政府派赴國外工作人員子女）<BR>
                                                轉入基北區就讀前已完成之服務學習時數，依據採計規定，由轉出學校進行服務時數認證，並由轉入學校進行採計；倘若未完成服務學習時數，則由轉入學校進行服務學習時數認證採計。
                                            </li>
                                            <li>跨就學區參加基北區免試入學學生（含歸國及政府派赴國外工作人員子女）<BR>
                                                由基北區免試入學委員會籌組服務學習時數認證採計小組，依據採計規定，進行服務學習時數認證採計。
                                            </li>
                                        </ol>
                                    </li>
                                    <li>實施方式
                                        <ol class="cc_Lv02_list">
                                            <li>
                                                服務學習時數採計期間自104學年度上學期(七年級)至106學年度上學期(九年級)（依學年度上下學期起迄月份計算，上學期為當年度8月1日至隔年度1月31日；下學期為當年度2
                                                月1日至7月31日）止，連續5學期選3學期，每學期完成6小時，可得5分，上限15分。
                                            </li>
                                            <li>非應屆畢(結)業生服務學習時數採計，除上開採計期間外，亦得選擇國中在學期間前5學期選3學期進行採計。</li>
                                        </ol>
                                    </li>
                                    <li>轉入基北區就讀學生(含歸國學生及政府派赴國外工作人員子女)，其採計之服務學習時數，需於107年1月31日前完成認證。</li>
                                    <li>
                                        跨就學區學生(含歸國學生及政府派赴國外工作人員子女)，如因申請變更就學區需繳交「戶口名簿影本」為證明文件者，應於107年4月30日前完成設籍基北區，其採計之服務學習時數，需於107年1月31日前完成認證。
                                    </li>
                                </ul>
                            </div>
                            <div class="catalog_file_list">
                                <div class="cf_Lv01_title">相關檔案</div>
                                <div class="cf_Lv01_list">
                                    <a class="download_icon" target="_blank"
                                       href="${pageContext.request.contextPath}/resources/download/107學年度基北區免試入學服務學習時數認證及轉換採計原則.pdf">●
                                        107學年度基北區免試入學服務學習時數認證及轉換採計原則</a>

                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>