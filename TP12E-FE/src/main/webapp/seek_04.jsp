<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />

<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->


		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="seek_01.jsp" />
			<jsp:param name="aName" value="宣導諮詢" />
			<jsp:param name="subPosition" value="<%=request.getRequestURI()%>" />
			<jsp:param name="subAName" value="免學費政策" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><jsp:include
								page="/WEB-INF/jsp/layout/listBox.jsp">
								<jsp:param value="3" name="hover" />
							</jsp:include></td>
						<td class="layout_04_02"><jsp:include
								page="/WEB-INF/jsp/layout/subListBox.jsp">
								<jsp:param value="8" name="title" />
								<jsp:param value="4" name="hover" />
							</jsp:include>

							<div class="catalog_content_box">
								<div class="title">免學費政策</div>
								<div class="catalog_content">
									<div class="cc_Lv01_table">
										<table width="100%" border="0" cellspacing="1" cellpadding="0"
											class="cc_table_01">
											<caption>免學費政策</caption>
											<tr>
												<th>高職（五專前三年）</th>
												<td>全面免學費</td>
											</tr>
											<tr>
												<th>高中</th>
												<td>家戶所得148萬元以下（臺北市購買國宅標準）免學費；家戶所得148萬元以上，就讀私校依戶 <br />
													籍所在縣市補助5000-6000元（臺北市）。
												</td>
											</tr>
											<tr>
												<th>綜合高中</th>
												<td>高一比照高職免學費，高二、三依所選學程比照高職或高中方式辦理。</td>
											</tr>
										</table>
									</div>

								</div>
							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->
			</div>


		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />

</body>
</html>