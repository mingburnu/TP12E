<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="seek_01.jsp"/>
        <jsp:param name="aName" value="宣導諮詢"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="臺北市高中職及五專學校"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="3" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="8" name="title"/>
                            <jsp:param value="5" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">臺北市高中職及五專學校</div>

                            <div class="catalog_content_list_box">
                                <a href="javascript: void(0)" onclick="showAll($(this))">全部</a>
                                <a href="javascript: void(0)" onclick="showAdvanced($(this))">該校設有進修學校</a>
                                <a href="javascript: void(0)" onclick="showCombined($(this))">該校設有普通科或綜合高中</a>
                                <a href="javascript: void(0)" onclick="showVocational($(this))">該校附設職業類科</a>
                            </div>

                            <div class="catalog_content">
                                <div class="cc_Lv01_txt_b">
                                    *新北市及基隆市公私立高中職學校詳見招生簡章<BR/>
                                    *107年3月18日(星期日) 於花博爭豔館舉辦臺北市高中職博覽會，歡迎來認識臺北市各高級中等學校喔！
                                </div>


                                <div class="cc_Lv01_title" id="all" style="display: none;">高中學校</div>
                                <div class="cc_Lv01_title_b" id="all" style="display: none;">公立</div>
                                <div class="cc_Lv01_txt" id="all" style="display: none;">
                                    大同高中、大直高中、大理高中、中山女高、中正高中、中崙高中、內湖高中、北一女高、永春高中、成功高中、成淵高中、百齡高中、西松高中、育成高中、和平高中、明倫高中、松山高中、南港高中、南湖高中、建國高中、復興高中、景美女高、華江高中、陽明高中、萬芳高中、麗山高中；國立政大附中、國立師大附中等28所
                                </div>
                                <div class="cc_Lv01_title_b" id="all" style="display: none;">私立</div>
                                <div class="cc_Lv01_txt" id="all" style="display: none;">
                                    十信高中、大同高中、大誠高中、文德女中、方濟中學、立人高中、再興中學、延平中學、東山高中、金甌女中、協和祐德高中、南華進修學校、泰北高中、強恕高中、復興實驗高中、景文高中、華興中學、達人女中、滬江高中、靜修女中、衛理女中、薇閣高中、奎山實驗高中、志仁進修學校等24所
                                </div>

                                <BR id="all" style="display: none;"/>

                                <div class="cc_Lv01_title" id="all" style="display: none;">高職學校</div>
                                <div class="cc_Lv01_title_b" id="all" style="display: none;">公立</div>
                                <div class="cc_Lv01_txt" id="all" style="display: none;">
                                    大安高工、士林高商、木柵高工、內湖高工、松山工農、松山家商、南港高工、國立臺灣戲曲學院（高職部）等共8所
                                </div>
                                <div class="cc_Lv01_title_b" id="all" style="display: none;">私立</div>
                                <div class="cc_Lv01_txt" id="all" style="display: none;">
                                    育達家商、東方工商、惇敘工商、喬治工商、開平餐飲、開南商工、華岡藝校、稻江高商、稻江護家等9所
                                </div>

                                <BR id="all" style="display: none;"/>

                                <div class="cc_Lv01_title" id="all" style="display: none;">特教學校</div>
                                <div class="cc_Lv01_title_b" id="all" style="display: none;">公立</div>
                                <div class="cc_Lv01_txt" id="all" style="display: none;">文山特教、啟明學校、啟智學校、啟聰學校等4所</div>

                                <BR id="all" style="display: none;"/>

                                <div class="cc_Lv01_title" id="all" style="display: none;">五專（包含新北市及基隆市學校）</div>
                                <div class="cc_Lv01_title_b" id="all" style="display: none;">公立</div>
                                <div class="cc_Lv01_txt" id="all" style="display: none;">國立臺北商業大學、國立臺北科技大學</div>
                                <div class="cc_Lv01_title_b" id="all" style="display: none;">私立</div>
                                <div class="cc_Lv01_txt" id="all" style="display: none;">
                                    聖約翰科技大學、臺北城市科技大學、醒吾科技大學、華夏科技大學、致理科技大學、宏國德霖科技大學、黎明技術學院、經國管理暨健康學院、台北海洋科技大學、馬偕醫護管理專科學校、耕莘健康管理專科學校、康寧大學(臺北校區)等12所
                                </div>

                                <div class="cc_Lv01_title" id="advanced" style="display: none;">高中學校</div>
                                <div class="cc_Lv01_title_b" id="advanced"
                                     style="display: none;">私立
                                </div>
                                <div class="cc_Lv01_txt" id="advanced" style="display: none;">大誠高中、南華進修學校、泰北高中、強恕高中
                                </div>

                                <BR id="advanced" style="display: none;"/>

                                <div class="cc_Lv01_title" id="advanced" style="display: none;">高職學校</div>
                                <div class="cc_Lv01_title_b" id="advanced"
                                     style="display: none;">公立
                                </div>
                                <div class="cc_Lv01_txt" id="advanced" style="display: none;">大安高工、士林高商</div>
                                <div class="cc_Lv01_title_b" id="advanced"
                                     style="display: none;">私立
                                </div>
                                <div class="cc_Lv01_txt" id="advanced" style="display: none;">東方工商、喬治工商、開平餐飲、開南商工、稻江高商
                                </div>

                                <div class="cc_Lv01_title" id="combined" style="display: none;">高中學校</div>
                                <div class="cc_Lv01_title_b" id="combined"
                                     style="display: none;">公立
                                </div>
                                <div class="cc_Lv01_txt" id="combined" style="display: none;">大理高中</div>
                                <div class="cc_Lv01_title_b" id="combined"
                                     style="display: none;">私立
                                </div>
                                <div class="cc_Lv01_txt" id="combined" style="display: none;">滬江高中</div>

                                <BR id="combined" style="display: none;"/>

                                <div class="cc_Lv01_title" id="combined" style="display: none;">高職學校</div>
                                <div class="cc_Lv01_title_b" id="combined"
                                     style="display: none;">公立
                                </div>
                                <div class="cc_Lv01_txt" id="combined" style="display: none;">大安高工、木柵高工、松山工農、南港高工</div>
                                <div class="cc_Lv01_title_b" id="combined"
                                     style="display: none;">私立
                                </div>
                                <div class="cc_Lv01_txt" id="combined" style="display: none;">開南商工</div>

                                <div class="cc_Lv01_title" id="vocational"
                                     style="display: none;">高中學校
                                </div>
                                <div class="cc_Lv01_title_b" id="vocational"
                                     style="display: none;">私立
                                </div>
                                <div class="cc_Lv01_txt" id="vocational" style="display: none;">
                                    十信高中、大同高中、大誠高中、金甌女中、協和祐德高中、南華進修學校、泰北高中、強恕高中、景文高中、滬江高中
                                </div>

                                <BR id="vocational" style="display: none;"/>

                                <div class="cc_Lv01_title" id="vocational" style="display: none;">高中學校</div>
                                <div class="cc_Lv01_title_b" id="vocational" style="display: none;">私立</div>
                                <div class="cc_Lv01_txt" id="vocational" style="display: none;">
                                    十信高中、大同高中、大誠高中、文德女中、金甌女中、協和祐德高中、南華進修學校、泰北高中、強恕高中、景文高中、滬江高中、靜修女中
                                </div>
                            </div>

                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>
<script type="text/javascript">
    $(document).ready(function () {
        showAll($(".catalog_content_list_box > a:eq(0)"));
    });

    function showAll(a) {
        $(".catalog_content_list_box > a").removeAttr("class");
        $(a).attr("class", "hover");

        $("div#all").each(function () {
            $(this).show();
        });

        $("br#all").each(function () {
            $(this).show();
        });

        $("div#advanced").each(function () {
            $(this).hide();
        });

        $("br#advanced").each(function () {
            $(this).hide();
        });

        $("div#combined").each(function () {
            $(this).hide();
        });

        $("br#combined").each(function () {
            $(this).hide();
        });

        $("div#vocational").each(function () {
            $(this).hide();
        });
    }

    function showAdvanced(a) {
        $(".catalog_content_list_box > a").removeAttr("class");
        $(a).attr("class", "hover");

        $("div#all").each(function () {
            $(this).hide();
        });

        $("br#all").each(function () {
            $(this).hide();
        });

        $("div#advanced").each(function () {
            $(this).show();
        });

        $("br#advanced").each(function () {
            $(this).show();
        });

        $("div#combined").each(function () {
            $(this).hide();
        });

        $("br#combined").each(function () {
            $(this).hide();
        });

        $("div#vocational").each(function () {
            $(this).hide();
        });
    }

    function showCombined(a) {
        $(".catalog_content_list_box > a").removeAttr("class");
        $(a).attr("class", "hover");

        $("div#all").each(function () {
            $(this).hide();
        });

        $("br#all").each(function () {
            $(this).hide();
        });

        $("div#advanced").each(function () {
            $(this).hide();
        });

        $("br#advanced").each(function () {
            $(this).hide();
        });

        $("div#combined").each(function () {
            $(this).show();
        });

        $("br#combined").each(function () {
            $(this).show();
        });

        $("div#vocational").each(function () {
            $(this).hide();
        });
    }

    function showVocational(a) {
        $(".catalog_content_list_box > a").removeAttr("class");
        $(a).attr("class", "hover");

        $("div#all").each(function () {
            $(this).hide();
        });

        $("br#all").each(function () {
            $(this).hide();
        });

        $("div#advanced").each(function () {
            $(this).hide();
        });

        $("br#advanced").each(function () {
            $(this).hide();
        });

        $("div#combined").each(function () {
            $(this).hide();
        });

        $("br#combined").each(function () {
            $(this).hide();
        });

        $("div#vocational").each(function () {
            $(this).show();
        });
    }
</script>
</body>
</html>