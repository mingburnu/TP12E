<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="set_01.jsp"/>
        <jsp:param name="aName" value="目標及配套"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="目標策略"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="1" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="13" name="title"/>
                            <jsp:param value="1" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">目標策略</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">一、臺北市推動12年國教目標</div>
                                <ul class="cc_Lv01_list">
                                    <li>推動策略與行動切實掌握教育部12年國民基本教育三大願景：提升教育品質、成就每一個孩子、厚植國家競爭力。</li>
                                    <li>校長有能力帶領教師聚焦學生學習，發展為學習共同體，打造學生學習的贏家學校。</li>
                                    <li>幫助國中教師活化教學，101學年度本市國七、國八的教學現場能轉向活化教學、以學生為中心的教學。</li>
                                </ul>


                                <div class="cc_Lv01_title">二、推動策略</div>
                                <ul class="cc_Lv01_list">
                                    <li>推動原則：面對變革，需要專業與團隊，以團隊力量迎向變革。致力推動學校發展PLC(Professional
                                        Learning Community)，形成專業團隊。
                                    </li>
                                    <li>方向架構：制定推動架構及臺北市K-12學生六項核心能力。
                                        <div class="cc_Lv02_box">
                                            <div class="cc_Lv03_box">
                                                <div class="cc_Lv04_pic">
                                                    <a target="_blank"
                                                       href="${pageContext.request.contextPath}/templates/images/pic_001.png">
                                                        <img src="${pageContext.request.contextPath}/templates/images/pic_001.png"
                                                             width="100%" alt="臺北市推動十二年國民基本教育核心工作架構圖"></a>
                                                </div>
                                                <strong>臺北市推動十二年國民基本教育核心工作架構</strong>
                                                <ul>
                                                    <li>課程與教
                                                        <ul>
                                                            <li>國中活化教學 高中職領先計畫</li>
                                                            <li>高中職發展課程</li>
                                                            <li>中等學校課程與教學補助計畫</li>
                                                        </ul>
                                                    </li>
                                                    <li>校長專業發展
                                                        <ul>
                                                            <li>校長培力課程 系統思考 課程教學領導</li>
                                                            <li>校長觀課 教室走察 發展學習共同體</li>
                                                            <li>跨國參訪學習 取得現場感動與團體動力</li>
                                                        </ul>
                                                    </li>
                                                    <li>教師專業發展
                                                        <ul>
                                                            <li>各學科/領域專業學習社群PLC</li>
                                                            <li>學科/領域輔導團 國高中職 精進教學</li>
                                                            <li>跨國參訪學習 取得現場感動與團體動力</li>
                                                        </ul>
                                                    </li>
                                                    <li>學生全面學習
                                                        <ul>
                                                            <li>品格力 學習力</li>
                                                            <li>閱讀力 思考力</li>
                                                            <li>創造力 移動力</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>


                                            <div class="cc_Lv03_box">
                                                <div class="cc_Lv04_pic">
                                                    <a target="_blank"
                                                       href="${pageContext.request.contextPath}/templates/images/pic_002.png">
                                                        <img src="${pageContext.request.contextPath}/templates/images/pic_002.png"
                                                             width="100%" alt="臺北市中小學K-12的六項核心能力"></a>
                                                </div>
                                                <strong>臺北市中小學K-12的六項核心能力</strong>
                                                <ul>
                                                    <li>品格力：學校核心品格 核心品格的內化與實踐體現</li>
                                                    <li>學習力：思考力、探究力、問題解決、統整力、創造力</li>
                                                    <li>閱讀力：大量閱讀的習慣與能力 瞭解使用與反映文字的 內容，以助達成個人的目標
                                                        發展個人的知識潛能以及社會參與
                                                    </li>
                                                    <li>思考力：能反思、能提問 高層次思考、批判思考</li>
                                                    <li>創造力：具流暢性、變通性、原創性、精進性 的水平/擴散思考力</li>
                                                    <li>移動力：資訊處理能力、外(英)語能力、異質團體內合作能力、公共性參與能力</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>建構系統環境
                                        <ol class="cc_Lv02_list">
                                            <li>經費籌措，資源到位</li>
                                            <li>建立聚焦學生學習的系統環境(校務評鑑、優質學校、校長考評與遴選)</li>
                                            <li>開學前公布特色招生考試方式具體說明，引領國中活化教學，培養學習力。</li>
                                        </ol>
                                    </li>
                                    <li>強化校長專業領導
                                        <div class="cc_Lv02_box">
                                            裝備並引導校長除了傳統的行政領導，擔起課程教學領導責任。推動校長走察與觀課、領導學校校本課程發展、帶領教師專業學習社群。
                                        </div>
                                    </li>
                                    <li>由點而面的啟動教師專業社群
                                        <ol class="cc_Lv02_list">
                                            <li>
                                                國中方面：開學前辦理全市五大領域召集人(350人)三天研習，做好全市一致的開學備課準備。各校領域召集人返校後，於開學前完成學校領域教師開學前的研習。
                                            </li>
                                            <li>高中職以領先計畫，促使全校教師投入專業社群，進行教師專業對話與聚焦學生學習的產出(課程規劃、教學設計、學習診斷、多元評量等)。</li>
                                        </ol>
                                    </li>
                                </ul>


                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>

    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>