<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="set_01.jsp"/>
        <jsp:param name="aName" value="目標及配套"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="辦理時程（重要日程）"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="1" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="13" name="title"/>
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">辦理時程（重要日程）</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">107學年度 基北區各入學管道重要日程</div>
                                <div class="cc_Lv01_pic">
                                    <a target="_blank"
                                       href="${pageContext.request.contextPath}/templates/images/pic_005.png">
                                        <img src="${pageContext.request.contextPath}/templates/images/pic_005.png"
                                             alt="107學年度基北區各入學管道重要日程" width="800" height="auto"></a>
                                </div>

                                <BR/>


                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <caption>
                                            107學年度 基北區適性入學管道重要行事曆
                                        </caption>
                                        <tr>
                                            <th>辦理日期 </th>
                                            <th>工作項目 </th>
                                        </tr>
                                        <tr>
                                            <td>107/03/01(四)-107/03/07(三)</td>
                                            <td>藝才班術科測驗報名 </td>
                                        </tr>
                                        <tr>
                                            <td>107/03/08(四)-107/03/09(五)</td>
                                            <td>科學班報名 </td>
                                        </tr>
                                        <tr>
                                            <td>107/03/15(四)-107/03/17(六)</td>
                                            <td>國中教育會考國中端集體報名 </td>
                                        </tr>
                                        <tr>
                                            <td>107/03/19(一)-107/03/23(五)</td>
                                            <td>專業群科(高職)甄選入學報名</td>
                                        </tr>
                                        <tr>
                                            <td>107/04/28(六)-107/04/29(日)</td>
                                            <td>專業群科(高職)甄選入學術科測驗</td>
                                        </tr>
                                        <tr>
                                            <td>107/04/30(一)-107/05/02(三)</td>
                                            <td>體育班、運動績優生(獨招學校)術科測驗報名</td>
                                        </tr>
                                        <tr>
                                            <td>107/04/30(一)-107/05/04(五)</td>
                                            <td>免試入學變更就學區申請</td>
                                        </tr>
                                        <tr>
                                            <td>107/05/05(六)</td>
                                            <td>體育班、運動績優生(獨招學校)術科測驗</td>
                                        </tr>
                                        <tr>
                                            <td>107/05/19(六)-107/05/20(日)</td>
                                            <td>國中教育會考 </td>
                                        </tr>
                                        <tr>
                                            <td>107/05/21(一)-107/05/22(二)</td>
                                            <td>優先免試入學第一類學校報名</td>
                                        </tr>
                                        <tr>
                                            <td>107/05/21(一)-107/05/25(五)</td>
                                            <td>五專優先免試入學報名</td>
                                        </tr>
                                        <tr>
                                            <td>107/05/23(三)-107/05/24(四)</td>
                                            <td>技優甄審入學報名、實用技能學程報名</td>
                                        </tr>
                                        <tr>
                                            <td>107/05/24(四)</td>
                                            <td>優先免試入學第一類學校公告報名結果</td>
                                        </tr>
                                        <tr>
                                            <td>107/05/25(五)</td>
                                            <td>優先免試入學第一類學校抽籤、撕榜、報到</td>
                                        </tr>
                                        <tr>
                                            <td>107/05/31(四)-107/06/08(五)</td>
                                            <td>直升入學報名</td>
                                        </tr>
                                        <tr>
                                            <td>107/06/02(六)</td>
                                            <td>運動績優生甄試學科測驗</td>
                                        </tr>
                                        <tr>
                                            <td>107/06/03(日)</td>
                                            <td>運動績優生甄試術科測驗</td>
                                        </tr>
                                        <tr>
                                            <td>107/06/04(一)-107/06/11(一)</td>
                                            <td>優先免試入學第二類學校報名</td>
                                        </tr>
                                        <tr>
                                            <td>107/06/08(五)</td>
                                            <td>寄發國中教育會考成績單並開放網路查詢</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">107/06/11(一)</td>
                                            <td>1. 優先免試入學第一類學校報到後聲明放棄錄取資格</td>
                                        </tr>
                                        <tr>
                                            <td>2. 實用技能學程放榜</td>
                                        </tr>
                                        <tr>
                                            <td>107/06/11(一)-107/06/15(五)</td>
                                            <td>藝才班分發報名 </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">107/06/12(二)</td>
                                            <td>1. 技優甄審入學放榜</td>
                                        </tr>
                                        <tr>
                                            <td>2. 特色招生專業群科甄選入學放榜</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="5">107/6/13(三)</td>
                                            <td>1. 技優甄審入學報到</td>
                                        </tr>
                                        <tr>
                                            <td>2. 實用技能學程報到</td>
                                        </tr>
                                        <tr>
                                            <td>3. 特色招生專業群科甄選入學報到</td>
                                        </tr>
                                        <tr>
                                            <td>4. 各校直升入學放榜截止日</td>
                                        </tr>
                                        <tr>
                                            <td>5. 五專優先免試入學放榜</td>
                                        </tr>
                                        <tr>
                                            <td>107/6/14(四)</td>
                                            <td>各校直升入學報到(含報到後聲明放棄綠取資格)截止日</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="5">107/6/15(五)</td>
                                            <td>1. 優先免試入學第二類學校公告分發序</td>
                                        </tr>
                                        <tr>
                                            <td>2. 技優甄審入學報到後聲明放棄錄取資格</td>
                                        </tr>
                                        <tr>
                                            <td>3. 實用技能學程報到後聲明放棄錄取資格</td>
                                        </tr>
                                        <tr>
                                            <td>4. 特色招生專業群科甄選入學報到後聲明放棄綠取資格、遞補截止日</td>
                                        </tr>
                                        <tr>
                                            <td>5. 五專優先免試入學報到(含報到後聲明放棄綠取資格)截止日</td>
                                        </tr>
                                        <tr>
                                            <td>107/06/19(二)</td>
                                            <td>優先免試入學第二類學校登記、撕榜、報到、放棄</td>
                                        </tr>
                                        <tr>
                                            <td>107/06/20(三)-107/07/2(一)</td>
                                            <td>五專免試入學報名 </td>
                                        </tr>
                                        <tr>
                                            <td>107/06/21(四)</td>
                                            <td>基北區公告免試入學實際招生名額、開放個人序位查詢及志願選填</td>
                                        </tr>
                                        <tr>
                                            <td>107/06/22(五)</td>
                                            <td>特色招生考試分發入學學科測驗報名截止</td>
                                        </tr>
                                        <tr>
                                            <td>107/06/24(日)</td>
                                            <td>特色招生考試分發入學學科測驗 </td>
                                        </tr>
                                        <tr>
                                            <td>107/06/26(二)-107/06/27(三)</td>
                                            <td>特色招生考試分發入學開放網路查詢學科測驗分數及志願選填 </td>
                                        </tr>
                                        <tr>
                                            <td>107/06/28(四)</td>
                                            <td>個人序位查詢及志願選填截止</td>
                                        </tr>
                                        <tr>
                                            <td>107/07/03(二)</td>
                                            <td>國中端集體報名高級中等學校免試入學及特色招生考試分發入學分發截止日 </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">107/07/10(二)</td>
                                            <td>1. 免試入學及特色招生考試分發入學放榜</td>
                                        </tr>
                                        <tr>
                                            <td>2. 藝才班(獨招學校)放榜</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">107/07/11(三)</td>
                                            <td>1. 五專免試入學現場登記分發報到</td>
                                        </tr>
                                        <tr>
                                            <td>2. 藝才班各類各區分發報到</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="4">107/07/13(五)</td>
                                            <td>1. 免試入學及特色招生考試分發入學報到</td>
                                        </tr>
                                        <tr>
                                            <td>2. 藝才班(獨招學校)報到</td>
                                        </tr>
                                        <tr>
                                            <td>3. 建教合作班入學報到截止日</td>
                                        </tr>
                                        <tr>
                                            <td>4. 體育班、運動績優生(獨招學校)報到截止日</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">107/07/16(一)</td>
                                            <td>1. 免試入學及特色招生考試分發入學報到後聲明放棄綠取資格截止日</td>
                                        </tr>
                                        <tr>
                                            <td>2. 體育班、運動績優生(獨招學校)報到後聲</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>
</td>
</tr>
</table>
<!-- 內容 區塊 End -->
</div>


</div>
<!-- 主內容 區塊 End -->

<!-- footer 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
<!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>