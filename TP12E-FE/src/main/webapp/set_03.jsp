<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />

<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->

		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="set_01.jsp" />
			<jsp:param name="aName" value="目標及配套" />
			<jsp:param name="subPosition" value="<%=request.getRequestURI()%>" />
			<jsp:param name="subAName" value="國中活化教學" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><jsp:include
								page="/WEB-INF/jsp/layout/listBox.jsp">
								<jsp:param value="1" name="hover" />
							</jsp:include></td>
						<td class="layout_04_02"><jsp:include
								page="/WEB-INF/jsp/layout/subListBox.jsp">
								<jsp:param value="13" name="title" />
								<jsp:param value="3" name="hover" />
							</jsp:include>

							<div class="catalog_content_box">
								<div class="title">國中活化教學</div>
								<div class="catalog_content">
									<div class="cc_Lv01_txt">國中活化教學配合教育部持續推動精進教學計畫，並擬定配合十二年國民基本教育臺北市提升國中小課程與教學品質整體計畫及推動臺北市國中提升教學及評量效能三年計畫。在此計畫下舉辦國中活化教學設計工作坊，分領域分區（東西南北四區）調訓五大領域教師，參與精進教學—含教學與評量—產出式研習。此研習為產出式研習，在六週內分三個半天進行。各領域課程單元活化教學設計徵件比賽：102年辦理全市國中分區（東西南北四區）座談會，各學科領域輔導團分區進行學校特色課程及活化教學經驗分享與交流，然後辦理各領域課程單元活化教學設計徵件及評選活動。</div>
								</div>

								<div class="catalog_file_list">
									<div class="cf_Lv01_title">相關檔案</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-配合12年國教臺北市提升國民中學教師教學與評量效能實施計畫.doc"
											title="01-配合12年國教臺北市提升國民中學教師教學與評量效能實施計畫.doc">●
											01-配合12年國教臺北市提升國民中學教師教學與評量效能實施計畫.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-國中工作圈活化教學三年計畫.doc"
											title="02-國中工作圈活化教學三年計畫.doc">● 02-國中工作圈活化教學三年計畫.doc</a> <a
											class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/03-國文領域活化教學工作坊計畫.doc"
											title="03-國文領域活化教學工作坊計畫.doc">● 03-國文領域活化教學工作坊計畫.doc</a> <a
											class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/04-英語領域活化教學工作坊計畫.doc"
											title="04-英語領域活化教學工作坊計畫.doc">● 04-英語領域活化教學工作坊計畫.doc</a> <a
											class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/05-數學領域活化教學工作坊計畫.doc"
											title="05-數學領域活化教學工作坊計畫.doc">● 05-數學領域活化教學工作坊計畫.doc</a> <a
											class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/06-自然領域活化教學工作坊計畫.doc"
											title="06-自然領域活化教學工作坊計畫.doc">● 06-自然領域活化教學工作坊計畫.doc</a> <a
											class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/07-社會領域活化教學工作坊計畫.doc"
											title="07-社會領域活化教學工作坊計畫.doc">● 07-社會領域活化教學工作坊計畫.doc</a>
									</div>
								</div>

							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->
			</div>


		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />

</body>
</html>