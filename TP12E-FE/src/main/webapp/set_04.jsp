<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />

<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->

		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="set_01.jsp" />
			<jsp:param name="aName" value="目標及配套" />
			<jsp:param name="subPosition" value="<%=request.getRequestURI()%>" />
			<jsp:param name="subAName" value="高中職領先計畫" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><jsp:include
								page="/WEB-INF/jsp/layout/listBox.jsp">
								<jsp:param value="1" name="hover" />
							</jsp:include></td>
						<td class="layout_04_02"><jsp:include
								page="/WEB-INF/jsp/layout/subListBox.jsp">
								<jsp:param value="13" name="title" />
								<jsp:param value="4" name="hover" />
							</jsp:include>

							<div class="catalog_content_box">
								<div class="title">高中職領先計畫</div>
								<div class="catalog_content">
									<div class="cc_Lv01_txt">領先計畫為一項5年6億的課程發展競爭型經費，鼓勵、引導高中職學校積極發展校本課程，以專業社群提升教師教學與學生學習，裝備教師差異化教學能力，提升學生學習效果。從102年度起至106年度止，分3個期程辦理，每個期程3年，申請通過的每校最高可獲得3年總共1千5百萬的經費補助。為了協助各校發展色課程，教育局舉辦高中課程發展工作坊，聘請專家以及以工作臺分享的方式，提供學校觀摩學習的機會</div>
								</div>

								<div class="catalog_file_list">
									<div class="cf_Lv01_title">相關檔案</div>
									<div class="cf_Lv01_title_b">領先計畫</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-臺北市公私立高中職102年度至106年度課程與教學領先計畫.doc">●
											01-臺北市公私立高中職102年度至106年度課程與教學領先計畫.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-申請書格式.doc">●
											02-申請書格式.doc</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/03-領先計畫審查表.doc">●
											03-領先計畫審查表.doc</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/04-計畫審查說明.doc">●
											04-計畫審查說明.doc</a>
									</div>

									<div class="cf_Lv01_title_b">課程發展</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-12年國教高中職課程發展計畫_高中篇.doc">●
											01-12年國教高中職課程發展計畫_高中篇.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-課程發展1_手冊.doc">●
											02-課程發展1_手冊.doc</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/03-課程發展2_手冊.doc">●
											03-課程發展2_手冊.doc</a>
									</div>
								</div>

							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->
			</div>


		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />

</body>
</html>