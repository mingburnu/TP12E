<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />

<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->


		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="set_01.jsp" />
			<jsp:param name="aName" value="目標及配套" />
			<jsp:param name="subPosition" value="<%=request.getRequestURI()%>" />
			<jsp:param name="subAName" value="課程與教學領導" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><jsp:include
								page="/WEB-INF/jsp/layout/listBox.jsp">
								<jsp:param value="1" name="hover" />
							</jsp:include></td>
						<td class="layout_04_02"><jsp:include
								page="/WEB-INF/jsp/layout/subListBox.jsp">
								<jsp:param value="13" name="title" />
								<jsp:param value="5" name="hover" />
							</jsp:include>

							<div class="catalog_content_box">
								<div class="title">課程與教學領導</div>
								<div class="catalog_content">
									<div class="cc_Lv01_txt">在教育轉型與改革的重要期，除了傳統的行政領導外，校長更要成為「課程及教學」的領導者。教育局投入資源提升校長專業領導，讓校長更聚焦於學校的課程發展、教師教學及學生學習。配套措施含國高中職課程發展領導系列工作坊、校長觀課及教室走察工作坊、國高中職校長培力工作坊等。</div>
								</div>

								<div class="catalog_file_list">
									<div class="cf_Lv01_title">相關檔案</div>
									<div class="cf_Lv01_title_b">國高中職課程發展領導系列工作坊</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-與未來接軌的課程和世界級的學習.doc">●
											01-與未來接軌的課程和世界級的學習.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-上課簡報檔.ppt">●
											02-上課簡報檔.ppt</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/03-課程領導實務工作坊_UbD.doc">●
											03-課程領導實務工作坊_UbD.doc</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/04-上課簡報檔.ppt">●
											04-上課簡報檔.ppt</a>
									</div>

									<div class="cf_Lv01_title_b">校長觀課及教室走察工作坊</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-12國教校長觀課暨教室走察工作坊計畫.doc">●
											01-12國教校長觀課暨教室走察工作坊計畫.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-工作坊手冊.doc">●
											02-工作坊手冊.doc</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/03-工作坊手冊.doc">●
											03-工作坊手冊.doc</a>
									</div>

									<div class="cf_Lv01_title_b">國高中職校長培力工作坊</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-北市校長培力工作坊實施計畫.doc">●
											01-北市校長培力工作坊實施計畫.doc</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-北市校長培力工作坊第一梯次手冊.pdf">●
											02-北市校長培力工作坊第一梯次手冊.pdf</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/03-第一梯校培工作坊照片PPT.ppsx">●
											03-第一梯校培工作坊照片PPT.ppsx</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/04-北市校長培力工作坊第二梯次手冊.docx">●
											04-北市校長培力工作坊第二梯次手冊.docx</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/05-第二梯校陪工作坊照片.flv">●
											05-第二梯校陪工作坊照片.flv</a>
									</div>
								</div>

							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->
			</div>


		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />

</body>
</html>