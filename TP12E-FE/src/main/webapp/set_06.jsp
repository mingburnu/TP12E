<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />

<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->


		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="set_01.jsp" />
			<jsp:param name="aName" value="目標及配套" />
			<jsp:param name="subPosition" value="<%=request.getRequestURI()%>" />
			<jsp:param name="subAName" value="教師專業學習社群" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><jsp:include
								page="/WEB-INF/jsp/layout/listBox.jsp">
								<jsp:param value="1" name="hover" />
							</jsp:include></td>
						<td class="layout_04_02"><jsp:include
								page="/WEB-INF/jsp/layout/subListBox.jsp">
								<jsp:param value="13" name="title" />
								<jsp:param value="6" name="hover" />
							</jsp:include>

							<div class="catalog_content_box">
								<div class="title">教師專業學習社群</div>
								<div class="catalog_content">
									<div class="cc_Lv01_txt">12年國民教育後，教師社群更為重要，各學科領域得召集人成為社群的核心人物。領域召集人的遴聘與權利義務朝向制度化、專業化發展，要求各校領召應具備一定資格(專業及熱忱)，應參加本市學科領域輔導團辦理之全市學科領域召集人專業學習社群工作坊、分區學科領域召集人專業社群運作，裝備領召可有效帶領學校該領域專業學習社群。</div>
								</div>

								<div class="catalog_file_list">
									<div class="cf_Lv01_title">相關檔案</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-臺北市國民中學領域召集人設置計畫.doc">●
											01-臺北市國民中學領域召集人設置計畫.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-五大領域召集人工作坊計畫.doc">●
											02-五大領域召集人工作坊計畫.doc</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/03-領召返校工作說明簡報檔.ppt">●
											03-領召返校工作說明簡報檔.ppt</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/04-各校社群工作坊計畫格式.doc">●
											04-各校社群工作坊計畫格式.doc</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/05-社群成果報告表.doc">●
											05-社群成果報告表.doc</a> <a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/06-領域出國申請表.doc">●
											06-領域出國申請表.doc</a>
									</div>

								</div>

							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->
			</div>


		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />

</body>
</html>