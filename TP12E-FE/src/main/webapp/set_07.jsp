<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />

<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->


		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="set_01.jsp" />
			<jsp:param name="aName" value="目標及配套" />
			<jsp:param name="subPosition" value="<%=request.getRequestURI()%>" />
			<jsp:param name="subAName" value="學習共同體試辦" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><jsp:include
								page="/WEB-INF/jsp/layout/listBox.jsp">
								<jsp:param value="1" name="hover" />
							</jsp:include></td>
						<td class="layout_04_02"><jsp:include
								page="/WEB-INF/jsp/layout/subListBox.jsp">
								<jsp:param value="13" name="title" />
								<jsp:param value="7" name="hover" />
							</jsp:include>

							<div class="catalog_content_box">
								<div class="title">學習共同體試辦</div>
								<div class="catalog_content">
									<div class="cc_Lv01_txt">面對十二年國教，因應入學方式不同及學生異質化趨勢，學校現場也應將過去關注教師教學、教材教法，轉化為以學生學習為主體的教學方式。透過以學習共同體為導向的教師專業學習社群運作，加強教師合作與專業對話，提昇學生的學習動機和學習成效，展現多元學習的面貌，進一步達到全面提升教育品質的目標。基此，臺北市政府教育局（以下簡稱本局）特規劃「學習共同體及授業研究試辦計畫」（以下簡稱本計畫），以支持學校由下而上以「學生學習」為核心的教學改革，並期待發展出「Learning
										Community(學習共同體)」及「Lesson Study(授業研究)」之本土模式。</div>
								</div>

								<div class="catalog_file_list">
									<div class="cf_Lv01_title">相關檔案</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-國高中學習共同體及授業研究試辦計畫.doc">●
											01-國高中學習共同體及授業研究試辦計畫.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-國高中學習共同體試辦學校一覽表.doc">●
											02-國高中學習共同體試辦學校一覽表.doc</a>
									</div>

								</div>

							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->
			</div>


		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />

</body>
</html>