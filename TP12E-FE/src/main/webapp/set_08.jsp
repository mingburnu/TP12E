<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />

<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->

		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="set_01.jsp" />
			<jsp:param name="aName" value="目標及配套" />
			<jsp:param name="subPosition" value="<%=request.getRequestURI()%>" />
			<jsp:param name="subAName" value="素養評量工作坊" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><jsp:include
								page="/WEB-INF/jsp/layout/listBox.jsp">
								<jsp:param value="1" name="hover" />
							</jsp:include></td>
						<td class="layout_04_02"><jsp:include
								page="/WEB-INF/jsp/layout/subListBox.jsp">
								<jsp:param value="13" name="title" />
								<jsp:param value="8" name="hover" />
							</jsp:include>

							<div class="catalog_content_box">
								<div class="title">素養評量工作坊</div>
								<div class="catalog_content">
									<div class="cc_Lv01_txt">各領域的教學應強調閱讀素養，著重培養閱讀理解的有效學習策略，包括做摘要、預測、自問自答、做反思等。教師宜利用所授學科的文本，帶領學生依照上述閱讀策略進行學習。以提問方式，引導學生進行高層次思考，啟發學生對文本的理解，建立自己的見解。教育局對於國中反映教師教學評量的需求，籌組學者教授團，將自10月下旬開始對北市國中各校四大領域種子教師進行素養評量工作坊。</div>
								</div>

								<div class="catalog_file_list">
									<div class="cf_Lv01_title">相關檔案</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-臺北市101學年度素養工作坊計畫.doc">●
											01-臺北市101學年度素養工作坊計畫.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-素養工作坊課程內容範本.doc">●
											02-素養工作坊課程內容範本.doc</a>
									</div>

								</div>

							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->
			</div>


		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />

</body>
</html>