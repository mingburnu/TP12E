<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />

<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->


		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="set_01.jsp" />
			<jsp:param name="aName" value="目標及配套" />
			<jsp:param name="subPosition" value="<%=request.getRequestURI()%>" />
			<jsp:param name="subAName" value="跨國參訪報告" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><jsp:include
								page="/WEB-INF/jsp/layout/listBox.jsp">
								<jsp:param value="1" name="hover" />
							</jsp:include></td>
						<td class="layout_04_02"><jsp:include
								page="/WEB-INF/jsp/layout/subListBox.jsp">
								<jsp:param value="13" name="title" />
								<jsp:param value="9" name="hover" />
							</jsp:include>

							<div class="catalog_content_box">
								<div class="title">跨國參訪報告</div>
								<div class="catalog_content">
									<div class="cc_Lv01_txt">「臺北走出去，世界走進來」深化教師跨國參訪學習，一則獲得實地參訪的感動與激勵；一則擴大教師的經驗與視野，轉化為熱情與行動力，落實臺北市與國際接軌，培養孩子的國際競爭力。</div>
								</div>

								<div class="catalog_file_list">
									<div class="cf_Lv01_title">相關檔案</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-日本學習共同體參訪報告_中正高中簡校長菲莉.doc">●
											01-日本學習共同體參訪報告_中正高中簡校長菲莉.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/02-校長觀點看學習共同體試辦_新興國中謝校長勝隆.doc">●
											02-校長觀點看學習共同體試辦_新興國中謝校長勝隆.doc</a> <a class="download_icon"
											target="_blank"
											href="${pageContext.request.contextPath}/resources/download/03-學習共同體參訪報告_北政國中高校長松景.doc">●
											03-學習共同體參訪報告_北政國中高校長松景.doc</a>
									</div>

								</div>

							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->
			</div>


		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />

</body>
</html>