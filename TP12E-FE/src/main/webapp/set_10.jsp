<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp" />

<body>
	<div class="wrapper">

		<!-- top 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/top.jsp" />
		<!-- top 區塊 End -->


		<!-- header 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/header.jsp">
			<jsp:param name="detail" value="1" />
		</jsp:include>
		<!-- header 區塊 End -->

		<!-- crumbs 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
			<jsp:param name="position" value="set_01.jsp" />
			<jsp:param name="aName" value="目標及配套" />
			<jsp:param name="subPosition" value="<%=request.getRequestURI()%>" />
			<jsp:param name="subAName" value="學生生涯輔導" />
		</jsp:include>
		<!-- crumbs 區塊 End -->

		<!-- 主內容 區塊 Begin -->
		<div class="container">
			<div class="innerwrapper">

				<!-- 內容 區塊 Begin -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					summary="版型表格：選單主頁區塊" class="layout_04">
					<tr valign="top">
						<td class="layout_04_01"><jsp:include
								page="/WEB-INF/jsp/layout/listBox.jsp">
								<jsp:param value="1" name="hover" />
							</jsp:include></td>
						<td class="layout_04_02"><jsp:include
								page="/WEB-INF/jsp/layout/subListBox.jsp">
								<jsp:param value="13" name="title" />
								<jsp:param value="10" name="hover" />
							</jsp:include>

							<div class="catalog_content_box">
								<div class="title">學生生涯輔導</div>
								<div class="catalog_content">
									<div class="cc_Lv01_txt">
										1、本局業於100年8月1日成立學生輔導諮商中心，並於其下設立適性輔導組，另同步成立國中生涯發展教育中心學校，並結合國中學生輔導工作執行小組，積極整合協調推動國中學生適性輔性輔導工作
										<br /> <br />
										2、101年度起各校皆須參加「國民中小學攜手計畫課後扶助學生篩選追蹤輔導轉銜試辦計畫」，經初步篩選符合參加攜手計畫資格之學生皆須參加篩選測驗（7年級學生不限身分別），測驗科目包含國文、數學、英文及自然，101年2月及6月各校施測率需達90％以上。
									</div>
								</div>

								<div class="catalog_file_list">
									<div class="cf_Lv01_title">相關檔案</div>
									<div class="cf_Lv01_list">
										<a class="download_icon" target="_blank"
											href="${pageContext.request.contextPath}/resources/download/01-臺北市國中學生生涯輔導紀錄手冊資訊化教育訓練計畫.doc">●
											01-臺北市國中學生生涯輔導紀錄手冊資訊化教育訓練計畫.doc</a>
									</div>

								</div>

							</div></td>
					</tr>
				</table>
				<!-- 內容 區塊 End -->
			</div>


		</div>
		<!-- 主內容 區塊 End -->

		<!-- footer 區塊 Begin -->
		<jsp:include page="/WEB-INF/jsp/layout/footer.jsp" />
		<!-- footer 區塊 End -->

	</div>

	<!-- 執行javascript 區塊 Begin -->
	<jsp:include page="/WEB-INF/jsp/layout/js.jsp" />

</body>
</html>