<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="sitemap.jsp"/>
        <jsp:param name="aName" value="網站導覽"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <div class="sitemap">
                <div class="title">
                    <img src="${pageContext.request.contextPath}/templates/images/titles_11.png"
                         width="100" height="20" alt="網站導覽">
                </div>
                <div class="sitemap_help">
                    本網站依無障礙網頁設計原則而建置，網站的主要樣版內容分為三個大區塊：
                    1)上方選單區、2)左方選單區、3)主要內容區，以及4)相關連結區。本網站的便捷鍵﹝Accesskey,也稱為快速鍵﹞設定如下：<BR/>
                    Alt+U： 上方選單區，此區塊列有本網站的主要連結。<BR/> Alt+S： 搜尋功能區，此區塊列為搜尋各網頁的資料。<BR/>
                    Alt+C： 中間主要內容區，此區塊呈現各網頁的網頁內容。<BR/>
                </div>

                <div class="sitemap_title">「臺北市十二年國民基本教育資訊網」網站架構如下：</div>
                <ul class="sitemap_list">
                    <li><a href="${pageContext.request.contextPath}/news/more">1.消息列表</a></li>
                    <li>2.主題選單
                        <ul>
                            <li><a href="${pageContext.request.contextPath}/bctest.jsp">2-1.國中教育會考</a></li>
                            <li><a href="${pageContext.request.contextPath}/link.jsp">2-2.相關連結</a></li>
                            <li><a href="${pageContext.request.contextPath}/law.jsp">2-3.相關法規</a></li>
                            <li><a href="${pageContext.request.contextPath}/faq.jsp">2-4.常見問答集</a></li>
                        </ul>
                    </li>
                    <li>3.選單列表
                        <ul>
                            <li><a href="${pageContext.request.contextPath}/set_01.jsp">3-1.目標及配套</a>
                                <ul>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_01.jsp">3-1-01.目標策略</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_02.jsp">3-1-02.辦理時程（重要日程）</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_03.jsp">3-1-03.國中活化教學</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_04.jsp">3-1-04.高中職領先計畫</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_05.jsp">3-1-05.課程與教學領導</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_06.jsp">3-1-06.教師專業學習社群</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_07.jsp">3-1-07.學習共同體試辦</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_08.jsp">3-1-08.素養評量工作坊</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_09.jsp">3-1-09.跨國參訪報告</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/set_10.jsp">3-1-10.學生生涯輔導</a></li>
                                </ul>
                            </li>
                            <li><a
                                    href="${pageContext.request.contextPath}/start_school_01.jsp">3-2.入學方式</a>
                                <ul>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_01.jsp">3-2-1.基北區免試入學</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_02.jsp">3-2-2.臺北市優先免試入學</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_03.jsp">3-2-3.直升入學</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_04.jsp">3-2-4.技優甄審入學</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_05.jsp">3-2-5.實用技能學程</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_06.jsp">3-2-6.特色招生考試分發</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_07.jsp">3-2-7.科學班</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_08.jsp">3-2-8.體育班及運動績優</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_09.jsp">3-2-9.專業群科（高職）</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/start_school_10.jsp">3-2-10.藝術才能班</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a
                                    href="${pageContext.request.contextPath}/seek_01.jsp">3-3.宣導諮詢</a>
                                <ul>
                                    <li><a
                                            href="${pageContext.request.contextPath}/seek_01.jsp">3-3-1.各入學管道主委學校</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/seek_02.jsp">3-3-2.服務學習</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/seek_03.jsp">3-3-3.超額比序</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/seek_04.jsp">3-3-4.免學費政策</a></li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/seek_05.jsp">3-3-5.高中職及五專學校</a>
                                    </li>
                                    <li><a
                                            href="${pageContext.request.contextPath}/seek_06.jsp">3-3-6.志願地圖</a></li>
                                    <li><a target="_blank"
                                           href="https://ttk.entry.edu.tw/NoExamImitate_TP/NoExamImitate/Apps/Page/Public/01/EnrollmentInquiry.aspx">3-2-6.招生資訊查詢</a></li>
                                </ul>
                            </li>
                            <li><a
                                    href="${pageContext.request.contextPath}/attachment/more/brochures">3-4.簡章下載</a>
                            </li>
                            <li><a
                                    href="${pageContext.request.contextPath}/attachment/more/briefings">3-5.簡報講綱</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="${pageContext.request.contextPath}/sitemap.jsp">4.網站導覽</a></li>
                    <li><a target="_blank"
                           href="https://ttk.entry.edu.tw/NoExamImitate_TP/NoExamImitate/Apps/Page/Public/01/EnrollmentInquiry.aspx">5.招生資訊查詢</a></li>
                </ul>
            </div>
            <!-- 內容 區塊 End -->

        </div>
    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>
<!-- 執行javascript 區塊 End -->
</body>
</html>
