<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="基北區免試入學"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="1" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">基北區免試入學</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">免試入學分發</div>
                                <div class="cc_Lv01_txt">
                                    免試入學分發並無入學門檻或申請條件，若報名人數未超過學校招生名額，則全額錄取（107學年度基北區高中職招生人數足以容納106學年度基北區國中畢業學生）；若報名人數超過學校招生名額，則進行超額比序。比序項目、積分計算與順次如下：
                                </div>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <caption>
                                            107 學年<strong>度</strong>免試入學超額比序項目積分對照表（108方案）
                                        </caption>
                                        <tr>
                                            <th>類別</th>
                                            <th>項目</th>
                                            <th>上限</th>
                                            <th colspan="17">積分換算</th>
                                            <th width="155">說明</th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">志願序</th>
                                            <td align="center"><span class="s_01">36分</span></td>
                                            <td colspan="2" align="center"><span class="s_01">36分</span><br/>第1-5志願</td>
                                            <td colspan="4" align="center"><span class="s_01">35分</span><br/>第6-10志願
                                            </td>
                                            <td colspan="4" align="center"><span class="s_01">34分</span><br/>第11-15志願
                                            </td>
                                            <td colspan="4" align="center"><span class="s_01">33分</span><br/>第16-20志願
                                            </td>
                                            <td colspan="3" align="center"><span class="s_01">32分</span><br/>第21-30志願
                                            </td>
                                            <td>同校、兩個以上科別連續選填，則視為同一志願</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">多<BR/>元<BR/>學<BR/>習<BR/>表<BR/>現</th>
                                            <th>均<BR/>衡<BR/>學<BR/>習</th>
                                            <td rowspan="2" align="center"><span class="s_01">36分</span></td>
                                            <td align="center">上限21分</td>
                                            <td colspan="8" align="center"><span class="s_01">7分</span><br/>符合1個領域</td>
                                            <td colspan="8" align="center"><span class="s_01">0分</span><br/>未符合</td>
                                            <td>健體、藝文、綜合三領域前五學習平均成績及格者</td>
                                        </tr>
                                        <tr>
                                            <th>服<BR/>務<BR/>學<BR/>習</th>
                                            <td align="center">上限15分</td>
                                            <td colspan="16" align="center"><span class="s_01">5分</span><br/>每學期服務<br/>滿6小時以上
                                            </td>
                                            <td>1.由國民中學學校認證。<br/><br/>
                                                2.採計期間為<u>104</u>學年度（<u>七年級</u>）上學期至<u>106</u>學年度（九年級）上學期，採計原則依「基北區免試入學服務學習時數認證及轉換採計原則」辦理。<br><br>3.
                                                非應屆畢（結）業生服務學習時數採計，除上開採計期間外，亦得選擇國民中學在學期間前5學期選3學期進行採計。
                                            </td>
                                        </tr>
                                        <tr>
                                            <th rowspan="4">國<BR/>中<BR/>教<BR/>育<BR/>會<BR/>考</th>
                                            <th rowspan="2">五科</th>
                                            <td rowspan="4" align="center"><span class="s_01">36分</span></td>
                                            <td rowspan="2" align="center">上限35分</td>
                                            <td colspan="2" align="center"><span class="s_01">7分</span></td>
                                            <td colspan="2" align="center"><span class="s_01">6分</span></td>
                                            <td colspan="3" align="center"><span class="s_01">5分</span></td>
                                            <td colspan="3" align="center"><span class="s_01">4分</span></td>
                                            <td colspan="2" align="center"><span class="s_01">3分</span></td>
                                            <td colspan="3" align="center"><span class="s_01">2分</span></td>
                                            <td align="center"><span class="s_01">1分</span></td>
                                            <td rowspan="4">1.國、數、英、社、自五科各科按等級標示轉換積分1-7分<br/><br/>
                                                2.寫作測驗級分轉換積分0.1-1分
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">A++</td>
                                            <td colspan="2">A+</td>
                                            <td colspan="3">A</td>
                                            <td colspan="3">B++</td>
                                            <td colspan="2">B+</td>
                                            <td colspan="3">B</td>
                                            <td>C</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">寫作測驗</th>
                                            <td rowspan="2" align="center">上限1分</td>
                                            <td colspan="3" align="center"><span class="s_01">1分</span></td>
                                            <td colspan="3" align="center"><span class="s_01">0.8分</span></td>
                                            <td colspan="2" align="center"><span class="s_01">0.6分</span></td>
                                            <td colspan="3" align="center"><span class="s_01">0.4分</span></td>
                                            <td colspan="3" align="center"><span class="s_01">0.2分</span></td>
                                            <td colspan="2" align="center"><span class="s_01">0.1分</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">6級</td>
                                            <td colspan="3">5級</td>
                                            <td colspan="2">4級</td>
                                            <td colspan="3">3級</td>
                                            <td colspan="3">2級</td>
                                            <td colspan="2">1級</td>
                                        </tr>
                                        <tr>
                                            <th>總積分</th>
                                            <td colspan="20" align="center"><span class="s_01">108分</span></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="cc_Lv01_txt">
                                    註:<br/>
                                    1.服務學習相關認證原則，依據「基北區十二年國民基本教育免試入學超額比序『服務學習』採計規定」辦理。<br/><br/>
                                    2.原住民學生、身心障礙學生、蒙藏學生、政府派赴國外工作人員子女、境外優秀科學技術人才子女、僑生及退伍軍人等法律授權訂定升學優待辦法之特殊身分學生，依相關特殊身分學生升學優待辦法辦理。<br/><br/>
                                    3.非應屆國中畢業生得向本區免試入學委員會提出申請參加免試入學，參加本年度國中教育會考，並採計其國中就學期間之紀錄，採計項目及積分由本區免試入學委員會審查認定之。
                                </div>

                                <BR/>
                                <div class="cc_Lv01_title">超額比序順次</div>
                                <div class="cc_Lv01_txt">總積分(108) &gt; 多元學習表現積分(36) &gt;
                                    國中教育會考積分(36) &gt; 志願序積分(36) &gt; 各科會考等級加標示
                                </div>

                                <BR/>
                                <div class="cc_Lv01_title">超額比序作業範例</div>
                                <div class="cc_Lv01_pic">
                                    <img src="${pageContext.request.contextPath}/templates/images/pic_003.png"
                                         alt="超額比序作業範例" width="799" height="851">
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>