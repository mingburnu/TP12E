<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="臺北市優先免試入學"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">臺北市優先免試入學</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">107年臺北市優先免試辦理學校</div>
                                <div class="cc_Lv01_title_b">第一類 完全免試（私立學校）</div>
                                <div class="cc_Lv01_txt">
                                    協和祐德高中、大誠高中、景文高中★、滬江高中、金甌女中、強恕高中、立人高中、十信高中、泰北高中、南華高中（進修學校（部））、志仁高中（進修學校（部））、育達高職、東方工商、喬治工商、稻江護家、惇敘工商、稻江商職、開南商工、開平餐飲（進修學校（部））
                                </div>

                                <BR/>
                                <div class="cc_Lv01_title_b">第二類 公立學校</div>
                                <div class="cc_Lv01_txt">
                                    西松高中★、中崙高中★、永春高中、和平高中★、大直高中★、明倫高中、成淵高中★、華江高中、大理高中★、景美女中、萬芳高中★、南港高中★、育成高中、內湖高中、麗山高中、南湖高中、陽明高中★、百齡高中★、復興高中、中正高中、松山家商、松山工農、大安高工、木柵高工、南港高工、內湖高工、士林高商
                                </div>

                                <BR/>
                                <div class="cc_Lv01_title_b">第二類 私立學校</div>
                                <div class="cc_Lv01_txt">達人女中★、大同高中、文德女中、衛理女中★、靜修女中★、方濟高中★</div>

                                <div class="cc_Lv01_txt_b">★表示該校現有辦理直升入學</div>
                                <div class="cc_Lv01_txt_b">註：學校辦理名單以「臺北市107
                                    學年度高級中等學校優先免試入學簡章」公告為準。
                                </div>

                                <BR/>
                                <div class="cc_Lv01_title">107 學年度優先免試入學流程圖</div>
                                <div class="cc_Lv01_pic">
                                    <img src="${pageContext.request.contextPath}/templates/images/pic_004.png"
                                         alt="107學年度優先免試入學流程圖" width="641" height="455">
                                </div>

                            </div>

                            <div class="catalog_file_list">
                                <div class="cf_Lv01_title">相關檔案</div>
                                <div class="cf_Lv01_title_b">簡章</div>
                                <div class="cf_Lv01_list">
                                    <a class="download_icon" target="_blank"
                                       href="${pageContext.request.contextPath}/resources/download/107學年度臺北市優先免試入學簡章.pdf">
                                        ● 臺北市107學年度高級中等學校優先免試入學簡章</a>
                                </div>

                                <div class="cf_Lv01_title_b">影片</div>
                                <div class="cf_Lv01_list">
                                    <a class="download_icon" target="_blank"
                                       href="https://www.youtube.com/watch?v=W3Tp5mbSrMM">
                                        ● 臺北市優先免試入學方案宣導影片</a>
                                </div>

                            </div>

                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>