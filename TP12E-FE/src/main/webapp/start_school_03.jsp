<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="直升入學"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="3" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">直升入學</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_txt">
                                    一、本市直升入學辦理學校<br>
                                    (一)國立學校：師大附中、政大附中。<br>
                                    (二)市立學校：中崙高中、大理高中、和平高中、大直高中、百齡高中、萬芳高中、西松高中、陽明高中、<br>
                                    　　　　　　　大同高中、成淵高中及南港高中。<br>
                                    (三)私立學校：靜修女中、華興高中、衛理女中、景文高中、達人女中、方濟高中。<br>
                                </div>
                                <div class="cc_Lv01_txt">
                                    二、本市直升入學辦理期程<br>
                                    依循教育部「107學年度國中教育會考暨全國高級中等學校及專科學校五年制適性入學重要日程表」並衡酌各校實際辦理需要，重要日程如下：<br>
                                    107年5月31日（星期四)至6月8日（星期五）中午12時：各校受理直升入學報名。<br>
                                    107年6月8日（星期五）：直升入學放榜。<br>
                                    107年6月11日（星期一）：各校直升入學報到（上午）及報到後放棄截止（下午）。<br>
                                </div>
                                <div class="cc_Lv01_txt">
                                    三、本市直升入學之招生名額、報名資格、報名方式、錄取方式(含超額比序項目、錄取報到等詳情，請依各校簡章規定辦理。<br>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>