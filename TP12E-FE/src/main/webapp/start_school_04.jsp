<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="技優甄審入學"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="4" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">技優甄審入學</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">志願選填：一校多科</div>

                                <div class="cc_Lv01_title">申請資格：</div>
                                <ul class="cc_Lv01_list">
                                    <li>國際技能競賽，獲得優勝以上名次</li>
                                    <li>全國技藝技能競賽，獲得優勝或佳作以上名次</li>
                                    <li>全國中小學科學展覽，獲得優勝以上名次</li>
                                    <li>各縣市技藝技能競賽、科學展覽，獲得優勝以上名次</li>
                                    <li>領有丙級以上技術士證</li>
                                    <li>應屆畢(結)業生技藝教育課程成績優良</li>
                                    <li>其他參加國際特殊技藝技能競賽，獲得優勝以上名次</li>
                                </ul>

                                <div class="cc_Lv01_title">積分核算：僅得擇優採一項計算積分</div>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0"
                                           class="cc_table_01">
                                        <tr>
                                            <th width="300">項目</th>
                                            <th>得獎名次</th>
                                            <th width="80">積分</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">國際技能競賽（包括科技展覽）</th>
                                            <td>優勝以上</td>
                                            <td>100</td>
                                        </tr>
                                        <tr>
                                            <td>未得獎</td>
                                            <td>95</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="4">全國性技藝技能競賽</th>
                                            <td rowspan="2">第一名、第二名、第三名</td>
                                            <td>主辦95</td>
                                        </tr>
                                        <tr>
                                            <td>協辦80</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">第四名至優勝或佳作</td>
                                            <td>主辦80</td>
                                        </tr>
                                        <tr>
                                            <td>協辦65</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">全國中小學科學展覽會、臺灣國際科學展覽會</th>
                                            <td>第一名、第二名、第三名</td>
                                            <td>95</td>
                                        </tr>
                                        <tr>
                                            <td>第四名至優勝或佳作</td>
                                            <td>80</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">其他國際性特殊技藝技能競賽</th>
                                            <td>第一名、第二名、第三名</td>
                                            <td>95</td>
                                        </tr>
                                        <tr>
                                            <td>第四名至優勝</td>
                                            <td>80</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="3">各縣（市）政府主辦報經本部備查之技藝技能比賽及科學展覽（不包括成果展）</th>
                                            <td>第一名、第二名、第三名（特優）</td>
                                            <td>70</td>
                                        </tr>
                                        <tr>
                                            <td>第四名、第五名、第六名（優等）</td>
                                            <td>65</td>
                                        </tr>
                                        <tr>
                                            <td>佳作（甲等）</td>
                                            <td>60</td>
                                        </tr>
                                        <tr>
                                            <th>領有技術士證</th>
                                            <td>領有丙級以上技術士證或相當於丙級以上之單一級技術士證</td>
                                            <td>50</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="3">
                                                應屆畢（結）業生技藝教育課程成績優良，技藝教育課程職群成績（與國中在校學習領域評量成績無涉）達該班PR值七十以上者（得擇優一職群成績採計得分）
                                            </th>
                                            <td>PR值（百分等級）九十以上</td>
                                            <td>50</td>
                                        </tr>
                                        <tr>
                                            <td>PR值（百分等級）八十以上未達九十</td>
                                            <td>45</td>
                                        </tr>
                                        <tr>
                                            <td>PR值（百分等級）七十以上未達八十</td>
                                            <td>40</td>
                                        </tr>
                                    </table>
                                </div>

                                <BR/>

                                <div class="cc_Lv01_title">分發依據：</div>
                                <ul class="cc_Lv01_list">
                                    <li>依積分高低順序及志願序分發相關專業群科就讀。 <BR>
                                        積分相同進行同分參酌以定錄取，同分參酌項目由各就學區自訂。
                                    </li>
                                    <li>所填志願之校科已額滿者，不予分發；其分發至額滿為止。</li>
                                    <li>經積分比序且同分參酌後，仍無法評比者，增額錄取之。</li>
                                    <li>分發以一次為限，一經分發後不得申請更改。</li>
                                </ul>

                                <div class="cc_Lv01_title">錄取名額：</div>
                                <div class="cc_Lv01_txt">公立學校每班內含2名，私立學校每班外加2名。</div>
                            </div>


                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>