<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="實用技能學程"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="5" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">實用技能學程</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">實用技能學程入學</div>
                                <ul class="cc_Lv01_list">
                                    <li>實用技能學程是以學生為中心、學校為本位的教育，注重學生多元性向與適性發展教育環境。</li>
                                    <li>課程設計是延續國中技藝教育課程，為具有技藝傾向、就業意願和想學習一技之長的學生所設計的學習環境。</li>
                                    <li>採分區分發方式。曾選習國中技藝教育學生優先分發，未曾選習國中技藝教育學生次之。</li>
                                </ul>

                                <div class="cc_Lv01_title">實用技能學程</div>
                                <ul class="cc_Lv01_list">
                                    <li>全國分區同時辦理輔導分發入學</li>
                                    <li>學生可任選一區報名(限一區)，採志願序分發</li>
                                    <li>不採計在校或國中教育會考成績</li>
                                    <li>修習國中技藝教育課程之國中畢業生優先錄取</li>
                                </ul>

                                <div class="cc_Lv01_title">優點特色</div>
                                <ul class="cc_Lv01_list">
                                    <li>培養學生職場就業技能為主</li>
                                    <li>適合有興趣學習技藝，就業意願高且想學習一技之長的學生</li>
                                    <li>日間上課／夜間上課</li>
                                </ul>

                                <div class="cc_Lv01_title">重要招生日程</div>
                                <ul class="cc_Lv01_list">
                                    <li>報名日期：107年5月23日（星期三）至107年5月24日（星期四）</li>
                                    <li>放榜：107年6月11日（星期一）</li>
                                    <li>報到：107年6月13日（星期三）</li>
                                    <li>報到後聲明放棄錄取資格截止日6月15日（星期五）</li>
                                    <li>未在規定日程內放棄不得報名參加下一個入學管道</li>
                                </ul>

                                <div class="cc_Lv01_title">實用技能學程職群與科別對照表</div>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0"
                                           class="cc_table_01">
                                        <tr>
                                            <th width="150">職群別</th>
                                            <th>科別</th>
                                        </tr>
                                        <tr>
                                            <th>機械群</th>
                                            <td valign="top">1.機械板金科 2.模具技術科 3.機械加工科 4.機械修護科&nbsp;<br>
                                                5.鑄造技術科 6.電腦繪圖科＊
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>動力機械群</th>
                                            <td>1.汽車修護科 2.機車修護科 3.塗裝技術科 4.汽車電機科</td>
                                        </tr>
                                        <tr>
                                            <th>電機與電子群</th>
                                            <td>1.水電技術科 2.家電技術科 3.視聽電子修護科 4.電機修護科&nbsp;<br/>
                                                5.微電腦修護科 6.冷凍空調技術科
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>土木與建築群</th>
                                            <td>1.營造技術科 2.電腦繪圖科＊</td>
                                        </tr>
                                        <tr>
                                            <th>化工群</th>
                                            <td>1.化工技術科 2.染整技術科</td>
                                        </tr>
                                        <tr>
                                            <th>商業群</th>
                                            <td>1.文書處理科 2.商業事務科 3.銷售事務科&nbsp;<br>
                                                4.商用資訊科 5.會計實務科 6.廣告技術科＊ 7.多媒體技術科
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>設計群</th>
                                            <td>1.金銀珠寶加工科 2.金屬工藝科3.廣告技術科＊ 4.服裝製作科&nbsp;<br>
                                                5.流行飾品製作科 6.裝潢技術科 7.竹木工藝科
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>農業群</th>
                                            <td>1.農業技術科 2.園藝技術科 3.造園技術科 4.寵物經營科&nbsp;<br>
                                                5.畜產加工科＊ 6.休閒農業科 7.茶葉技術科
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>食品群</th>
                                            <td>1.烘焙食品科＊ 2.食品經營科 3.水產食品加工科 4.畜產加工科＊</td>
                                        </tr>
                                        <tr>
                                            <th>美容造型群</th>
                                            <td>1.美髮技術科 2.美顏技術科3.美容造型科 4.美髮造型科</td>
                                        </tr>
                                        <tr>
                                            <th>餐旅群</th>
                                            <td><p>1.觀光事務科 2.餐飲技術科 3.旅遊事務科 4.烹調技術科 5.中餐廚師科 6.烘焙食品科＊</p></td>
                                        </tr>
                                        <tr>
                                            <th><strong>水產群</strong></th>
                                            <td>1.水產養殖技術科 2.漁具製作科 3.休閒漁業科</td>
                                        </tr>
                                        <tr>
                                            <th><strong>海事群</strong></th>
                                            <td>1.船舶機電科 2.海事資訊處理科</td>
                                        </tr>
                                    </table>
                                    <p>註：＊表示可跨職群科別，各校可自行視情況調整所屬職群。</p>

                                </div>
                                <BR>

                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <caption>
                                            臺北市107學年度實用技能學程各校申請一覽表
                                        </caption>
                                        <tr>
                                            <th rowspan="4">編號</th>
                                            <th rowspan="4">學校</th>
                                            <th colspan="5">107學年度申請實用技能學程</th>
                                        </tr>
                                        <tr>
                                            <th colspan="3">班數、學生人數</th>
                                            <th colspan="2">上課時段</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">職群/科別</th>
                                            <th colspan="2">一年級</th>
                                            <th rowspan="2">日間</th>
                                            <th rowspan="2">夜間</th>
                                        </tr>
                                        <tr>
                                            <th>班數</th>
                                            <th>人數</th>
                                        </tr>
                                        <tr>
                                            <td align="center">1</td>
                                            <td>市立南港高工</td>
                                            <td>機械群<br/>
                                                機械加工科
                                            </td>
                                            <td align="center">1</td>
                                            <td align="center">35</td>
                                            <td align="center">■</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>私立稻江商職</td>
                                            <td>餐旅群<br/>
                                                餐飲技術科
                                            </td>
                                            <td align="center">2</td>
                                            <td align="center">100</td>
                                            <td align="center">&nbsp;</td>
                                            <td align="center">■</td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td>私立開平餐飲</td>
                                            <td>餐旅群<br/>
                                                餐飲技術科
                                            </td>
                                            <td align="center">1</td>
                                            <td align="center">50</td>
                                            <td align="center">&nbsp;</td>
                                            <td align="center">■</td>
                                        </tr>
                                        <tr>
                                            <td align="center">4</td>
                                            <td>私立滬江高中</td>
                                            <td>餐旅群<br/>
                                                餐飲技術科
                                            </td>
                                            <td align="center">1</td>
                                            <td align="center">45</td>
                                            <td align="center">■</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="4" align="center">5</td>
                                            <td rowspan="4">私立協和祐德高中</td>
                                            <td><p>動力機械群<br>
                                                汽車修護科</p></td>
                                            <td align="center">2</td>
                                            <td align="center">100</td>
                                            <td align="center">■</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>電機電子群<br>
                                                水電技術科
                                            </td>
                                            <td align="center">1</td>
                                            <td align="center">40</td>
                                            <td align="center">■</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>設計群<br>
                                                廣告技術科
                                            </td>
                                            <td align="center">1</td>
                                            <td align="center">40</td>
                                            <td align="center">■</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>餐旅群<br>
                                                觀光事務科
                                            </td>
                                            <td align="center">1</td>
                                            <td align="center">40</td>
                                            <td align="center">■</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center">合計</td>
                                            <td align="center">5校</td>
                                            <td align="center">8科</td>
                                            <td align="center">10班</td>
                                            <td align="center">220人</td>
                                            <td align="center">&nbsp;</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>