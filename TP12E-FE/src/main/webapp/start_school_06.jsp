<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="特色招生考試分發"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="6" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">特色招生考試分發</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <caption>特色招生甄選入學</caption>
                                        <tr valign="top">
                                            <th>招生種類</th>
                                            <td>高中藝才班（含音樂、美術、舞蹈及戲劇班）、體育班、科學班、專業群科（高職）</td>
                                        </tr>
                                        <tr valign="top">
                                            <th>考試</th>
                                            <td>術科測驗（107年3-5月辦理），並得採計會考成績作為門檻</td>
                                        </tr>
                                        <tr valign="top">
                                            <th>分發放榜</th>
                                            <td>詳見各入學管道簡章日程</td>
                                        </tr>
                                    </table>
                                </div>

                                <BR/>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <caption>特色招生考試分發入學</caption>
                                        <tr>
                                            <th>招生學校</th>
                                            <th>國立政大附中</th>
                                            <th>國立師大附中</th>
                                            <th><strong>臺北市立麗山高級中學</strong></th>
                                        </tr>
                                        <tr>
                                            <th>招生班別</th>
                                            <td align="center">英語國際特色班</td>
                                            <td align="center">資訊科學特色班</td>
                                            <td align="center">資訊科學特色班</td>
                                        </tr>
                                        <tr>
                                            <th>招生名額</th>
                                            <td align="center">38</td>
                                            <td align="center">35</td>
                                            <td align="center">30</td>
                                        </tr>
                                        <tr>
                                            <th>報名截止日期</th>
                                            <td align="center">107年6月22日</td>
                                            <td align="center">107年6月22日</td>
                                            <td align="center">107年6月22日</td>
                                        </tr>
                                        <tr>
                                            <th>考試日期</th>
                                            <td align="center">107年6月24日</td>
                                            <td align="center">107年6月24日</td>
                                            <td align="center">107年6月24日</td>
                                        </tr>
                                        <tr>
                                            <th>考試科目</th>
                                            <td align="center">英文（閱讀、聽力）</td>
                                            <td align="center">數學、資訊</td>
                                            <td align="center">數學、資訊</td>
                                        </tr>
                                        <tr>
                                            <th>分發放榜</th>
                                            <td align="center">107年7月10日</td>
                                            <td align="center">107年7月10日</td>
                                            <td align="center">107年7月10日</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="cc_Lv01_txt_b">註：詳見各校招生簡章</div>

                                <BR/>

                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <caption>
                                            107學年基北區免試入學與特色招生考試分發入學一次分發到位規劃
                                        </caption>
                                        <tr>
                                            <td><strong>★甲生只有報名免試入學（A卡）</strong><br/>
                                                選填時間107年6月21日～ 6月28日
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>★乙生只有報名特色招生(他區的學生，B卡)</strong><br/>
                                                選填時間107年6月26日～ 6月28日
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>★丙生報名免試入學+ 特色招生（A+B 卡→ C卡）</strong> <br/>
                                                免試選填時間107年6月21日～ 6月29日<br/>
                                                特招選填時間107年6月26日～ 6月28日
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="cc_Lv01_txt_b">報名系統自動合成一張志願表，請家長簽名</div>


                            </div>

                            <div class="catalog_file_list">
                                <div class="cf_Lv01_title">相關檔案連結</div>
                                <div class="cf_Lv01_list">
                                    <a class="download_icon" target="_blank"
                                       href="${pageContext.request.contextPath}/resources/download/基北區高級中等學校特色招生核定作業要點.pdf">●
                                        基北區高級中等學校特色招生核定作業要點</a>
                                    <a class="download_icon" target="_blank"
                                       href="${pageContext.request.contextPath}/resources/download/107學年度政大附中特色招生考試分發入學簡章.pdf">●
                                        107學年度政大附中特色招生考試分發入學簡章</a>
                                    <a class="download_icon" target="_blank"
                                       href="${pageContext.request.contextPath}/resources/download/107學年度師大附中特色招生考試分發入學簡章.pdf">●
                                        107學年度師大附中特色招生考試分發入學簡章</a>
                                    <a class="download_icon" target="_blank"
                                       href="${pageContext.request.contextPath}/resources/download/107學年度麗山高中特色招生考試分發入學簡章.pdf">●
                                        107學年度麗山高中特色招生考試分發入學簡章</a>
                                </div>
                            </div>


                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>