<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="科學班"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="7" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">科學班</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">設班目的</div>
                                <ul class="cc_Lv01_list">
                                    <li>提供具<span class="txt_01">科學潛能</span>之高級中等學校優秀學生適性發展機會。
                                    </li>
                                    <li>開設及發展特殊科學教育學程，提供優越之教學環境及卓越師資，培養學生從事個別科學研究之能力和創造力，充分發揮天賦潛能。</li>
                                    <li>培育兼具人文素養與科學專業知能之科學傑出人才，厚植國家之高素質科技人才及國家競爭力。</li>
                                </ul>

                                <div class="cc_Lv01_title">課程規劃</div>
                                <ul class="cc_Lv01_list">
                                    <li>
                                        科學班之課程規劃，應依高級中等學校辦理實驗教育辦法之規定，以學校三年課程整體規劃，除學校基礎科學相關科目外，得彈性設計科學專業領域科目；其應修學分數及教學方式，由<span
                                            class="txt_01">合作大學</span>與<span class="txt_01">學校</span>共同規劃。
                                    </li>
                                    <li>科學班課程分二階段：
                                        <ol class="cc_Lv02_list">
                                            <li><span class="txt_01">第一階段學程</span>(高一及高二)學生應修讀普通高級中學基礎科學相關科目與人文及社會相關領域學分，並得於就讀期間依資賦優異降低入學年齡縮短就業年限相關規定，參加學科免修考試；
                                            </li>
                                            <li><span class="txt_01">第二階段學程</span>由學校設計科學專業領域科目，邀請合作大學教師至學校講授，或直接選修大學開設之相關科目。
                                            </li>
                                        </ol>
                                    </li>
                                    <li>科學班學生於修讀第二階段學程期間，在大學教之指導下，進行<span class="txt_01">個別科學研究</span>計畫。</li>
                                </ul>

                                <div class="cc_Lv01_title">升學進路</div>
                                <ul class="cc_Lv01_list">
                                    <li>科學班畢業學生應修畢各開班實施計畫所定課程，並修畢一百六十學分，以取得高級中學畢業證書。</li>
                                    <li>
                                        科學班學生於就讀期間之學科資格考試成績及個別研究成果得作為「大學多元入學方案」甄選入學之參據。學生於第二階段學程修得之數理科學分及修課證明書，得作為將來進入大學後抵免學分之參考。
                                    </li>
                                    <li>科學班學生就讀期間通過高級中學與合作大學辦理之學科資格考試，未修畢第二階段學程者，其升學仍依「大學多元入學方案」辦理。</li>
                                </ul>

                                <div class="cc_Lv01_title">退場輔導機制</div>
                                <div class="cc_Lv01_txt">
                                    科學班學生就讀期間，因學習適應、生涯規劃考量、或未通過學科資格考試等因素，無法繼續第二階段學程者，<span
                                        class="txt_01">得轉介校內其他班級</span>就讀。
                                </div>


                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <caption>
                                            107學年度辦理科學班甄選入學學校
                                        </caption>
                                        <tr>
                                            <th width="13%">區域別</th>
                                            <th width="36%">招生學校</th>
                                            <th width="34%">合作辦理大學</th>
                                            <th width="17%">招生人數</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="6">北區</th>
                                            <td>臺北市立建國高中</td>
                                            <td>國立臺灣大學</td>
                                            <td>30</td>
                                        </tr>
                                        <tr>
                                            <td>臺北市立第一女子高級中學</td>
                                            <td>國立臺灣大學</td>
                                            <td>30</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">國立臺灣師大附中</td>
                                            <td>國立臺灣師範大學</td>
                                            <td rowspan="2">30</td>
                                        </tr>
                                        <tr>
                                            <td>國立陽明大學</td>
                                        </tr>
                                        <tr>
                                            <td>國立武陵高中</td>
                                            <td>國立中央大學</td>
                                            <td>30</td>
                                        </tr>
                                        <tr>
                                            <td>國立新竹實中</td>
                                            <td>國立清華大學</td>
                                            <td>25</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">中區</th>
                                            <td>國立臺中一中</td>
                                            <td>國立交通大學</td>
                                            <td>30</td>
                                        </tr>
                                        <tr>
                                            <td>國立彰化高中</td>
                                            <td>國立中興大學</td>
                                            <td>30</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="4">南區</th>
                                            <td rowspan="2">國立嘉義高中</td>
                                            <td>國立嘉義大學</td>
                                            <td rowspan="2">25</td>
                                        </tr>
                                        <tr>
                                            <td>國立中正大學</td>
                                        </tr>
                                        <tr>
                                            <td>國立臺南一中</td>
                                            <td>國立成功大學</td>
                                            <td>30</td>
                                        </tr>
                                        <tr>
                                            <td>高雄市立高雄中學</td>
                                            <td>國立中山大學</td>
                                            <td>30</td>
                                        </tr>
                                    </table>
                                </div>
                                <BR>

                                <div class="cc_Lv01_title">招生對象與名額</div>
                                <ul class="cc_Lv01_list">
                                    <li>招生對象：凡國民中學<span class="txt_01">應屆畢業</span>、高級中學國中部<span
                                            class="txt_01">應屆畢業</span>或符合「特殊教育學生調整入學年齡及修業年限實施辦法」之國中學生。
                                    </li>
                                    <li>招收人數：30名為限。(各校<span class="txt_01">實際招生名額與招生性別以各校經核定後公告之簡章為準</span>)
                                    </li>
                                    <li>科學班採各校單獨招生，同時考試。</li>
                                </ul>


                                <div class="cc_Lv01_title">報考資格</div>
                                <div class="cc_Lv01_txt"><span class="txt_01">學生應具備下列資格，經學校推薦後，始得報考科學班；其推薦總人數，以該國中應屆畢業生總人數百分之二十為限：</span>
                                    <ol class="cc_Lv02_list">
                                        <li>學校參酌學生於國民中學(含高級中等學校所設國民中學部)就學期間之下列學業成就，予以推薦：
                                            <ul class="cc_Lv03_list">
                                                <li class="txt_01">在國民中學就讀期間，學業總成績排名居全校同一年級前百分之二十以內(依各校招生簡章規定)
                                                    (應屆畢業生及調整修業年限學生之學業總成績所採計學期數需以文字清楚說明，學期數由各校自訂)。
                                                </li>
                                                <li>依前述學業總成績計算原則，數學或自然領域成績在全校前百分之二以內。</li>
                                                <li>凡就讀於教育主管機關核定之國民中學數理資優資源班者，或通過教育主管機關數理資優鑑定<span
                                                        class="txt_01">並持有證明者</span>。
                                                </li>
                                                <li>通過「國際國中生科學奧林匹亞競賽」或「國際數理奧林匹亞競賽初選」，或具備「亞太數學奧林匹亞競賽國家代表隊決選研習營」報名資格。</li>
                                                <li>曾獲教育部或國教署或科技部主辦有關數理科目之全國競賽（例如全國中小學科學展覽會）前三名或佳作。</li>
                                            </ul>
                                        </li>
                                        <li>通過心理素質評估，由就讀學校推薦。</li>
                                    </ol>
                                </div>


                                <div class="cc_Lv01_title">簡章及報名表下載</div>
                                <ul class="cc_Lv01_list">
                                    <li>招生學校多採<span class="txt_01">網路下載自行列印</span>方式，不另發售。
                                    </li>
                                    <li>下載日期：依各校網頁公告。</li>
                                    <li>下載網站：各招生學校科學班招生網頁。</li>
                                    <li>請<span class="txt_01">家長及各國中老師協助下載各種表格</span>，以<span
                                            class="txt_01">A4規格</span>白色普通影印紙（直式）<span class="txt_01">單面</span>列印，提供有意願且符合資格學生使用。
                                    </li>
                                </ul>


                                <div class="cc_Lv01_title">招生流程與方式(一)</div>
                                <div class="cc_Lv01_txt">
                                    報名暨入班資格審查<BR> (一)報名時間及地點：依各校簡章公告<BR> (二)報名費用：500元<BR>
                                    (三)報名之學生應檢具以下表件，並依序彙整
                                </div>
                                <ul class="cc_Lv01_list">
                                    <li>報名表</li>
                                    <li>甄選證：請填妥應自行填寫部分，<span class="txt_01">並將照片黏貼於甄選證(需與報名表上的照片相同)</span>。</li>
                                    <li>心理素質評估觀察表：請就讀學校老師推薦並經教務處核章。</li>
                                    <li>相關資格證明文件(依簡章報名之學生應檢具之證明文件，依序彙整。)</li>
                                    <li>其他(低收入、中低收入、身心障礙等相關證明文件)</li>
                                </ul>


                                <div class="cc_Lv01_title">招生流程與方式(二)</div>
                                <div class="cc_Lv01_txt">
                                    錄取方式<BR> (一)直接錄取：符合下列任一條件者，不必參加科學能力檢定及實驗實作，直接錄取進入科學班。
                                    <ol class="cc_Lv02_list">
                                        <li>參加「國際國中生科學奧林匹亞競賽」獲個人銅牌獎（含）以上者。</li>
                                        <li>參加「國際數理奧林匹亞競賽」獲獎或選訓決賽完成結訓，並獲保送高中資格者。</li>
                                        <li>參加「國際科學展覽」獲獎或獲選「國際科學展覽」正選代表，並獲保送高中資格者。</li>
                                    </ol>
                                    直接錄取名額是否有限制，依各校簡章之規定。
                                </div>

                                <BR>

                                <div class="cc_Lv01_title">招生流程與方式(三)</div>
                                <div class="cc_Lv01_txt">
                                    錄取方式<BR> (二)甄選錄取：
                                    <ol class="cc_Lv02_list">
                                        <li>科學能力檢定。
                                            <ul class="cc_Lv03_list">
                                                <li>科學能力檢定之內容與通過名額，均依各校簡章規定。</li>
                                                <li>自105學年度起，科學能力檢定均加考語文項目。詳情以正式公告簡章為準。</li>
                                            </ul>
                                        </li>
                                        <li>實驗實作。
                                            <ul class="cc_Lv03_list">
                                                <li><span class="txt_01">實驗實作之項目與繳交費用，各校簡章規定</span>。</li>
                                            </ul>
                                        </li>
                                    </ol>
                                </div>

                                <BR>

                                <div class="cc_Lv01_title">招生流程與方式(四)</div>
                                <div class="cc_Lv01_pic">
                                    <a target="_blank"
                                       href="${pageContext.request.contextPath}/templates/images/pic_006.png">
                                        <img src="${pageContext.request.contextPath}/templates/images/pic_006.png"
                                             alt="招生流程與方式"></a>
                                </div>
                                <BR>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <tr>
                                            <th width="30%">時間</th>
                                            <th width="40%">辦理事項</th>
                                            <th width="30%">備註</th>
                                        </tr>
                                        <tr>
                                            <td>107年 1月~2月</td>
                                            <td>簡章公告 <br/>
                                                辦理招生說明會
                                            </td>
                                            <td>依各校正式公告為準</td>
                                        </tr>
                                        <tr>
                                            <td>107年 3月8日~9日</td>
                                            <td>科學班報名</td>
                                            <td>各校自行辦理</td>
                                        </tr>
                                        <tr>
                                            <td>107年 3月14日</td>
                                            <td>公告資格審查結果</td>
                                            <td>依各校簡章規定</td>
                                        </tr>
                                        <tr>
                                            <td>107年 3月17日</td>
                                            <td>科學能力檢定</td>
                                            <td>各校同時辦理</td>
                                        </tr>
                                        <tr>
                                            <td>107年 3月中旬至 4月上旬</td>
                                            <td>實驗實作與面談</td>
                                            <td>依各校簡章規定</td>
                                        </tr>
                                        <tr>
                                            <td>107年 4月13日</td>
                                            <td>科學班錄取新生報到</td>
                                            <td>各校同時辦理</td>
                                        </tr>
                                    </table>
                                    *報到後如欲放棄入學科學班，須以書面提出放棄錄取資格聲明後，始能透過高中高職其他入學管道就學；重複申請其他入學管道者將取消入班資格。
                                </div>
                            </div>


                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>