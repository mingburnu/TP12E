<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="體育班及運動績優"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="8" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">體育班及運動績優</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">體育班</div>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <tr>
                                            <th width="40%">招生種類及名額</th>
                                            <th width="20%">招生範圍</th>
                                            <th width="20%">招生方式</th>
                                            <th width="20%">錄取依據</th>
                                        </tr>
                                        <tr>
                                            <td valign="top">1.107學年度計有 145校辦理，招生班級數計158班，招生名額共 5,313名。 <br/>
                                                2.107學年度各校招生名額已公布於教育部體育署網站電子公告欄。
                                            </td>
                                            <td>不受就學區限制，學生得跨區報考</td>
                                            <td>學校得採獨立或聯合招生方式為之</td>
                                            <td>依術科測驗分數為錄取依據</td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <tr>
                                            <th width="40%">107年重要日程</th>
                                            <th width="30%">術科測驗內容</th>
                                            <th width="30%">未獲錄取可報名參加續招</th>
                                        </tr>
                                        <tr>
                                            <td valign="top">1.術科測驗報名：107年4月30日至5月2日 <br/>
                                                2.術科測驗：107年5月5日 <br/>
                                                3.放榜：107年5月7日 <br/>
                                                4.報到：107年7月13日
                                            </td>
                                            <td>包括基礎體能、專項技術及運動成就表現等，由學校依體育班發展運動種類及特色課程內容，設計考科及計分方式。</td>
                                            <td>若參加體育班術科測驗結果未獲錄取，可於體育班學校辦理續招時，再次報名參加辦理續招學校之體育班特色招生甄選入學。</td>
                                        </tr>
                                    </table>
                                </div>

                                <BR>

                                <div class="cc_Lv01_title">運動績優</div>
                                <div class="cc_Lv01_pic">
                                    <img src="${pageContext.request.contextPath}/templates/images/pic_007.png"
                                         alt="運動績優" width="100%">
                                </div>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <tr>
                                            <th valign="top" width="40%">107年甄審甄試重要日程</th>
                                            <th width="30%" valign="top">招生種類及名額</th>
                                            <th width="30%" valign="top">招生範圍</th>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                1.甄試學科考試：107年6月2日<br/>
                                                2.甄試術科檢定：107年6月3日 <br/>
                                                3.錄取分發放榜:107年6月〇日<br/>
                                                4.報到：107年7月13日截止
                                            </td>
                                            <td valign="top">招生名額不限體育班級。各校招生名額會已公布於國教署網站電子公告欄。</td>
                                            <td valign="top">不受就學區限制，學生得跨區報考。</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>