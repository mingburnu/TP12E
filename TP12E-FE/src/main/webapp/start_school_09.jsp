<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="專業群科（高職）"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="9" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">專業群科（高職）</div>
                            <div class="catalog_content">
                                <div class="cc_Lv01_title">特色招生專業群科(高職)甄選入學之依據</div>
                                <div class="cc_Lv01_title_b">高級中等學校多元入學招生辦法第十一條</div>
                                <div class="cc_Lv01_txt">
                                    特色招生辦理方式如下：<BR>
                                    一、甄選入學：學校之音樂、美術、舞蹈、戲劇、科學、體育及高級中等學校專業群、科之特殊班、科、組、群，得參採國中教育會考成績作為錄取門檻，辦理單獨或聯合招生，並應依術科測驗分數及學生志願，作為錄取依據。<BR>
                                </div>

                                <BR>

                                <div class="cc_Lv01_title_b">107學年度辦理學校現況</div>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <tr>
                                            <th valign="top">校名</th>
                                            <th valign="top">特色招生科/班</th>
                                            <th valign="top">招生名額</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2" valign="middle">士林高商</th>
                                            <td valign="top">廣告設計科</td>
                                            <td valign="top">34</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">資料處理科</td>
                                            <td valign="top">16</td>
                                        </tr>
                                        <tr>
                                            <th valign="top">松山家商</th>
                                            <td valign="top">廣告設計科</td>
                                            <td valign="top">33</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="6">惇敘工商</th>
                                            <td valign="top">汽車科-進口車輛技術人才班</td>
                                            <td valign="top">40</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">汽車科-柴油車輛維修班</td>
                                            <td valign="top">10</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>汽車科-汽車生活顧問養成班</p></td>
                                            <td valign="top">10</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>汽車科-車輛板噴技術人才班</p></td>
                                            <td valign="top">10</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>電機科-水電與空調實務班</p></td>
                                            <td valign="top">20</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>建築科-室內裝潢木工達人特色班</p></td>
                                            <td valign="top">18</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="5">金甌女中</th>
                                            <td valign="top">應用外語科(日文組)-日文創意聲優專班</td>
                                            <td valign="top">50</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">應用外語科(英語組)-英文導遊會展專班</td>
                                            <td valign="top">50</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">商業經營科-時尚經營微型創業班</td>
                                            <td valign="top">25</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>多媒體設計科-2D動畫與影音剪輯班</p></td>
                                            <td valign="top">25</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">觀光事業科-觀光旅運專班</td>
                                            <td valign="top">25</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="4">育達家商</th>
                                            <td valign="top"><p>表演藝術科-多元表演班</p></td>
                                            <td valign="top">20</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">餐飲管理科-烘焙實務廚藝專班</td>
                                            <td valign="top">50</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">觀光事業科-五星旅館達人專班</td>
                                            <td valign="top">50</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>多媒體設計科-動畫遊戲製作專班</p></td>
                                            <td valign="top">50</td>
                                        </tr>
                                        <tr>
                                            <th valign="top">協和祐德高中</th>
                                            <td valign="top">汽車科-重型機車維修班</td>
                                            <td valign="top">45</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="5">景文高中</th>
                                            <td valign="top">應用外語科(英文組)–航空服務特色班</td>
                                            <td valign="top">15</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>資訊科-APP設計班</p></td>
                                            <td valign="top">15</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>廣告設計科-動漫及動畫設計班</p></td>
                                            <td valign="top">15</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>室內空間設計科-住家及商店設計班</p></td>
                                            <td valign="top">20</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><p>商業經營科-小資創業班</p></td>
                                            <td valign="top">10</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">滬江高中</th>
                                            <td valign="top">餐飲管理科-烘焙專班</td>
                                            <td valign="top">35</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">廣告設計科-漫遊文創作實作班</td>
                                            <td valign="top">35</td>
                                        </tr>
                                        <tr>
                                            <th>稻江高商</th>
                                            <td valign="top"><p>觀光事業科-五星旅館專才實務班</p></td>
                                            <td valign="top">50</td>
                                        </tr>
                                        <tr>
                                            <th><strong>私立東方工商</strong></th>
                                            <td valign="top"><p>多媒體應用科-文創專班</p></td>
                                            <td valign="top">30</td>
                                        </tr>
                                    </table>
                                </div>

                                <BR>

                                <div class="cc_Lv01_title">專業群科(高職)（臺北市）</div>
                                <div class="cc_Lv01_title_b">入學方式：</div>
                                <div class="cc_Lv01_txt">除辦理術科測驗（如實驗、面試、實作、表演等項目）外，亦得參採國中教育會考成績做為錄取門檻。</div>
                                <div class="cc_Lv01_title_b">重要日程：</div>
                                <div class="cc_Lv01_txt">線上填寫報名資料：107年3月12日至3月20日<BR>
                                    報名：107年3月19日至3月22日<BR>
                                    術科測驗：107年4月29日<BR>
                                    術科測驗成績公告：107年5月21日<BR>
                                    放榜：107年6月12日<BR>
                                    報到：107年6月13日<BR>
                                    放棄及遞補：107年6月15日
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>