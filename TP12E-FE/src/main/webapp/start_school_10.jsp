<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/jsp/layout/head.jsp"/>

<body>
<div class="wrapper">

    <!-- top 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/top.jsp"/>
    <!-- top 區塊 End -->


    <!-- header 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp">
        <jsp:param name="detail" value="1"/>
    </jsp:include>
    <!-- header 區塊 End -->

    <!-- crumbs 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/crumbs.jsp">
        <jsp:param name="position" value="start_school_01.jsp"/>
        <jsp:param name="aName" value="入學方式"/>
        <jsp:param name="subPosition" value="<%=request.getRequestURI()%>"/>
        <jsp:param name="subAName" value="藝術才能班"/>
    </jsp:include>
    <!-- crumbs 區塊 End -->

    <!-- 主內容 區塊 Begin -->
    <div class="container">
        <div class="innerwrapper">

            <!-- 內容 區塊 Begin -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   summary="版型表格：選單主頁區塊" class="layout_04">
                <tr valign="top">
                    <td class="layout_04_01">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/listBox.jsp">
                            <jsp:param value="2" name="hover"/>
                        </jsp:include>
                    </td>
                    <td class="layout_04_02">
                        <jsp:include
                                page="/WEB-INF/jsp/layout/subListBox.jsp">
                            <jsp:param value="7" name="title"/>
                            <jsp:param value="10" name="hover"/>
                        </jsp:include>

                        <div class="catalog_content_box">
                            <div class="title">藝術才能班</div>
                            <div class="catalog_content">
                                <ul class="cc_Lv01_list">
                                    <li>分為音樂班、美術班、舞蹈班、戲劇班等四類。</li>
                                    <li>藝術才能班甄選入學以聯合辦理為原則。國中非藝術才能班學生亦可報考，且不受免試就學區之限制，但僅能向一區（校）報名術科測驗及申請分發。</li>
                                    <li>招生作業分術科測驗及分發作業二階段辦理：
                                        <ol class="cc_Lv02_list">
                                            <li>（一）術科測驗：不得施以學科成就測驗
                                                <ul class="cc_Lv03_list">
                                                    <li>音樂班考主修(樂器)、副修(樂器)、 視唱、聽寫、樂理及基礎和聲5科</li>
                                                    <li>美術班考素描、水彩、水墨及書法4科；舞蹈班考芭蕾、現代舞、中國舞、即興與創作4科</li>
                                                </ul>
                                            </li>
                                            <li>（二）分發作業：以術科測驗分數，現場撕榜作業，並得以國民中學教育會考成績為入學門檻。</li>
                                        </ol>
                                    </li>
                                </ul>

                                <div class="cc_Lv01_title">甄選入學招生作業</div>
                                <div class="cc_Lv01_pic">
                                    <img src="${pageContext.request.contextPath}/templates/images/pic_008.png"
                                         alt="甄選入學招生作業" width="370" height="261">
                                </div>

                                <BR>

                                <div class="cc_Lv01_title_b">甄選入學招生作業－術科測驗</div>
                                <ul class="cc_Lv01_list">
                                    <li>學生不受免試就學區之限制，惟僅能向一區報名</li>
                                    <li>同一類型之術科測驗以同日辦理為原則，不得施以學科成就測驗</li>
                                    <li>免試入學前，完成術科測驗並取得成績證明</li>
                                </ul>

                                <div class="cc_Lv01_title_b">甄選入學招生作業－分發作業</div>
                                <ul class="cc_Lv01_list">
                                    <li>報名及報到，與免試入學同時辦理</li>
                                    <li>以術科測驗分數現場撕榜作業</li>
                                    <li>得以國民中學教育會考評量結果為入學門檻</li>
                                    <li>如有缺額，得經主管機關同意後辦理續招</li>
                                </ul>

                                <div class="cc_Lv01_title_b">其他規定事項</div>
                                <ul class="cc_Lv01_list">
                                    <li>報名高中高職藝術才能資賦優異班或藝術才能班分發之學生，應依各該法規規定通過鑑定。</li>
                                    <li>經核定採特色招生甄選入學之學校，其名額不得流入其他入學管道。</li>
                                    <li>提供名額供參加國際性或全國性競賽表現獲前三等獎項者，申請鑑定安置，辦理時程、名額明定於簡章。</li>
                                </ul>

                                <div class="cc_Lv01_title">藝才班甄選入學術科測驗科目</div>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0"
                                           class="cc_table_01">
                                        <tr>
                                            <td width="50%">
                                                <img src="${pageContext.request.contextPath}/templates/images/pic_009.png"
                                                     alt="藝才班甄選入學術科測驗科目-音樂類" width="100%"></td>
                                            <td width="50%">
                                                <img src="${pageContext.request.contextPath}/templates/images/pic_010.png"
                                                     alt="藝才班甄選入學術科測驗科目-美術類" width="100%"></td>
                                        </tr>
                                        <tr>
                                            <td width="50%">
                                                <img src="${pageContext.request.contextPath}/templates/images/pic_011.png"
                                                     alt="藝才班甄選入學術科測驗科目-舞蹈類" width="100%"></td>
                                            <td width="50%">
                                                <img src="${pageContext.request.contextPath}/templates/images/pic_012.png"
                                                     alt="藝才班甄選入學術科測驗科目-戲劇類" width="100%"></td>
                                        </tr>
                                    </table>
                                </div>

                                <BR>

                                <div class="cc_Lv01_title">107年藝才班甄選入學聯合招生區</div>
                                <div class="cc_Lv01_pic">
                                    <img src="${pageContext.request.contextPath}/templates/images/pic_013.png"
                                         alt="105年藝才班甄選入學聯合招生區" width="100%">
                                </div>


                                <div class="cc_Lv01_title">藝才班北區承辦學校</div>
                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <tr>
                                            <th valign="top">班別</th>
                                            <th valign="top">承辦學校</th>
                                        </tr>
                                        <tr>
                                            <td valign="top">音樂班</td>
                                            <td valign="top">國立臺灣師大附中</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">舞蹈班</td>
                                            <td valign="top">國立竹北高中</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">美術班</td>
                                            <td valign="top">臺北市立中正高中</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">戲劇班</td>
                                            <td valign="top">臺北市立復興高中（全國唯一）</td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="cc_Lv01_table">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cc_table_01">
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>音樂</th>
                                            <th>舞蹈</th>
                                            <th>美術</th>
                                            <th>戲劇</th>
                                        </tr>
                                        <tr>
                                            <td>承辦學校 <br/>
                                                【臺灣北區】
                                            </td>
                                            <td><p>國立<br>
                                                臺灣師大附中</p></td>
                                            <td><p>國立<br>
                                                竹北高中</p></td>
                                            <td><p>臺北市立<br>
                                                中正高中</p></td>
                                            <td><p>臺北市立<br>
                                                復興高中</p></td>
                                        </tr>
                                        <tr>
                                            <td>簡章公告</td>
                                            <td colspan="4">107年1月10日</td>
                                        </tr>
                                        <tr>
                                            <td>術科測驗線上報名暨郵寄繳件</td>
                                            <td colspan="4">107年3月1日至107年3月7日</td>
                                        </tr>
                                        <tr>
                                            <td>以競賽表現入學報名 <br/>
                                                暨郵寄繳件
                                            </td>
                                            <td colspan="3">107年3月21日至107年3月30日</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>以競賽表現入學放榜 <br/>
                                                暨寄發錄取通知
                                            </td>
                                            <td colspan="3">107年4月9日</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>以競賽表現入學報到</td>
                                            <td colspan="3">107年4月11日</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>術科測驗</td>
                                            <td>107年4月14日&nbsp;<br/>
                                                至 <br/>
                                                107年4月16日&nbsp;
                                            </td>
                                            <td>107年4月14日&nbsp;</td>
                                            <td>107年4月14日</td>
                                            <td>107年4月14日<br/>
                                                至 <br/>
                                                107年4月16日
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>國中教育會考</td>
                                            <td colspan="4">107年5月19日至107年5月26日</td>
                                        </tr>
                                        <tr>
                                            <td>甄選入學報名暨郵寄繳件</td>
                                            <td colspan="4">107年6月11日至107年6月15日</td>
                                        </tr>
                                        <tr>
                                            <td>甄選入學分發現場撕榜暨報到</td>
                                            <td colspan="3">107年7月11日</td>
                                            <td>107年7月13日</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- 內容 區塊 End -->
        </div>


    </div>
    <!-- 主內容 區塊 End -->

    <!-- footer 區塊 Begin -->
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    <!-- footer 區塊 End -->

</div>

<!-- 執行javascript 區塊 Begin -->
<jsp:include page="/WEB-INF/jsp/layout/js.jsp"/>

</body>
</html>