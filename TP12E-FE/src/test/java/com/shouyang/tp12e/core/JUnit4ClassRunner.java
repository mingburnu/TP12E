package com.shouyang.tp12e.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runners.model.InitializationError;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * load the log4j.properties for JUnit
 * 
 * @author Roderick
 * @version 2014/9/29
 */
public class JUnit4ClassRunner extends SpringJUnit4ClassRunner {

	public JUnit4ClassRunner(Class<?> clazz) throws InitializationError {
		super(clazz);
	}

	private static Logger logger = LogManager.getLogger(LogManager.ROOT_LOGGER_NAME);

	static {
		logger.trace("trace message");
		logger.debug("debug message");
		logger.info("info message");
		logger.warn("warn message");
		logger.error("error message");
		logger.fatal("fatal message");
		System.out.println("Hello World!");
	}

}
