package com.shouyang.tp12e.core;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by roderick on 2017/5/18.
 */
public class TestUrl {

    public static void main(String[] args) throws Exception {
        String url = "http://127.0.01:8080/link.jsp";
        URL obj = new URL(url);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        String useragent ="Googlebot/2.1 (+http://www.google.com/bot.html)";
        con.setRequestProperty("User-Agent", useragent);

        int responseCode = con.getResponseCode();

        System.out.println(responseCode);

    }

}
